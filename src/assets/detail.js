'use strict';

//=include ./common/js/compatible.js

(function (factory) {
    let BASE = factory();

    //=include ./common/js/dropload.js
    //=include ./common/js/Config.js
    //=include ./common/js/Util.js
    //=include ./common/js/Render.js
    //=include ./detail/js/Component.js
    //=include ./detail/js/Api.js

    //=include ./detail/js/Editate.js
    //=include ./detail/js/Light7.js

    const QUERY = Util.getQuery();

    //=include ./detail/js/Init.js

})(function () {
    //=include ./common/js/factory.js
});
