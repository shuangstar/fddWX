const Api = function () {

    let Api = function () {};
    let _this = Api.prototype;

    //=include ../../common/js/extends/Api.js

    // 所有用到的接口名称
    _this.urls = {
        // 审批详情
        getApprovalInfo : "getApprovalInfo.json",
        // 通过拒绝
        approval: "approval.json",
        // 提醒
        sendApprovalUser: "sendApprovalUser.json",
        // 获取提醒列表
        getApprovalUser: "getApprovalUser.json",
        // 撤销
        revokeApproval: "revokeApproval.json",
        // 节点留言
        approvalLeaveMessage: "approvalLeaveMessage.json",
        // 作废流程
        getStartApprovalInfo: "getStartApprovalInfo.json",
        // 作废
        saveFormDetail:  "saveFormDetail.json"
    };

    _this.getApprovalInfo = function ({ AD_id }, callback, fail) {
        let _loading = Component.loading("数据加载中").render();
        this.run("getApprovalInfo", {
            formApprovalDetailId: AD_id
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    _this.approval = function ({D_id, A_id, AD_id, pass, complain = "", formDetailJson = undefined}, callback, fail) {
        let _loading = Component.loading().render();
        let params = {
            formDetailId: D_id,
            formApprovalId: A_id,
            formApprovalDetailId: AD_id,
            result: pass,
            content: complain
        };

        if (formDetailJson) {
            params.formDetailJson = formDetailJson;
        }

        this.run("approval", params, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    _this.sendApprovalUser = function ({ D_id }, callback, fail) {
        let _loading = Component.loading().render();

        this.run("sendApprovalUser", {
            formDetailId: D_id
        }, function (result) {
            _loading.hide(function () {
                callback && callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
                fail && fail(e);
            });
        });
    };

    _this.getApprovalUser = function ({ D_id }, callback, fail) {
        let _loading = Component.loading().render();
        this.run("getApprovalUser", {
            formDetailId: D_id
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    _this.revokeApproval = function ({ D_id }, callback, fail) {
        let _loading = Component.loading().render();
        this.run("revokeApproval", {
            formDetailId: D_id
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    _this.approvalLeaveMessage = function ({approval_identity, student_id, D_id, A_id, message }, callback, fail) {
        let _loading = Component.loading().render();
        this.run("approvalLeaveMessage", {
            formDetailId: D_id,
            formApprovalId: A_id,
            content: message,
            studentId: student_id || '',
            identity: approval_identity
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    _this.getRange = function ({}, callback, fail) {
        let _loading = Component.loading().render();
        _this.post(Config.api.base_url + '/form/basedata/getRange.json', {
            range: "",
            identity: 1
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    _this.getStudent = function ({ id }, callback, fail) {
        let _loading = Component.loading().render();
        _this.get(Config.api.base_url + '/esb/api/student/getStudentsByCid', {
            clas_id: id
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    };

    /**
     * 上传
     */
    _this.upload = function ({ mediaIds }, callback, fail) {
        _this.post(Config.api.base_url + '/eduWeixin/uploadMedias', {
            mediaIds: mediaIds
        }, function (result) {
            callback(result);
        }, function (e) {
            fail && fail(e);
        });
    };

    /** 
     * 上传图片，通过base64
     */
    _this.uploadStudImage = function ({ base64, width, height }, callback, fail) {
        _this.post(Config.api.base_url + '/esb/api/uploadBase64', {
            file: base64.split(',')[1],
            width: width,
            height: height,
            ext: "jpg"
        }, function (data) {
            callback && callback(data);
        }, fail);
    };

    _this.getStartApprovalInfo = function ({D_id}, callback, fail) {
        let _loading = Component.loading().render();
        this.run("getStartApprovalInfo", {
            formDetailId: D_id,
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    }

    _this.saveFormDetail = function ({form_id, json, D_id, insert_stud_id}, callback, fail) {
        let _loading = Component.loading().render();
        this.run("saveFormDetail", {
            formId: form_id,
            formDetailJson: json,
            isInvalid: '1',
            oldFormDetailId: D_id,
            insertStudId: insert_stud_id
        }, function (result) {
            _loading.hide(function () {
                callback(result);
            });
        }, function (e) {
            _loading.hide(function () {
                Component.toast(e.message).render();
            });
        });
    }

    /** 
     * 再次发起
     */
    _this.replyApprovalInfo = function ({approval_identity, D_id, form_id, model_id}) {
        let techOrParent = approval_identity == "0" ? "parent" : "teacher";
        location.href = "/shijiwxy/weixin/html/"+ techOrParent +"/approval/approvalInfo.html?" + [
            "d=" + D_id,
            "formId=" + form_id,
            "formModelId=" + model_id
        ].join("&");
    }


    let address = null;
    _this.getAddress = function (params = {}, callback, fail) {
        if (address) {
            callback && callback(address, true);
            return;
        }
        let _loading = Component.loading().render();
        let fd = new FormData();
        fd.append("token", Config.user.token);
        fd.append("udid", Config.user.udid);
        fd.append("version", Config.user.version);
        fd.append("org_id", Config.user.org_id);

        $.ajax({
            url: Config.api.base_url + "/studentfiles/registration/getDistrictSelectJson.htm",
            type: 'POST',
            data: fd,
            cache: false,
            processData: false,
            contentType: false
        }).done(function (result) {
            _loading.hide();
            result = JSON.parse(result);
            address = JSON.parse(result.data);
            callback && callback(JSON.parse(result.data), true);
        }).fail(function (e) {
            _loading.hide();
            // 如果是手动中断，不弹出提示
            if (e.statusText == 'abort') {
                return false;
            }
            fail && fail({
                code: -1,
                message: '服务器繁忙，请重试'
            });
        });
    }

    return new Api;
}();
