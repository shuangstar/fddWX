/**
 * 初始化
 */
const Init = (function () {

    let Init = function () {};

    // 筛选结构
    let screen = {
        status: {
            name: 'status',
            color: 'primary',
            title: '审批状态',
            value: '',
            options: [
                {label: '全部', value: ''},
                {label: '正在审批', value: '1'},
                {label: '审批通过', value: '2'},
                {label: '审批拒绝', value: '3'},
                // {label: '审批撤销', value: '5'},
            ]
        },
        is_read: {
            name: 'is_read',
            color: 'warning',
            title: '消息状态',
            value: '',
            options: [
                {label: '全部', value: ''},
                {label: '未读', value: '0'}
            ]
        },
        modelLoading: false,
        model: {
            name: 'model',
            color: 'success',
            title: '审批名称',
            value: '',
            options: [
                {label: '全部', value: ''}
            ]
        }
    };

    function newInstance() {
        // 创建组件
        let Navbar = Component.navbar({
            onClick: function (tab) {
                switch (tab) {
                    case 'wait':
                        Toolbar.all_readed = true;
                        List.api = 'getPageApprovalNocheck';
                    break;
                    case 'readed':
                        Toolbar.all_readed = true;
                        List.api = 'getPageApprovalChecked';
                    break;
                    case 'copymy':
                        Toolbar.all_readed = false;
                        List.api = 'getPageApprovalCopy';
                    break;
                }

                // 重置其他条件为默认条件
                screen.status.value = '';
                screen.is_read.value = '';
                screen.model.value = '';
                Toolbar.is_show = false;
                Searchbar.search = '';
                List.resetForm();

                // 刷新列表
                List.refresh();
            }
        });
        let Toolbar = Component.toolbar({
            openClickSearch: function () {
                Searchbar.is_show = true;
            },
            closeClickSearch: function () {
                Searchbar.is_show = false;
                Searchbar.search = '';
                List.title = '';
                List.refresh();
            },
            screenClick: function () {

                let _opera = function () {
                    let menu = [];
                    switch (Navbar.active) {
                        case 'wait':
                            menu = [screen.model];
                            break;
                        case 'readed':
                            menu = [screen.status, screen.model];
                            break;
                        case 'copymy':
                            menu = [screen.status, screen.is_read, screen.model];
                            break;
                    }
                    Component.screenDialog({
                        screen: menu,
                        onComplete: function (screen) {
                            List.status = screen.status || '';
                            List.model_id = screen.model || '';
                            List.is_read = screen.is_read || '';

                            List.refresh();
                        }
                    }).render();
                };

                // *yk: 修改由于延时调用模板导致的显示不全问题
                var _loading;
                if (screen.modelLoading) {
                    _loading = Component.loading().render();
                    (function loadingModelSet() {
                        setTimeout(() => {
                            if (screen.modelLoading) {
                                loadingModelSet();
                            } else {
                                _loading.hide(() => _opera());
                            }
                        }, 200);
                    })();
                    return;
                } else {
                    _opera();
                }
            },
            readClick: function () {
                if (!window.confirm("是否全部已读？")) {
                    return false;
                }
                // 全部已读
                let _loading = Component.loading("操作中").render();
                Api.doApprovalReaded({
                    status: screen.status.value,
                    title: List.title
                }, function (result) {
                    _loading.hide(function () {
                        Component.toast("操作成功").render(function () {
                            List.refresh();
                        });
                    });
                }, function (e) {
                    _loading.hide(function () {
                        Component.toast(e.message).render();
                    });
                });
            }
        });
        let Searchbar = Component.searchbar({
            autoFocus: true,
            onSubmit: function () {
                List.title = Searchbar.search;
                List.refresh();
            }
        });
        let List = Component.list({
            api: 'getPageApprovalNocheck',
            page: 1,
            onLoad: function (exec) {
                exec(function (result) {
                    let form = [];
                    let auth = {};
                    try {
                        let ele = JSON.parse(result.approvalEleJson);
                        if (ele.hasOwnProperty("auth")) {
                            auth = ele.auth;
                        }
                    } catch (error) {}
                    try {
                        form = Util.likeItemObjectMap(JSON.parse(result.formJson), function (item, index) {
                            // *yk: 权限为隐藏不显示列表中
                            if (auth[index] == 3) {
                                return true;
                            }
                            // ＊yk: type为7和99的项不显示在列表中
                            if (["7", "99"].includes(item.type)) {
                                return true;
                            }
                            if (Util.type(item.value) == "string") {
                                item.value = item.value.trim();
                            }
                            if (Util.isEmpty(item.value)) {
                                return true;
                            }
                            return item;
                        });
                    } catch (error) {
                        form = [];
                    }

                    let item = {
                        D_id: result.formDetailId,
                        A_id: result.formApprovalId,
                        AD_id: result.formApprovalDetailId,
                        type: result.formDetailTitle,
                        time: result.insertTimeStr,
                        avatar: result.startHeadUrl || Config.default_avatar,
                        title: result.formModelTitle,
                        status: (function (status) {
                            let _status = {
                                1: 'approval',
                                2: 'success',
                                3: 'refuse',
                                4: 'cancel',
                                5: 'stop',
                                6: 'invaldate',
                                7: 'invalidated'
                            };
                            return _status[status] || '';
                        })(result.approval_status),
                        is_unreaded: ('' + result.is_read) === '0',
                        extra: form.slice(0, 3).map(item => {
                            return item.title + '：' + item.value_text;
                        })
                    };

                    // 附加：分页列表需要额外参数：列表中最后一项的ID
                    List.query_id = item.id;

                    return item;
                })
            }
        });

        // 加载组件
        let Container = Render.getContainer('app');
        Container.el.appendChild(Navbar.el);
        Container.el.appendChild(Toolbar.el);
        Container.el.appendChild(Searchbar.el);
        Container.el.appendChild(List.el);
        Container.render();
    }

    Init.prototype.instance = function () {
        screen.modelLoading = true;
        Api.getPageFormModel({}, function (result) {
            screen.model.options.length = 1;
            result.data.data.map(model => {
                screen.model.options.push({
                    label: model.title,
                    value: model.id + ""
                });
            });
            screen.modelLoading = false;
        });
        newInstance();
    };

    return new Init;
})();

// 初始化执行
Init.instance();
