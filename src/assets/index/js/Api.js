const Api = function () {

    let Api = function () {};
    let _this = Api.prototype;

    //=include ../../common/js/extends/Api.js

    // 所有用到的接口名称
    _this.urls = {
        // 待我审批
        getPageApprovalNocheck : "getPageApprovalNocheck.json",
        // 我已审批
        getPageApprovalChecked : "getPageApprovalChecked.json",
        // 抄送我的
        getPageApprovalCopy : "getPageApprovalCopy.json",
        // 表单模板页
        getPageFormModel : "getPageFormModel.json",
        // 全部已读
        doApprovalReaded: "doApprovalReaded.json"
    };

    /**
     * 获取进入选择盘列表
     */
    _this.getPageApprovalNocheck = function ({ page, size = 10, title = "", model_id = "", status = "", query_id = ""  }, callback, fail) {
        this.run("getPageApprovalNocheck", {
            pageNo: page,
            pageSize: size,
            queryId: query_id,
            title: title,
            formModelId: model_id,
            approvalStatus: status
        }, callback, fail);
    };

    _this.getPageApprovalChecked = function ({ page, size = 10, title = "", model_id = "", status = "", query_id = ""  }, callback, fail) {
        this.run("getPageApprovalChecked", {
            pageNo: page,
            pageSize: size,
            queryId: query_id,
            title: title,
            formModelId: model_id,
            approvalStatus: status
        }, callback, fail);
    };

    _this.getPageApprovalCopy = function ({ page, size = 10, title = "", model_id = "", status = "", is_read = "", query_id = "" }, callback, fail) {
        this.run("getPageApprovalCopy", {
            pageNo: page,
            pageSize: size,
            queryId: query_id,
            title: title,
            formModelId: model_id,
            approvalStatus: status,
            isRead: is_read
        }, callback, fail);
    };

    _this.getPageFormModel = function ({}, callback, fail) {
        this.run("getPageFormModel", {
            pageNo: 1,
            pageSize: 10000,
            title: ""
        }, callback, fail);
    };

    _this.doApprovalReaded = function ({ status, title = "" }, callback, fail) {
        this.run("doApprovalReaded", {
            approvalStatus: status,
            title: title
        }, callback, fail);
    };

    return new Api;
}();
