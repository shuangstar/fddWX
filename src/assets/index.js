'use strict';

//=include ./common/js/compatible.js

(function (factory) {
    let BASE = factory();

    //=include ./common/js/dropload.js
    //=include ./common/js/Config.js
    //=include ./common/js/Util.js
    //=include ./common/js/Render.js
    //=include ./index/js/Component.js
    //=include ./index/js/Api.js

    const QUERY = Util.getQuery();

    //=include ./index/js/Init.js

})(function () {
    //=include ./common/js/factory.js
});

