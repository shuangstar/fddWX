'use strict';

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  // 去除config.jsArray方法自定义扩展
  for (var name in Array.prototype) {
    delete Array.prototype[name];
  }
})();

(function (factory) {
  var BASE = factory();
  /**
     * dropload
     * 西门(http://ons.me/526.html)
     * 0.9.1(161205)
     */

  ;

  (function ($) {
    'use strict';

    var win = window;
    var doc = document;
    var $win = $(win);
    var $doc = $(doc);

    $.fn.dropload = function (options) {
      return new MyDropLoad(this, options);
    };

    var MyDropLoad = function MyDropLoad(element, options) {
      var me = this;
      me.$element = element; // 上方是否插入DOM

      me.upInsertDOM = false; // loading状态

      me.loading = false; // 是否锁定

      me.isLockUp = false;
      me.isLockDown = false; // 是否有数据

      me.isData = true;
      me._scrollTop = 0;
      me._threshold = 0;
      me.init(options);
    }; // 初始化


    MyDropLoad.prototype.init = function (options) {
      var me = this;
      me.opts = $.extend(true, {}, {
        scrollArea: me.$element,
        // 滑动区域
        domUp: {
          // 上方DOM
          domClass: 'dropload-up',
          domRefresh: '<div class="dropload-refresh">↓下拉刷新</div>',
          domUpdate: '<div class="dropload-update">↑释放更新</div>',
          domLoad: '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown: {
          // 下方DOM
          domClass: 'dropload-down',
          domRefresh: '<div class="dropload-refresh">↑上拉加载更多</div>',
          domLoad: '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
          domNoData: '<div class="dropload-noData">没有更多数据了</div>'
        },
        autoLoad: true,
        // 自动加载
        distance: 50,
        // 拉动距离
        threshold: '',
        // 提前加载距离
        loadUpFn: '',
        // 上方function
        loadDownFn: '' // 下方function

      }, options); // 如果加载下方，事先在下方插入DOM

      if (me.opts.loadDownFn != '') {
        me.$domDown = $('<div class="' + me.opts.domDown.domClass + '">' + me.opts.domDown.domRefresh + '</div>');
        me.$element.append(me.$domDown);
      } // 计算提前加载距离


      if (!!me.$domDown && me.opts.threshold === '') {
        // 默认滑到加载区2/3处时加载
        me._threshold = Math.floor(me.$domDown.height() * 1 / 3);
      } else {
        me._threshold = me.opts.threshold;
      } // 判断滚动区域


      if (me.opts.scrollArea == win) {
        me.$scrollArea = $win; // 获取文档高度

        me._scrollContentHeight = $doc.height(); // 获取win显示区高度  —— 这里有坑

        me._scrollWindowHeight = doc.documentElement.clientHeight;
      } else {
        me.$scrollArea = me.opts.scrollArea;
        me._scrollContentHeight = me.$element[0].scrollHeight;
        me._scrollWindowHeight = me.$scrollArea.height();
      }

      fnAutoLoad(me); // 窗口调整

      $win.on('resize', function () {
        clearTimeout(me.timer);
        me.timer = setTimeout(function () {
          if (me.opts.scrollArea == win) {
            // 重新获取win显示区高度
            me._scrollWindowHeight = win.innerHeight;
          } else {
            me._scrollWindowHeight = me.$element.height();
          }

          fnAutoLoad(me);
        }, 150);
      });
      var _control = false; // 绑定触摸

      me.$element.on('mousedown touchstart', function (e) {
        _control = true;

        if (!me.loading) {
          fnTouches(e);
          fnTouchstart(e, me);
        }
      });
      me.$element.on('mousemove touchmove', function (e) {
        if (!_control) {
          return;
        }

        if (!me.loading) {
          fnTouches(e, me);
          fnTouchmove(e, me);
        }
      });
      me.$element.on('mouseup touchend', function () {
        _control = false;

        if (!me.loading) {
          fnTouchend(me);
        }
      }); // 加载下方

      me.$scrollArea.on('scroll', function () {
        me._scrollTop = me.$scrollArea.scrollTop();
        fnRecoverContentHeight(me); // 滚动页面触发加载数据

        if (me.opts.loadDownFn != '' && !me.loading && !me.isLockDown && me._scrollContentHeight - me._threshold <= me._scrollWindowHeight + me._scrollTop) {
          loadDown(me);
        }
      });
    }; // touches


    function fnTouches(e) {
      if (!e.touches) {
        e.touches = e.originalEvent.touches || [{
          pageX: e.originalEvent.pageX,
          pageY: e.originalEvent.pageY
        }];
      }
    } // touchstart


    function fnTouchstart(e, me) {
      me._startY = e.touches[0].pageY; // 记住触摸时的scrolltop值

      me.touchScrollTop = me.$scrollArea.scrollTop();
    } // touchmove


    function fnTouchmove(e, me) {
      me._curY = e.touches[0].pageY;
      me._moveY = me._curY - me._startY;

      if (me._moveY > 0) {
        me.direction = 'down';
      } else if (me._moveY < 0) {
        me.direction = 'up';
      }

      var _absMoveY = Math.abs(me._moveY); // 加载上方


      if (me.opts.loadUpFn != '' && me.touchScrollTop <= 0 && me.direction == 'down' && !me.isLockUp) {
        e.preventDefault();
        me.$domUp = $('.' + me.opts.domUp.domClass); // 如果加载区没有DOM

        if (!me.upInsertDOM) {
          me.$element.prepend('<div class="' + me.opts.domUp.domClass + '"></div>');
          me.upInsertDOM = true;
        }

        fnTransition(me.$domUp, 0); // 下拉

        if (_absMoveY <= me.opts.distance) {
          me._offsetY = _absMoveY; // todo：move时会不断清空、增加dom，有可能影响性能，下同

          me.$domUp.html(me.opts.domUp.domRefresh); // 指定距离 < 下拉距离 < 指定距离*2
        } else if (_absMoveY > me.opts.distance && _absMoveY <= me.opts.distance * 2) {
          me._offsetY = me.opts.distance + (_absMoveY - me.opts.distance) * 0.5;
          me.$domUp.html(me.opts.domUp.domUpdate); // 下拉距离 > 指定距离*2
        } else {
          me._offsetY = me.opts.distance + me.opts.distance * 0.5 + (_absMoveY - me.opts.distance * 2) * 0.2;
        }

        me.$domUp.css({
          'height': me._offsetY
        });
      }
    } // touchend


    function fnTouchend(me) {
      var _absMoveY = Math.abs(me._moveY);

      if (me.opts.loadUpFn != '' && me.touchScrollTop <= 0 && me.direction == 'down' && !me.isLockUp) {
        fnTransition(me.$domUp, 300);

        if (_absMoveY > me.opts.distance) {
          me.$domUp.css({
            'height': me.$domUp.children().height()
          });
          me.$domUp.html(me.opts.domUp.domLoad);
          me.loading = true;
          me.opts.loadUpFn(me);
        } else {
          me.$domUp.css({
            'height': '0'
          }).on('webkitTransitionEnd mozTransitionEnd transitionend', function () {
            me.upInsertDOM = false;
            $(this).remove();
          });
        }

        me._moveY = 0;
      }
    } // 如果文档高度不大于窗口高度，数据较少，自动加载下方数据


    function fnAutoLoad(me) {
      if (me.opts.loadDownFn != '' && me.opts.autoLoad) {
        if (me._scrollContentHeight - me._threshold <= me._scrollWindowHeight) {
          loadDown(me);
        }
      }
    } // 重新获取文档高度


    function fnRecoverContentHeight(me) {
      // if (me.opts.scrollArea == win) {
      //     me._scrollContentHeight = $doc.height();
      // } else {
      //     me._scrollContentHeight = me.$element[0].scrollHeight;
      // }
      me._scrollContentHeight = me.$element[0].scrollHeight;
    } // 加载下方


    function loadDown(me) {
      if (me.isData) {
        me.direction = 'up';
        me.$domDown.html(me.opts.domDown.domLoad);
        me.loading = true;
        me.opts.loadDownFn(me);
      }
    } // 锁定


    MyDropLoad.prototype.lock = function (direction) {
      var me = this; // 如果不指定方向

      if (direction === undefined) {
        // 如果操作方向向上
        if (me.direction == 'up') {
          me.isLockDown = true; // 如果操作方向向下
        } else if (me.direction == 'down') {
          me.isLockUp = true;
        } else {
          me.isLockUp = true;
          me.isLockDown = true;
        } // 如果指定锁上方

      } else if (direction == 'up') {
        me.isLockUp = true; // 如果指定锁下方
      } else if (direction == 'down') {
        me.isLockDown = true; // 为了解决DEMO5中tab效果bug，因为滑动到下面，再滑上去点tab，direction=down，所以有bug
        // me.direction = 'up';
      }
    }; // 解锁


    MyDropLoad.prototype.unlock = function () {
      var me = this; // 简单粗暴解锁

      me.isLockUp = false;
      me.isLockDown = false; // 为了解决DEMO5中tab效果bug，因为滑动到下面，再滑上去点tab，direction=down，所以有bug
      // me.direction = 'up';
    }; // 无数据


    MyDropLoad.prototype.noData = function (flag) {
      var me = this;

      if (flag === undefined || flag == true) {
        me.isData = false;
      } else if (flag == false) {
        me.isData = true;
      }
    }; // 重置


    MyDropLoad.prototype.resetload = function () {
      var me = this;

      if (me.direction == 'down' && me.upInsertDOM) {
        me.$domUp.css({
          'height': '0'
        }).on('webkitTransitionEnd mozTransitionEnd transitionend', function () {
          me.loading = false;
          me.upInsertDOM = false;
          $(this).remove();
          fnRecoverContentHeight(me);
        });
      } else if (me.direction == 'up') {
        me.loading = false;
      } // 如果有数据


      if (me.isData) {
        // 加载区修改样式
        me.$domDown.html(me.opts.domDown.domRefresh);
        fnRecoverContentHeight(me);
        fnAutoLoad(me);
      } else {
        // 如果没数据
        me.$domDown.html(me.opts.domDown.domNoData);
      }
    };
    /**
       * 手动出发下拉
       */


    MyDropLoad.prototype.refresh = function () {
      var me = this; // 如果加载区没有DOM

      if (!me.upInsertDOM) {
        me.$element.prepend('<div class="' + me.opts.domUp.domClass + '"></div>');
        me.upInsertDOM = true;
      }

      me.direction = 'down';
      me.$domUp = $('.' + me.opts.domUp.domClass).html(me.opts.domUp.domLoad).css('transition', 'linear .1s');
      me.$domUp.css({
        'height': me.$domUp.children().height()
      });
      me.loading = true;
      me.opts.loadUpFn(me);
    }; // css过渡


    function fnTransition(dom, num) {
      dom.css({
        '-webkit-transition': 'all ' + num + 'ms',
        'transition': 'all ' + num + 'ms'
      });
    }
  })(window.Zepto || window.jQuery);

  var Config = function () {
    var Config = {
      default_avatar: BASE.DEFAULT_AVATAR || "/shijiwxy/weixin/images/defaultHead.jpg",
      _page: {
        detail: "./detail.html?d={D_id}&a={A_id}&ad={AD_id}"
      },
      page: function page(name) {
        var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        if (!Config._page.hasOwnProperty(name)) {
          return "";
        }

        var page = Config._page[name];
        page = page.replace(/{(\w+)}/g, function (match, name, index, str, callee) {
          return params[name] || "";
        });
        return page;
      },
      api: {
        base_url: BASE.API_BASE_URL || ""
      },
      user: {
        token: BASE.TOKEN,
        udid: BASE.UDID,
        id: BASE.USER_ID,
        org_id: BASE.ORG_ID,
        version: BASE.VERSION
      }
    };
    return Config;
  }();
  /**
   * 工具层
   */


  var Util = function () {
    var Util = function Util() {};

    var _this = Util.prototype;
    /**
     * 下载文件
     * url：下载文件的远程地址
     */

    _this.download = function (url) {
      window.open(url);
    };
    /**
     * 判断数据的具体类型
     */


    _this.type = function (mixin) {
      if (mixin == null) {
        return mixin + "";
      }

      var class2type = {
        '[object Boolean]': 'boolean',
        '[object Number]': 'number',
        '[object String]': 'string',
        '[object Function]': 'function',
        '[object Array]': 'array',
        '[object Date]': 'date',
        '[object RegExp]': 'regexp',
        '[object Object]': 'object',
        '[object Error]': 'error',
        '[object Symbol]': 'symbol'
      };

      var mixin_type = _typeof(mixin);

      if (mixin_type === 'undefined') {
        return 'undefined';
      }

      if (mixin_type === 'object' || mixin_type === "function") {
        var _type = class2type[Object.prototype.toString.call(mixin)];

        if (!_type) {
          return _this.isDom(mixin) ? "dom" : "object";
        } else {
          return _type;
        } // return class2type[Object.prototype.toString.call(mixin)] || "object";

      }

      return mixin_type;
    };
    /**
     * 获取路由切换的完整地址
     */


    _this.getRealPath = function () {
      var path = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      if (path == '') {
        return Router.route;
      }

      if (path.indexOf('/') == 0) {
        return path;
      }

      var hash = this.getHashSimplePath(Router.route);
      var hashs = hash == '' ? [] : hash.split('/');

      var _paths = path == '' ? [] : path.split('/');

      var paths = [];

      for (var i in _paths) {
        if (_paths[i] != hashs[i]) {
          paths = _paths.splice(i);
          break;
        }
      }

      for (var _i = 0; _i < paths.length; _i++) {
        if (paths[_i] == '') {
          continue;
        }

        if (paths[_i] == '.') {
          continue;
        }

        if (paths[_i] == '..') {
          hashs.pop();
          continue;
        }

        hashs.push(paths[_i]);
      }

      return hashs.join('/');
    };
    /**
     * 获取某元素以浏览器左上角为原点的坐标
     */


    _this.offset = function (dom) {
      var top = dom.offsetTop;
      var left = dom.offsetLeft;
      var width = dom.offsetWidth;
      var height = dom.offsetHeight;

      while (dom = dom.offsetParent) {
        top += dom.offsetTop;
        left += dom.offsetLeft;
      }

      return {
        top: top,
        left: left,
        width: width,
        height: height
      };
    };
    /**
     * 判断传入的变量是否是一个dom对象
     */


    _this.isDom = function (dom) {
      return (typeof HTMLElement === "undefined" ? "undefined" : _typeof(HTMLElement)) === 'object' ? dom instanceof HTMLElement : dom && _typeof(dom) === 'object' && dom.nodeType === 1 && typeof dom.nodeName === 'string';
    };
    /**
     * 创建上拉下拉动作
     */


    _this.scroll = function (DOM) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      options = Object.assign({
        refer: window,
        onRefresh: function onRefresh(done) {
          done();
        },
        onContinue: function onContinue(done) {
          done();
        }
      }, options);
      var DOMdropload = $(DOM).dropload({
        scrollArea: options.refer,
        loadDownFn: function loadDownFn(me) {
          me.lock('up');
          options.onContinue(function () {
            var noData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            me.unlock();
            me.noData(noData);
            me.resetload();
          });
        },
        loadUpFn: function loadUpFn(me) {
          me.lock('down');
          options.onRefresh(function () {
            var noData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            me.unlock();
            me.noData(noData);
            me.resetload();
          });
        }
      });
      return DOMdropload;
    };
    /**
     * 从1开始的对象，遍历
     * @param {Object} maps
     * @param {Function} callback
     */


    _this.likeItemObjectMap = function (maps, callback) {
      var i = 0;
      var result = [];
      var map; // eslint-disable-next-line no-constant-condition

      while (true) {
        i += 1;

        if (!maps.hasOwnProperty(i)) {
          break;
        }

        map = callback(maps[i], i, map);

        if (map === true) {
          continue;
        }

        if (map === false) {
          break;
        }

        result.push(map);
      }

      return result;
    };

    _this.getQuery = function () {
      var query = {};
      location.search.slice(1).split("&").map(function (item) {
        var srt = item.split("=");

        if (srt[0] != "") {
          query[srt[0]] = srt[1];
        }
      });
      return query;
    };
    /**
     * 是否为空值，不包括0
     */


    _this.isEmpty = function (mixin) {
      var _type = _this.type(mixin);

      if (["null", "undefined"].includes(_type)) {
        return true;
      }

      if (_type == "boolean" && mixin == false) {
        return true;
      }

      if (_type == "array" && mixin.length == 0) {
        return true;
      }

      if (_type == "object" && Object.keys(mixin).length == 0) {
        return true;
      }

      return mixin === "";
    }; //金额输入框实时大写


    _this.convertCurrency = function (money) {
      //汉字的数字
      var cnNums = new Array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"); //基本单位

      var cnIntRadice = new Array("", "拾", "佰", "仟"); //对应整数部分扩展单位

      var cnIntUnits = new Array("", "万", "亿", "兆"); //对应小数部分单位

      var cnDecUnits = new Array("角", "分", "毫", "厘"); //整数金额时后面跟的字符

      var cnInteger = "整"; //整型完以后的单位

      var cnIntLast = "元"; //最大处理的数字

      var maxNum = 999999999999999.9999; //金额整数部分

      var integerNum; //金额小数部分

      var decimalNum; //输出的中文金额字符串

      var chineseStr = ""; //分离金额后用的数组，预定义

      var parts;

      if (money == "") {
        return "";
      }

      if ((money + "").length > 15) {
        return "大写转换最多支持15位数的金额！";
      }

      money = parseFloat(money);

      if (money >= maxNum) {
        //超出最大处理数字
        return "";
      }

      if (money == 0) {
        chineseStr = cnNums[0] + cnIntLast + cnInteger;
        return chineseStr;
      } //转换为字符串


      money = money.toString();

      if (money.indexOf(".") == -1) {
        integerNum = money;
        decimalNum = "";
      } else {
        parts = money.split(".");
        integerNum = parts[0];
        decimalNum = parts[1].substr(0, 4);
      } //获取整型部分转换


      if (parseInt(integerNum, 10) > 0) {
        var zeroCount = 0;
        var IntLen = integerNum.length;

        for (var i = 0; i < IntLen; i++) {
          var n = integerNum.substr(i, 1);
          var p = IntLen - i - 1;
          var q = p / 4;
          var m = p % 4;

          if (n == "0") {
            zeroCount++;
          } else {
            if (zeroCount > 0) {
              chineseStr += cnNums[0];
            } //归零


            zeroCount = 0;
            chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
          }

          if (m == 0 && zeroCount < 4) {
            chineseStr += cnIntUnits[q];
          }
        }

        chineseStr += cnIntLast;
      } //小数部分


      if (decimalNum != "") {
        var decLen = decimalNum.length;

        for (var j = 0; j < decLen; j++) {
          var nn = decimalNum.substr(j, 1);

          if (nn != "0") {
            chineseStr += cnNums[Number(nn)] + cnDecUnits[j];
          }
        }
      }

      if (chineseStr == "") {
        chineseStr += cnNums[0] + cnIntLast + cnInteger;
      } else if (decimalNum == "") {
        chineseStr += cnInteger;
      }

      return chineseStr;
    }; // textarea不回弹


    Util.prototype.iosTextBlurScroll = function (input) {
      if (!input) {
        return false;
      }

      var trueHeight = document.body.scrollHeight; //解决ios唤起键盘后留白

      var backPageSize = function backPageSize() {
        setTimeout(function () {
          window.scroll(0, trueHeight - 10);
          window.innerHeight = window.outerHeight = trueHeight;
        }, 200);
      };

      input.onblur = backPageSize; // onblur是核心方法
    };
    /**
     * 数字前补0变为字符串数字
     */


    _this.fullZeroNumber = function (number) {
      var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

      var __number = number + "";

      if (isNaN(__number)) {
        return number;
      }

      while (__number.length < size) {
        __number = "0" + __number;
      }

      return __number;
    };
    /**
     * 获取设置时间的小时分钟秒
     */


    _this.getCalendarDate = function () {
      var ND = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Date();

      if (_this.type(ND) == "string") {
        ND = ND.replace(/-/g, "/");
      }

      if (_this.isEmpty(ND)) {
        ND = new Date();
      } else {
        ND = new Date(ND);
      }

      var hour = ND.getHours();
      var minute = ND.getMinutes();
      var second = ND.getSeconds();
      var timestamp = ND.getTime();
      ND = new Date(ND.getFullYear(), ND.getMonth(), ND.getDate());
      var time = ND.getTime();
      var NW = {
        ND: ND,
        year: ND.getFullYear(),
        month: _this.fullZeroNumber(ND.getMonth() + 1),
        day: _this.fullZeroNumber(ND.getDate()),
        hour: _this.fullZeroNumber(hour),
        minute: _this.fullZeroNumber(minute),
        second: _this.fullZeroNumber(second),
        time: time,
        timestamp: timestamp
      };
      NW.format = NW.year + "/" + NW.month + "/" + NW.day;
      NW.formatText = NW.year + "年" + NW.month + "月" + NW.day + "日";
      NW.monthFormat = NW.year + "/" + NW.month;
      NW.monthFormatText = NW.year + "年" + NW.month + "月";
      NW.timeFormat = NW.hour + ":" + NW.minute + ":" + NW.second;
      NW.timeFormatText = NW.hour + "时" + NW.minute + "分" + NW.second + "秒";
      NW.minuteTimeFormat = NW.hour + ":" + NW.minute;
      NW.minuteTimeFormatText = NW.hour + "时" + NW.minute + "分"; // 获取当月天数，day=0时month必须+1

      NW.monthDay = _this.fullZeroNumber(new Date(ND.getFullYear(), ND.getMonth() + 1, 0).getDate());
      NW.firstWeek = new Date(ND.getFullYear(), ND.getMonth()).getDay();
      NW.firstTime = new Date(ND.getFullYear(), ND.getMonth()).getTime();
      return NW;
    };

    return new Util();
  }();

  var Render = function () {
    var Render = function Render() {};

    var _this = Render.prototype;

    _this.getContainer = function () {
      var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      var DOM = _this.create('<div id="' + id + '"></div>');

      return {
        el: DOM,
        // 支持格式： 
        // append("")  append(dom) append(Component) append([dom1, dom2, Component])
        // append(Component, dom)
        // 这种不行：
        // append("", [dom]) append([Template], "")
        append: function append(template) {
          var templates = Array.prototype.slice.call(arguments); // 如果第一个参数传了数组，后面的参数无效

          if (Util.type(template) == "array") {
            templates = template;
          }

          templates.map(function (template) {
            var type = Util.type(template);

            if (Util.isEmpty(template)) {
              return false;
            }

            if (type == "string") {
              template = template.trim();

              if (template.length == 0) {
                return false;
              }

              template = _this.create(template);
              type = "dom";
            }

            if (type == "dom") {
              return template;
            }

            if (Util.type(template.el) == "dom") {
              return template.el;
            }

            return false;
          }).filter(function (dom) {
            return dom !== false;
          }).map(function (dom) {
            DOM.appendChild(dom);
          });
          return this;
        },
        render: function render(selector) {
          if (typeof selector == 'undefined') {
            var original = document.querySelector('#' + id);
            original.parentNode.replaceChild(DOM, original);
          } else {
            var parent = document.querySelector(selector);
            parent.append(DOM);
          }

          setTimeout(function () {
            DOM.className = (DOM.className + " on").trim();
          }, 100);
        }
      };
    };
    /**
     * 通过字符串模板创建DOM
     * @param {String} string 字符串模板，模板必须包含一个最外层标签
     */


    _this.create = function (string) {
      var Element = document.createElement('div');
      string = string.trim();
      var wrapMap = {
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
      };
      var tag = (/<([a-z][^\/\0>\x20\t\r\n\f]*)/i.exec(string) || ["", ""])[1].toLowerCase();
      var wrap = wrapMap[tag] || wrapMap._default;
      Element.innerHTML = wrap[1] + string + wrap[2];
      var j = wrap[0];

      while (j--) {
        Element = Element.lastChild;
      }

      return Element.firstChild;
    };

    _this.compile = function (DOM, template) {
      var event = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
      var Element = this.create(template);
      event && event(Element);

      if (typeof DOM == 'string') {
        DOM = document.querySelector(DOM);
      }

      DOM.appendChild(Element);
    };

    return new Render();
  }();

  var Component = function () {
    var Component = function Component() {};

    var _this = Component.prototype;
    /**
           * 获取定制数组重构方法
           */

    function getArrayArgumentations(callback) {
      var aryMethods = ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'];
      var arrayArgumentations = [];
      aryMethods.forEach(function (method) {
        var original = Array.prototype[method];

        arrayArgumentations[method] = function () {
          var result = original.apply(this, arguments);
          callback && callback(method);
          return result;
        };
      }); // 清空数组只保留项数

      arrayArgumentations.clear = function () {
        var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        this.length = length;
        callback && callback('clear');
      };

      return arrayArgumentations;
    }

    ;
    /**
           * 软提示
           */

    var _CACHE_TOAST_DOMS;

    _this.toast = function (data) {
      if (typeof data == 'string') {
        data = {
          text: data
        };
      }

      var _data = Object.assign({
        // 提示文字，必填
        text: '',
        // 消失延时毫秒数
        delay: 3000
      }, data);

      if (_data.text == '') {
        return false;
      }

      var DOM = Render.create("<div class=\"toast\">\n    \n                <div class=\"toast-box\">".concat(_data.text, "</div>\n    \n            </div>"));
      return {
        el: DOM,
        render: function render(callback) {
          // 清除上一次显示的DOM
          if (_CACHE_TOAST_DOMS) {
            _CACHE_TOAST_DOMS.parentNode.removeChild(_CACHE_TOAST_DOMS);
          }

          _CACHE_TOAST_DOMS = DOM;
          $("body").addClass('clamp');
          setTimeout(function () {
            $(DOM).addClass('on');
          }, 0);
          setTimeout(function () {
            callback && callback();
            $("body").removeClass('clamp');
            $(DOM).removeClass('on');
            setTimeout(function () {
              DOM.parentNode.removeChild(DOM);
              _CACHE_TOAST_DOMS = undefined;
            }, _data.delay + 1000);
          }, _data.delay);
          $("body").append(DOM);
        }
      };
    };
    /**
           * 加载中
           */


    var _CACHE_LOADING_DOMS;

    _this.loading = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "加载中";

      if (typeof data == 'string') {
        data = {
          text: data
        };
      }

      var _data = Object.assign({
        // 提示文字，必填
        text: ""
      }, data);

      var DOM = Render.create("<div class=\"loading\">\n    \n                <div class=\"loading-box\">\n    \n                    <i class=\"fa fa-spinner fa-spin\"></i>\n    \n                    ".concat(_data.text, "\n    \n                </div>\n    \n            </div>"));
      return {
        el: DOM,
        render: function render() {
          // 清除上一次显示的DOM
          if (_CACHE_LOADING_DOMS) {
            _CACHE_LOADING_DOMS.parentNode.removeChild(_CACHE_LOADING_DOMS);
          }

          _CACHE_LOADING_DOMS = DOM;
          $('body').addClass('clamp');
          setTimeout(function () {
            $(DOM).addClass('on');
          }, 0);
          $('body').append(DOM);
          return {
            hide: function hide(callback) {
              $('body').removeClass('clamp');
              $(DOM).removeClass('on');
              setTimeout(function () {
                DOM.parentNode.removeChild(DOM);
                _CACHE_LOADING_DOMS = undefined;
                callback && callback();
              }, 300);
            }
          };
        }
      };
    };
    /**
           * 模态框
           */


    _this.media = function (data) {
      if (typeof data == 'string') {
        data = {
          component: ""
        };
      }

      var _data = Object.assign({
        title: "无标题",
        closeConfirm: false,
        body: undefined,
        onComplete: function onComplete(data, done) {
          done();
        }
      }, data);

      if (_data.text == '') {
        return false;
      }

      var DOM = Render.create("<div class=\"media\">\n    \n                <div class=\"media-box\">\n    \n                    <div class=\"media--header\">\n    \n                        <span>".concat(_data.title, "</span>\n    \n                        <span class=\"media--close\"><i class=\"fa fa-times\"></i></span>\n    \n                    </div>\n    \n                    <div class=\"media--body\"></div>\n    \n                    <div class=\"media--footer\">\n    \n                        <button type=\"button\" class=\"btn btn-small media--close\">\u53D6\u6D88</button>\n    \n                        <button type=\"button\" class=\"btn btn-small btn-primary media--complete\">\u786E\u5B9A</button>\n    \n                    </div>\n    \n                </div>\n    \n            </div>"));
      $(DOM).find(".media--body").append(_data.body);
      $(DOM).on("close", function () {
        $("body").removeClass('clamp');
        $(DOM).removeClass("on");
        setTimeout(function () {
          DOM.parentNode.removeChild(DOM);
        }, 300);
      });
      $(DOM).on("complete", function (e) {
        var data = {};
        $(_data.body).find("[data-media-form-name]").each(function (index, item) {
          var $item = $(item);
          var key = $item.attr("data-media-form-name");
          var value = $item.attr("data-media-form-value") || $item.val() || "";
          data[key] = value;
        });

        _data.onComplete(data, function () {
          $(DOM).trigger("close");
        });
      });
      $(DOM).find(".media--close").on("click", function () {
        if (_data.closeConfirm) {
          var confirm_text = "确定关闭此弹窗？";

          if (typeof _data.closeConfirm == "string") {
            confirm_text = _data.closeConfirm;
          }

          if (!window.confirm(confirm_text)) {
            return false;
          }
        }

        $(DOM).trigger("close");
      });
      $(DOM).find(".media--complete").on("click", function () {
        $(DOM).trigger("complete");
      });
      _data.el = DOM;

      _data.render = function () {
        $("body").addClass('clamp');
        setTimeout(function () {
          $(DOM).addClass('on');
        }, 0);
        $("body").append(DOM);
        return _data;
      };

      return _data;
    };
    /**
           * 确认框
           */


    _this.confirm = function () {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "继续操作？";
      var onClick = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
      var DOM = Render.create("<div class=\"confirm\">\n    \n                <div class=\"confirm-box\">\n    \n                    <div class=\"confirm--title\">".concat(message, "</div>\n    \n                    <div class=\"confirm--button\">\n    \n                        <span class=\"confirm-close\">\u53D6\u6D88</span>\n    \n                        <span class=\"confirm-complete\">\u786E\u5B9A</span>\n    \n                    </div>\n    \n                </div>\n    \n            </div>"));
      $(DOM).on("close", function (e, onClick) {
        console.log(arguments);
        $(this).removeClass("on");
        setTimeout(function () {
          DOM.parentNode.removeChild(DOM);
          onClick && onClick();
        }, 300);
      });
      $(DOM).find(".confirm-close").on("click", function () {
        $(this).trigger("close");
      });
      $(DOM).find(".confirm-complete").on("click", function () {
        $(this).trigger("close", onClick);
      });
      return {
        render: function render() {
          setTimeout(function () {
            $(DOM).addClass("on");
          }, 0);
          $("body").append(DOM);
        }
      };
    };
    /**
           * 抽屉
           */


    var _CACHE_DRAWER_DOMS = null;

    _this.drawer = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        color: "primary",
        // +yk: 扩展栏
        stretch: null,
        is_show: false,
        body: "",
        closeByBlank: true,
        onComplete: function onComplete() {},
        onClose: function onClose() {}
      }, data);

      var DOM = Render.create("<div class=\"half-screen-dialog\">\n    \n                <div class=\"half-screen-dialog-box\">\n    \n                    <div class=\"half-screen-dialog--header\"></div>\n    \n                    <div class=\"half-screen-dialog--body\"></div>\n    \n                    <div class=\"half-screen-dialog--footer\">\n    \n                        <button class=\"btn btn-".concat(_data.color, " btn-half-round screen-complete\">\u786E\u5B9A</button>\n    \n                    </div>\n    \n                </div>\n    \n            </div>"));
      var $Half_screen_dialog__body = $(DOM).find('.half-screen-dialog--body');
      var $Half_screen_dialog__footer = $(DOM).find('.half-screen-dialog--footer');

      if (_data.stretch) {
        $Half_screen_dialog__footer.prepend(_data.stretch);
        $Half_screen_dialog__footer.children(".btn").addClass("btn-small");
      } // 加载数据


      $Half_screen_dialog__body.append(_data.body); // 切换显示隐藏

      Object.defineProperty(data, 'is_show', {
        get: function get() {
          return _data.is_show;
        },
        set: function set(val) {
          _data.is_show = !!val;

          if (_data.is_show) {
            $('body').addClass('clamp');
            $(DOM).addClass('on');
            $(DOM).find('.half-screen-dialog-box').animate({
              right: 0
            });
          } else {
            $('body').removeClass('clamp');
            $(DOM).find('.half-screen-dialog-box').animate({
              right: '-100%'
            });
            $(DOM).removeClass('on');
            setTimeout(function () {
              if (_CACHE_DRAWER_DOMS) {
                _CACHE_DRAWER_DOMS.parentNode.removeChild(_CACHE_DRAWER_DOMS);

                _CACHE_DRAWER_DOMS = null;
              }
            }, 600);
          }
        }
      }); // 点击背景时

      $(DOM).on('click', function () {
        if (_data.closeByBlank) {
          data.is_show = false;

          _data.onClose.call(data);
        }
      }); // 禁止冒泡点击

      $(DOM).children().on('click', function (e) {
        e.stopPropagation();
      }); // 点击确定

      $(DOM).find('.screen-complete').on('click', function () {
        // 发送数据通知
        var result = _data.onComplete.call(data); // 只有明确返回false时才不会关闭抽屉


        if (result !== false) {
          // 关闭显示层
          data.is_show = false;
        }
      });
      data.el = DOM;

      data.render = function (frame_selector) {
        // 清除上一次显示的DOM
        if (_CACHE_DRAWER_DOMS) {
          _CACHE_DRAWER_DOMS.parentNode.removeChild(_CACHE_DRAWER_DOMS);
        }

        _CACHE_DRAWER_DOMS = DOM;
        document.body.append(DOM);
        setTimeout(function () {
          data.is_show = true;
        }, 0);
        console.log(data);
      };

      return data;
    };
    /**
     * 顶部选择tab
     */


    _this.navbar = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        active: 'wait',
        onClick: function onClick() {}
      }, data);

      var DOM = Render.create("<div class=\"navbar\">\n                <ul class=\"navbar-box\">\n                    <li class=\"navbar--item\" data-id=\"wait\"><span>\u5F85\u6211\u5BA1\u6279</span></li>\n                    <li class=\"navbar--item\" data-id=\"readed\"><span>\u6211\u5DF2\u5BA1\u6279</span></li>\n                    <li class=\"navbar--item\" data-id=\"copymy\"><span>\u6284\u9001\u6211\u7684</span></li>\n                </ul>\n            </div>");
      Object.defineProperty(data, 'active', {
        get: function get() {
          return _data.active;
        },
        set: function set(val) {
          _data.active = val;
        }
      }); // 选中默认

      $(DOM).find('.navbar--item[data-id="' + _data.active + '"]').addClass('active');
      $(DOM).find('.navbar--item').on('click', function () {
        $(DOM).find('.navbar--item').removeClass('active');
        $(this).addClass('active');
        data.active = $(this).attr('data-id');

        _data.onClick(data.active);
      });
      data.el = DOM;
      return data;
    };
    /**
     * 工具框
     */


    _this.toolbar = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        // 是否可点全部已读（在列表全部为已读状态下的值是true，页面显示灰色）
        all_readed: true,
        // 是否打开搜索框
        isOpenSearch: false,
        openClickSearch: function openClickSearch() {},
        closeClickSearch: function closeClickSearch() {},
        screenClick: function screenClick() {},
        readClick: function readClick() {}
      }, data);

      var DOM = Render.create("<div class=\"toolbar\">\n                <ul class=\"toolbar-box\">\n                    <li class=\"toolbar--item search\"><i class=\"fa fa-search\"></i> \u641C\u7D22</li>\n                    <li class=\"toolbar--item screen\"><i class=\"fa fa-filter\"></i> \u7B5B\u9009</li>\n                    <li class=\"toolbar--item set-read\">\u5168\u90E8\u5DF2\u8BFB</li>\n                </ul>\n            </div>");
      var $Toolbar__item_search = $(DOM).find('.search');
      var $Toolbar__item_screen = $(DOM).find('.screen');
      var $Toolbar__item_set_read = $(DOM).find('.set-read');
      Object.defineProperty(data, 'openClickSearch', {
        enumerable: false,
        get: function get() {
          return _data.openClickSearch;
        }
      });
      Object.defineProperty(data, 'closeClickSearch', {
        enumerable: false,
        get: function get() {
          return _data.closeClickSearch;
        }
      });
      Object.defineProperty(data, 'isOpenSearch', {
        get: function get() {
          return _data.isOpenSearch;
        },
        set: function set(val) {
          _data.isOpenSearch = !!val;

          if (_data.isOpenSearch) {
            $Toolbar__item_search.html('<i class="fa fa-search"></i> 取消搜索');
            data.openClickSearch();
          } else {
            $Toolbar__item_search.html('<i class="fa fa-search"></i> 搜索');
            data.closeClickSearch();
          }
        }
      });

      var allReadedSetter = function allReadedSetter(val) {
        if (val) {
          $Toolbar__item_set_read.hide(); // $Toolbar__item_set_read.addClass('disabled');
        } else {
          $Toolbar__item_set_read.show(); // $Toolbar__item_set_read.removeClass('disabled');
        }
      };

      Object.defineProperty(data, 'all_readed', {
        get: function get() {
          return _data.all_readed;
        },
        set: function set(val) {
          _data.all_readed = val;
          allReadedSetter(data.all_readed);
        }
      });
      allReadedSetter(data.all_readed);
      $Toolbar__item_search.on('click', function () {
        data.isOpenSearch = !data.isOpenSearch;
      });
      $Toolbar__item_screen.on('click', function () {
        data.screenClick();
      });
      $Toolbar__item_set_read.on('click', function () {
        if (!data.all_readed) {
          data.readClick();
        }
      });
      data.el = DOM;
      return data;
    };
    /**
     * 搜索框
     */


    _this.searchbar = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        // 是否在is_show=true打开时自动聚焦文本框
        auto_focus: false,
        is_show: false,
        search: '',
        onSubmit: function onSubmit(val) {}
      }, data);

      var DOM = Render.create("<div class=\"searchbar\">\n                <div class=\"searchbar-box\">\n                    <div class=\"searchbar--write\">\n                        <input type=\"text\" class=\"search--write-input\" placeholder=\"\u641C\u7D22\u6807\u9898\u5185\u5BB9\">\n                    </div>\n                    <div class=\"searchbar--btn\">\n                        <a href=\"javascript:;\" class=\"search--btn-link\">\u641C\u7D22</a>\n                    </div>\n                </div>\n            </div>"); // 设置显示隐藏

      _data.is_show ? $(DOM).addClass('on') : $(DOM).removeClass('on');
      Object.defineProperty(data, 'is_show', {
        get: function get() {
          return _data.is_show;
        },
        set: function set(val) {
          _data.is_show = !!val;

          if (_data.is_show) {
            $(DOM).addClass('on');

            if (_data.auto_focus) {
              setTimeout(function () {
                $(DOM).find('.search--write-input').trigger('focus');
              }, 200);
            }
          } else {
            $(DOM).removeClass('on');
          }
        }
      }); // input输入监听

      var $Searchbar__write_input = $(DOM).find('.search--write-input');
      var $Search__btn_link = $(DOM).find('.search--btn-link');
      $Searchbar__write_input.val(_data.search);
      Object.defineProperty(data, 'search', {
        get: function get() {
          return _data.search;
        },
        set: function set(val) {
          _data.search = val;
          $Searchbar__write_input.val(_data.search);
          return val;
        }
      });
      $Searchbar__write_input.on('change', function () {
        data.search = this.value;
      });
      $Search__btn_link.on('click', function () {
        _data.onSubmit.call(_data);
      });
      data.el = DOM;
      return data;
    };
    /**
     * 列表
     */


    _this.list = function (data) {
      var _this2 = this;

      /**
       * 对返回数据统一过滤引擎方法
       */
      var request_filter = function request_filter(data, filter) {
        if (Util.type(filter) == 'function') {
          data = data.map(function (item) {
            var data = filter.apply(this, arguments);

            if (Util.type(data) == 'undefined' || Util.type(data) == 'null') {
              return item;
            }

            return data;
          }); // map后不是数组的项删除掉

          data = data.filter(function (item) {
            return Util.type(item) == 'object';
          });
        }

        return data;
      };

      var st = null;

      var _data = Object.assign({
        list: [],
        api: '',
        page: 1,
        size: 10,
        query_id: '',
        title: '',
        is_read: '',
        model_id: '',
        status: '',
        onLoad: function onLoad(exec, type) {},
        // 下拉时出发重新渲染数据
        onRefresh: function onRefresh(done) {
          if (data.api == '') {
            return;
          }

          if (Util.type(_data.onLoad) != 'function') {
            return;
          }

          data.page = 1;
          var request = {
            page: data.page,
            size: data.size,
            title: data.title,
            model_id: data.model_id,
            is_read: data.is_read,
            status: data.status,
            query_id: data.query_id
          };

          _data.onLoad(function (filter) {
            Api[data.api](request, function (result) {
              var _data$list;

              result.data.data = request_filter(result.data.data, filter);
              data.list.clear();

              (_data$list = data.list).push.apply(_data$list, _toConsumableArray(result.data.data));

              data.page += 1; // 小于预定个数时，视为没有以后数据，noData=true

              done(result.data.data.length < _data.size);
            }, function (e) {
              _this.toast(e.message).render();

              done(true);
            });
          }, 'refresh');
        },
        // 上拉时出发继续加载后续
        onContinue: function onContinue(done) {
          if (data.api == '') {
            return;
          }

          if (Util.type(_data.onLoad) != 'function') {
            return;
          }

          var request = {
            page: data.page,
            size: data.size,
            title: data.title,
            model_id: data.model_id,
            is_read: data.is_read,
            status: data.status,
            query_id: data.query_id
          };

          _data.onLoad(function (filter) {
            clearTimeout(st);
            st = setTimeout(function () {
              Api[data.api](request, function (result) {
                var _data$list2;

                result.data.data = request_filter(result.data.data, filter);

                if (data.page == 1) {
                  data.list.clear();
                }

                (_data$list2 = data.list).push.apply(_data$list2, _toConsumableArray(result.data.data));

                data.page += 1; // 小于预定个数时，视为没有以后数据，noData=true

                done(result.data.data.length < data.size);
              }, function (e) {
                _this.toast(e.message).render();

                done(true);
              });
            }, 800);
          }, 'continue');
        }
      }, data);
      /**
       * 统一注册
       */


      (function (names) {
        names.map(function (name) {
          Object.defineProperty(data, name, {
            set: function set(val) {
              _data[name] = val;
            },
            get: function get() {
              return _data[name];
            }
          });
        });
      })(['title', 'model_id', 'status', 'is_read', 'api', 'page', 'size', 'query_id']);

      data.list = [];
      data._cache_list_length = 0;
      var DOM = Render.create("<div class=\"list\"><ul class=\"list-box\"></ul></div>");
      var $List_box = $(DOM).find('.list-box');
      /**
       * 监听数组变化
       */

      data.list.__proto__ = getArrayArgumentations(function (method) {
        // push只向后追加
        var _list = [];

        if (method == 'push') {
          _list = data.list.slice(data._cache_list_length);

          _list.forEach(function (item) {
            $List_box.append(_this2.listItem(item).el);
          });
        } else {
          $List_box.empty();
          data.list.forEach(function (item) {
            $List_box.append(_this2.listItem(item).el);
          });
        }

        data._cache_list_length = data.list.length;
      }); // 添加下拉上拉监听

      var Scroll = Util.scroll(DOM, {
        onRefresh: _data.onRefresh.bind(data),
        onContinue: _data.onContinue.bind(data)
      });

      data.refresh = function () {
        Scroll.refresh();
      };

      data.resetForm = function () {
        data.title = '';
        data.model_id = '';
        data.status = '';
        data.page = 1;
        data.query_id = '';
      };

      data.el = DOM;
      data.scroll = Scroll;
      return data;
    };
    /**
     * 列表item
     */


    _this.listItem = function (data) {
      var DOM = Render.create("<li class=\"list--item\">\n                <div class=\"list--item-box\">\n                    <a href=\"".concat(Config.page('detail', {
        D_id: data.D_id,
        A_id: data.A_id,
        AD_id: data.AD_id
      }), "\" class=\"list--item-link\" data-id=\"").concat(data.id, "\">\n                        <div class=\"list--item--title\">\n                            <span class=\"list--item--title-text\">\n                                <span>").concat(data.title, "</span>\n                            </span>\n                            <span class=\"list--item--title-time\">").concat(data.time, "</span>\n                        </div>\n                        <div class=\"list--item--content\">\n                            <div class=\"list--item--content-avatar\">\n                                <img src=\"").concat(data.avatar, "\" alt=\"\" srcset=\"\">\n                            </div>\n                            <div class=\"list--item--content-main\">\n                                <p class=\"list--item--content--title\">\n                                    <span>").concat(data.type, "</span>\n                                </p>\n                            </div>\n                        </div>\n                    </a>\n                </div>\n            </li>")); // 是否已读

      if (data.is_unreaded === true) {
        var BadgeDOM = Render.create("<span class=\"badge badge-danger\"></span>");
        $(DOM).find('.list--item--title-text').append(BadgeDOM);
        $(DOM).find(".list--item-link").on("click", function () {
          if (BadgeDOM) {
            BadgeDOM.parentNode.removeChild(BadgeDOM);
            BadgeDOM = null;
          }
        });
      } // 详情


      var $list__item__content_main = $(DOM).find('.list--item--content-main');
      data.extra.forEach(function (extra) {
        $list__item__content_main.append("<p>".concat(extra, "</p>"));
      }); // 状态

      switch (data.status) {
        case 'approval':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-info\">\u5BA1\u6279\u4E2D</span>");
          break;

        case 'success':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-success\">\u5DF2\u901A\u8FC7</span>");
          break;

        case 'refuse':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-danger\">\u5DF2\u62D2\u7EDD</span>");
          break;

        case 'cancel':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-warning\">\u5DF2\u64A4\u9500</span>");
          break;

        case 'stop':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-dark\">\u5DF2\u7EC8\u6B62</span>");
          break;

        case 'invaldate':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-gray\">\u4F5C\u5E9F\u4E2D</span>");
          break;

        case 'invalidated':
          $(DOM).find('.list--item--content--title').append("<span class=\"label label-gray\">\u5DF2\u4F5C\u5E9F</span>");
          break;
      }

      return {
        el: DOM
      };
    };
    /**
     * 筛选dialog
     */


    var _CACHE_SCREEN_DIALOG_DOMS = null;

    _this.screenDialog = function () {
      var _this3 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        is_show: false,
        screen: [],
        onComplete: function onComplete() {}
      }, data);

      var DOM = Render.create("<div class=\"half-screen-dialog\">\n                <div class=\"half-screen-dialog-box\">\n                    <div class=\"half-screen-dialog--header\"></div>\n                    <div class=\"half-screen-dialog--body\"></div>\n                    <div class=\"half-screen-dialog--footer\">\n                        <button class=\"btn btn-primary screen-complete\">\u786E\u5B9A</button>\n                    </div>\n                </div>\n            </div>");
      var $Half_screen_dialog__body = $(DOM).find('.half-screen-dialog--body'); // 加载数据

      data.screen.forEach(function (screen) {
        $Half_screen_dialog__body.append(_this3.screenDialogItem(screen).el);
      }); // 切换显示隐藏

      Object.defineProperty(data, 'is_show', {
        get: function get() {
          return _data.is_show;
        },
        set: function set(val) {
          _data.is_show = !!val;

          if (_data.is_show) {
            $('body').addClass('clamp');
            $(DOM).addClass('on');
            $(DOM).find('.half-screen-dialog-box').animate({
              right: 0
            });
          } else {
            $('body').removeClass('clamp');
            $(DOM).find('.half-screen-dialog-box').animate({
              right: '-100%'
            });
            $(DOM).removeClass('on');
            setTimeout(function () {
              if (_CACHE_SCREEN_DIALOG_DOMS) {
                _CACHE_SCREEN_DIALOG_DOMS.parentNode.removeChild(_CACHE_SCREEN_DIALOG_DOMS);

                _CACHE_SCREEN_DIALOG_DOMS = null;
              }
            }, 600);
          }
        }
      }); // 点击背景时

      $(DOM).on('click', function () {
        data.is_show = false; // 恢复上一次保存的值

        data.screen.forEach(function (item, index) {
          item.value = _data.screen[index].value;
        });
      }); // 禁止冒泡点击

      $(DOM).children().on('click', function (e) {
        e.stopPropagation();
      }); // 点击确定

      $(DOM).find('.screen-complete').on('click', function () {
        // 关闭显示层
        data.is_show = false; // 收集数据

        var result = {};
        data.screen.map(function (screen) {
          result[screen.name] = screen.value;
        }); // 发送数据通知

        _data.onComplete.call(data, result);
      });
      data.el = DOM;

      data.render = function (frame_selector) {
        // 清除上一次显示的DOM
        if (_CACHE_SCREEN_DIALOG_DOMS) {
          _CACHE_SCREEN_DIALOG_DOMS.parentNode.removeChild(_CACHE_SCREEN_DIALOG_DOMS);
        }

        _CACHE_SCREEN_DIALOG_DOMS = DOM;
        document.body.appendChild(DOM);
        setTimeout(function () {
          data.is_show = true;
        }, 0);
      };

      return data;
    };
    /**
     * 筛选dialog item
     */


    _this.screenDialogItem = function (data) {
      var _data = Object.assign({}, data);

      var DOM = Render.create("<div class=\"half-screen-dialog--access\">\n                <div class=\"half-screen-dialog--access-box\">\n                    <div class=\"half-screen-dialog--access--title ".concat(data.color, "\">").concat(data.title, "</div>\n                    <ul class=\"half-screen-dialog--access--content\"></ul>\n                </div>\n            </div>"));
      var $Half_screen_dialog__access__content = $(DOM).find('.half-screen-dialog--access--content');
      var _CACHE_LABEL_DOMS = [];
      data.options.forEach(function (item) {
        var LABEL_DOM = Render.create("<li><button data-label=\"".concat(item.value, "\" class=\"btn\">").concat(item.label, "</button></li>")); // 点击修改value值

        var $Btn = $(LABEL_DOM).find('.btn');
        $Btn.on('click', function () {
          data.value = item.value;
        }); // 将每个创建的li放入变量中待执行

        _CACHE_LABEL_DOMS.push(LABEL_DOM);
      }); // 收集起来后统一渲染

      $Half_screen_dialog__access__content.append(_CACHE_LABEL_DOMS); // 设置setter执行效果

      var setter = function setter(val) {
        _CACHE_LABEL_DOMS.forEach(function (LABEL_DOM) {
          var $Btn = $(LABEL_DOM).find('.btn');

          if ($Btn.attr('data-label') == val) {
            $Btn.addClass('btn-' + data.color);
          } else {
            $Btn.removeClass('btn-' + data.color);
          }
        });
      }; // 监听value值变化


      Object.defineProperty(data, 'value', {
        get: function get() {
          return _data.value;
        },
        set: function set(val) {
          _data.value = val;
          setter(val);
        }
      }); // 初始进入时执行一遍setter

      setter(data.value);
      return {
        el: DOM
      };
    };

    return new Component();
  }();

  var Api = function () {
    var Api = function Api() {};

    var _this = Api.prototype; // 接口公共地址前缀

    _this.base_url = Config.api.base_url + "/form/approval/"; // 构建一个统一请求方式

    _this.exec = {
      // 请求的所有数据
      query: {
        token: Config.user.token,
        udid: Config.user.udid,
        user_id: Config.user.id,
        version: Config.user.version,
        org_id: Config.user.org_id
      },
      // 暂存的用户数据数组
      params: {},
      callback: function callback() {},
      fail: function fail() {}
    };
    /**
           * 进行一个post请求
           */

    function _post(url, data, callback, fail) {
      var params = new FormData();

      for (var name in data) {
        params.append(name, data[name]);
      }

      _this.exec.query = {
        token: Config.user.token,
        udid: Config.user.udid,
        user_id: Config.user.id,
        version: Config.user.version,
        org_id: Config.user.org_id // category: 1,
        // subject: 1

      };

      for (var _name in _this.exec.query) {
        params.append(_name, _this.exec.query[_name]);
      }

      return $.ajax({
        url: url,
        type: 'POST',
        data: params,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
          token: Config.user.token,
          udid: Config.user.udid,
          user_id: Config.user.id,
          version: Config.user.version,
          org_id: Config.user.org_id // category: 1,
          // subject: 1

        }
      }).done(function (result) {
        // 如果请求成功，但接口返回失败，提示错误
        if (result.success !== true) {
          fail && fail({
            code: result.code,
            message: result.message
          });
          return;
        }

        callback && callback(result, true);
      }).fail(function (e) {
        // 如果是手动中断，不弹出提示
        if (e.statusText == 'abort') {
          return false;
        }

        fail && fail({
          code: -1,
          message: '服务器繁忙，请重试'
        });
      });
    }

    ;

    function _get(url, data, callback, fail) {
      var params = [];

      for (var name in data) {
        params.push(name + "=" + data[name]);
      }

      _this.exec.query = {
        token: Config.user.token,
        udid: Config.user.udid,
        user_id: Config.user.id,
        version: Config.user.version,
        org_id: Config.user.org_id,
        category: 1,
        subject: 1
      };

      for (var _name2 in _this.exec.query) {
        params.push(_name2 + "=" + _this.exec.query[_name2]);
      }

      return $.ajax({
        url: url,
        type: 'GET',
        data: params.join("&"),
        cache: false,
        processData: false,
        contentType: false,
        headers: {
          token: Config.user.token,
          udid: Config.user.udid,
          user_id: Config.user.id,
          version: Config.user.version,
          org_id: Config.user.org_id
        }
      }).done(function (result) {
        // 如果请求成功，但接口返回失败，提示错误
        if (result.success !== true) {
          fail && fail({
            code: result.code,
            message: result.message
          });
          return;
        }

        callback && callback(result, true);
      }).fail(function (e) {
        // 如果是手动中断，不弹出提示
        if (e.statusText == 'abort') {
          return false;
        }

        fail && fail({
          code: -1,
          message: '服务器繁忙，请重试'
        });
      });
    }

    ;
    _this.post = _post;
    _this.get = _get;

    _this.exec.post = function (name) {
      var exec = _this.exec;
      var url = _this.base_url + _this.urls[name];

      _post(url, exec.params, exec.callback, exec.fail);
    };
    /**
           * 快速执行一个接口
           * 使用于普通通用的调用情况
           */


    _this.run = function (name, params, callback, fail) {
      _this.exec.params = params;

      if (Object.prototype.toString.call(callback) == '[object Function]') {
        _this.exec.callback = callback;
      }

      if (Object.prototype.toString.call(fail) == '[object Function]') {
        _this.exec.fail = fail;
      }

      _this.exec.post(name);
    };

    _this.resource = function (url) {
      var imgName = url.split(".")[0];
      imgName = Config.api.base_url + "/esb/res/pic/" + Math.floor(+imgName / 10000) + "/" + Math.floor(+imgName / 100) + "/" + url;
      return imgName;
    };

    _this.jssdkRegister = function (callback) {
      var _loading = Component.loading().render();

      _this.post(Config.api.base_url + '/shijiwxy/wechat/portal/getWxJsConfig.json', {
        url: window.location.href
      }, function (result) {
        var access = result.data;
        wx.config({
          debug: false,
          appId: access.appId,
          timestamp: access.timestamp,
          nonceStr: access.nonceStr,
          signature: access.signature,
          jsApiList: ['checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone', 'hideMenuItems', 'showMenuItems', 'hideAllNonBaseMenuItem', 'showAllNonBaseMenuItem', 'translateVoice', 'startRecord', 'stopRecord', 'onVoiceRecordEnd', 'playVoice', 'onVoicePlayEnd', 'pauseVoice', 'stopVoice', 'uploadVoice', 'downloadVoice', 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage', 'getNetworkType', 'openLocation', 'getLocation', 'hideOptionMenu', 'showOptionMenu', 'closeWindow', 'scanQRCode', 'chooseWXPay', 'openProductSpecificView']
        });

        _loading.hide(function () {
          callback && callback();
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    }; // 所有用到的接口名称


    _this.urls = {
      // 待我审批
      getPageApprovalNocheck: "getPageApprovalNocheck.json",
      // 我已审批
      getPageApprovalChecked: "getPageApprovalChecked.json",
      // 抄送我的
      getPageApprovalCopy: "getPageApprovalCopy.json",
      // 表单模板页
      getPageFormModel: "getPageFormModel.json",
      // 全部已读
      doApprovalReaded: "doApprovalReaded.json"
    };
    /**
     * 获取进入选择盘列表
     */

    _this.getPageApprovalNocheck = function (_ref, callback, fail) {
      var page = _ref.page,
          _ref$size = _ref.size,
          size = _ref$size === void 0 ? 10 : _ref$size,
          _ref$title = _ref.title,
          title = _ref$title === void 0 ? "" : _ref$title,
          _ref$model_id = _ref.model_id,
          model_id = _ref$model_id === void 0 ? "" : _ref$model_id,
          _ref$status = _ref.status,
          status = _ref$status === void 0 ? "" : _ref$status,
          _ref$query_id = _ref.query_id,
          query_id = _ref$query_id === void 0 ? "" : _ref$query_id;
      this.run("getPageApprovalNocheck", {
        pageNo: page,
        pageSize: size,
        queryId: query_id,
        title: title,
        formModelId: model_id,
        approvalStatus: status
      }, callback, fail);
    };

    _this.getPageApprovalChecked = function (_ref2, callback, fail) {
      var page = _ref2.page,
          _ref2$size = _ref2.size,
          size = _ref2$size === void 0 ? 10 : _ref2$size,
          _ref2$title = _ref2.title,
          title = _ref2$title === void 0 ? "" : _ref2$title,
          _ref2$model_id = _ref2.model_id,
          model_id = _ref2$model_id === void 0 ? "" : _ref2$model_id,
          _ref2$status = _ref2.status,
          status = _ref2$status === void 0 ? "" : _ref2$status,
          _ref2$query_id = _ref2.query_id,
          query_id = _ref2$query_id === void 0 ? "" : _ref2$query_id;
      this.run("getPageApprovalChecked", {
        pageNo: page,
        pageSize: size,
        queryId: query_id,
        title: title,
        formModelId: model_id,
        approvalStatus: status
      }, callback, fail);
    };

    _this.getPageApprovalCopy = function (_ref3, callback, fail) {
      var page = _ref3.page,
          _ref3$size = _ref3.size,
          size = _ref3$size === void 0 ? 10 : _ref3$size,
          _ref3$title = _ref3.title,
          title = _ref3$title === void 0 ? "" : _ref3$title,
          _ref3$model_id = _ref3.model_id,
          model_id = _ref3$model_id === void 0 ? "" : _ref3$model_id,
          _ref3$status = _ref3.status,
          status = _ref3$status === void 0 ? "" : _ref3$status,
          _ref3$is_read = _ref3.is_read,
          is_read = _ref3$is_read === void 0 ? "" : _ref3$is_read,
          _ref3$query_id = _ref3.query_id,
          query_id = _ref3$query_id === void 0 ? "" : _ref3$query_id;
      this.run("getPageApprovalCopy", {
        pageNo: page,
        pageSize: size,
        queryId: query_id,
        title: title,
        formModelId: model_id,
        approvalStatus: status,
        isRead: is_read
      }, callback, fail);
    };

    _this.getPageFormModel = function (_ref4, callback, fail) {
      _objectDestructuringEmpty(_ref4);

      this.run("getPageFormModel", {
        pageNo: 1,
        pageSize: 10000,
        title: ""
      }, callback, fail);
    };

    _this.doApprovalReaded = function (_ref5, callback, fail) {
      var status = _ref5.status,
          _ref5$title = _ref5.title,
          title = _ref5$title === void 0 ? "" : _ref5$title;
      this.run("doApprovalReaded", {
        approvalStatus: status,
        title: title
      }, callback, fail);
    };

    return new Api();
  }();

  var QUERY = Util.getQuery();
  /**
     * 初始化
     */

  var Init = function () {
    var Init = function Init() {}; // 筛选结构


    var screen = {
      status: {
        name: 'status',
        color: 'primary',
        title: '审批状态',
        value: '',
        options: [{
          label: '全部',
          value: ''
        }, {
          label: '正在审批',
          value: '1'
        }, {
          label: '审批通过',
          value: '2'
        }, {
          label: '审批拒绝',
          value: '3'
        } // {label: '审批撤销', value: '5'},
        ]
      },
      is_read: {
        name: 'is_read',
        color: 'warning',
        title: '消息状态',
        value: '',
        options: [{
          label: '全部',
          value: ''
        }, {
          label: '未读',
          value: '0'
        }]
      },
      modelLoading: false,
      model: {
        name: 'model',
        color: 'success',
        title: '审批名称',
        value: '',
        options: [{
          label: '全部',
          value: ''
        }]
      }
    };

    function newInstance() {
      // 创建组件
      var Navbar = Component.navbar({
        onClick: function onClick(tab) {
          switch (tab) {
            case 'wait':
              Toolbar.all_readed = true;
              List.api = 'getPageApprovalNocheck';
              break;

            case 'readed':
              Toolbar.all_readed = true;
              List.api = 'getPageApprovalChecked';
              break;

            case 'copymy':
              Toolbar.all_readed = false;
              List.api = 'getPageApprovalCopy';
              break;
          } // 重置其他条件为默认条件


          screen.status.value = '';
          screen.is_read.value = '';
          screen.model.value = '';
          Toolbar.is_show = false;
          Searchbar.search = '';
          List.resetForm(); // 刷新列表

          List.refresh();
        }
      });
      var Toolbar = Component.toolbar({
        openClickSearch: function openClickSearch() {
          Searchbar.is_show = true;
        },
        closeClickSearch: function closeClickSearch() {
          Searchbar.is_show = false;
          Searchbar.search = '';
          List.title = '';
          List.refresh();
        },
        screenClick: function screenClick() {
          var _opera = function _opera() {
            var menu = [];

            switch (Navbar.active) {
              case 'wait':
                menu = [screen.model];
                break;

              case 'readed':
                menu = [screen.status, screen.model];
                break;

              case 'copymy':
                menu = [screen.status, screen.is_read, screen.model];
                break;
            }

            Component.screenDialog({
              screen: menu,
              onComplete: function onComplete(screen) {
                List.status = screen.status || '';
                List.model_id = screen.model || '';
                List.is_read = screen.is_read || '';
                List.refresh();
              }
            }).render();
          }; // *yk: 修改由于延时调用模板导致的显示不全问题


          var _loading;

          if (screen.modelLoading) {
            _loading = Component.loading().render();

            (function loadingModelSet() {
              setTimeout(function () {
                if (screen.modelLoading) {
                  loadingModelSet();
                } else {
                  _loading.hide(function () {
                    return _opera();
                  });
                }
              }, 200);
            })();

            return;
          } else {
            _opera();
          }
        },
        readClick: function readClick() {
          if (!window.confirm("是否全部已读？")) {
            return false;
          } // 全部已读


          var _loading = Component.loading("操作中").render();

          Api.doApprovalReaded({
            status: screen.status.value,
            title: List.title
          }, function (result) {
            _loading.hide(function () {
              Component.toast("操作成功").render(function () {
                List.refresh();
              });
            });
          }, function (e) {
            _loading.hide(function () {
              Component.toast(e.message).render();
            });
          });
        }
      });
      var Searchbar = Component.searchbar({
        autoFocus: true,
        onSubmit: function onSubmit() {
          List.title = Searchbar.search;
          List.refresh();
        }
      });
      var List = Component.list({
        api: 'getPageApprovalNocheck',
        page: 1,
        onLoad: function onLoad(exec) {
          exec(function (result) {
            var form = [];
            var auth = {};

            try {
              var ele = JSON.parse(result.approvalEleJson);

              if (ele.hasOwnProperty("auth")) {
                auth = ele.auth;
              }
            } catch (error) {}

            try {
              form = Util.likeItemObjectMap(JSON.parse(result.formJson), function (item, index) {
                // *yk: 权限为隐藏不显示列表中
                if (auth[index] == 3) {
                  return true;
                } // ＊yk: type为7和99的项不显示在列表中


                if (["7", "99"].includes(item.type)) {
                  return true;
                }

                if (Util.type(item.value) == "string") {
                  item.value = item.value.trim();
                }

                if (Util.isEmpty(item.value)) {
                  return true;
                }

                return item;
              });
            } catch (error) {
              form = [];
            }

            var item = {
              D_id: result.formDetailId,
              A_id: result.formApprovalId,
              AD_id: result.formApprovalDetailId,
              type: result.formDetailTitle,
              time: result.insertTimeStr,
              avatar: result.startHeadUrl || Config.default_avatar,
              title: result.formModelTitle,
              status: function (status) {
                var _status = {
                  1: 'approval',
                  2: 'success',
                  3: 'refuse',
                  4: 'cancel',
                  5: 'stop',
                  6: 'invaldate',
                  7: 'invalidated'
                };
                return _status[status] || '';
              }(result.approval_status),
              is_unreaded: '' + result.is_read === '0',
              extra: form.slice(0, 3).map(function (item) {
                return item.title + '：' + item.value_text;
              })
            }; // 附加：分页列表需要额外参数：列表中最后一项的ID

            List.query_id = item.id;
            return item;
          });
        }
      }); // 加载组件

      var Container = Render.getContainer('app');
      Container.el.appendChild(Navbar.el);
      Container.el.appendChild(Toolbar.el);
      Container.el.appendChild(Searchbar.el);
      Container.el.appendChild(List.el);
      Container.render();
    }

    Init.prototype.instance = function () {
      screen.modelLoading = true;
      Api.getPageFormModel({}, function (result) {
        screen.model.options.length = 1;
        result.data.data.map(function (model) {
          screen.model.options.push({
            label: model.title,
            value: model.id + ""
          });
        });
        screen.modelLoading = false;
      });
      newInstance();
    };

    return new Init();
  }(); // 初始化执行


  Init.instance();
})(function () {
  // return {
  //     DEFAULT_AVATAR: "/shijiwxy/weixin/images/defaultHead.jpg",
  //     API_BASE_URL: "https://t.shijiwxy.5tree.cn",
  //     TOKEN: "c74b4af5c0a834a08af1ac2b848a79cd_1594273538171_4932698_0_3ad33b1c",
  //     UDID: "cf5c56aa-a023-4a2f-9370-b7e63ad33b1c",
  //     USER_ID: 4932698,
  //     ORG_ID: 192,
  //     VERSION: 3,
  // };
  return {
    DEFAULT_AVATAR: "/shijiwxy/weixin/images/defaultHead.jpg",
    API_BASE_URL: "https://t.shijiwxy.5tree.cn",
    // API_BASE_URL: "http://192.168.0.132:9080",
    TOKEN: "57c19f77bfb30b1910bfa2f3f05b48df_1616465358219_2515046_0_5be123f1",
    UDID: "05647b96-d59a-48f9-956e-6ad25be123f1",
    USER_ID: 2515046,
    ORG_ID: 192,
    VERSION: 3
  };
});