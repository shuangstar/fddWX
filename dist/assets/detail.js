'use strict';

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  // 去除config.jsArray方法自定义扩展
  for (var name in Array.prototype) {
    delete Array.prototype[name];
  }
})();

(function (factory) {
  var BASE = factory();
  /**
     * dropload
     * 西门(http://ons.me/526.html)
     * 0.9.1(161205)
     */

  ;

  (function ($) {
    'use strict';

    var win = window;
    var doc = document;
    var $win = $(win);
    var $doc = $(doc);

    $.fn.dropload = function (options) {
      return new MyDropLoad(this, options);
    };

    var MyDropLoad = function MyDropLoad(element, options) {
      var me = this;
      me.$element = element; // 上方是否插入DOM

      me.upInsertDOM = false; // loading状态

      me.loading = false; // 是否锁定

      me.isLockUp = false;
      me.isLockDown = false; // 是否有数据

      me.isData = true;
      me._scrollTop = 0;
      me._threshold = 0;
      me.init(options);
    }; // 初始化


    MyDropLoad.prototype.init = function (options) {
      var me = this;
      me.opts = $.extend(true, {}, {
        scrollArea: me.$element,
        // 滑动区域
        domUp: {
          // 上方DOM
          domClass: 'dropload-up',
          domRefresh: '<div class="dropload-refresh">↓下拉刷新</div>',
          domUpdate: '<div class="dropload-update">↑释放更新</div>',
          domLoad: '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown: {
          // 下方DOM
          domClass: 'dropload-down',
          domRefresh: '<div class="dropload-refresh">↑上拉加载更多</div>',
          domLoad: '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
          domNoData: '<div class="dropload-noData">没有更多数据了</div>'
        },
        autoLoad: true,
        // 自动加载
        distance: 50,
        // 拉动距离
        threshold: '',
        // 提前加载距离
        loadUpFn: '',
        // 上方function
        loadDownFn: '' // 下方function

      }, options); // 如果加载下方，事先在下方插入DOM

      if (me.opts.loadDownFn != '') {
        me.$domDown = $('<div class="' + me.opts.domDown.domClass + '">' + me.opts.domDown.domRefresh + '</div>');
        me.$element.append(me.$domDown);
      } // 计算提前加载距离


      if (!!me.$domDown && me.opts.threshold === '') {
        // 默认滑到加载区2/3处时加载
        me._threshold = Math.floor(me.$domDown.height() * 1 / 3);
      } else {
        me._threshold = me.opts.threshold;
      } // 判断滚动区域


      if (me.opts.scrollArea == win) {
        me.$scrollArea = $win; // 获取文档高度

        me._scrollContentHeight = $doc.height(); // 获取win显示区高度  —— 这里有坑

        me._scrollWindowHeight = doc.documentElement.clientHeight;
      } else {
        me.$scrollArea = me.opts.scrollArea;
        me._scrollContentHeight = me.$element[0].scrollHeight;
        me._scrollWindowHeight = me.$scrollArea.height();
      }

      fnAutoLoad(me); // 窗口调整

      $win.on('resize', function () {
        clearTimeout(me.timer);
        me.timer = setTimeout(function () {
          if (me.opts.scrollArea == win) {
            // 重新获取win显示区高度
            me._scrollWindowHeight = win.innerHeight;
          } else {
            me._scrollWindowHeight = me.$element.height();
          }

          fnAutoLoad(me);
        }, 150);
      });
      var _control = false; // 绑定触摸

      me.$element.on('mousedown touchstart', function (e) {
        _control = true;

        if (!me.loading) {
          fnTouches(e);
          fnTouchstart(e, me);
        }
      });
      me.$element.on('mousemove touchmove', function (e) {
        if (!_control) {
          return;
        }

        if (!me.loading) {
          fnTouches(e, me);
          fnTouchmove(e, me);
        }
      });
      me.$element.on('mouseup touchend', function () {
        _control = false;

        if (!me.loading) {
          fnTouchend(me);
        }
      }); // 加载下方

      me.$scrollArea.on('scroll', function () {
        me._scrollTop = me.$scrollArea.scrollTop();
        fnRecoverContentHeight(me); // 滚动页面触发加载数据

        if (me.opts.loadDownFn != '' && !me.loading && !me.isLockDown && me._scrollContentHeight - me._threshold <= me._scrollWindowHeight + me._scrollTop) {
          loadDown(me);
        }
      });
    }; // touches


    function fnTouches(e) {
      if (!e.touches) {
        e.touches = e.originalEvent.touches || [{
          pageX: e.originalEvent.pageX,
          pageY: e.originalEvent.pageY
        }];
      }
    } // touchstart


    function fnTouchstart(e, me) {
      me._startY = e.touches[0].pageY; // 记住触摸时的scrolltop值

      me.touchScrollTop = me.$scrollArea.scrollTop();
    } // touchmove


    function fnTouchmove(e, me) {
      me._curY = e.touches[0].pageY;
      me._moveY = me._curY - me._startY;

      if (me._moveY > 0) {
        me.direction = 'down';
      } else if (me._moveY < 0) {
        me.direction = 'up';
      }

      var _absMoveY = Math.abs(me._moveY); // 加载上方


      if (me.opts.loadUpFn != '' && me.touchScrollTop <= 0 && me.direction == 'down' && !me.isLockUp) {
        e.preventDefault();
        me.$domUp = $('.' + me.opts.domUp.domClass); // 如果加载区没有DOM

        if (!me.upInsertDOM) {
          me.$element.prepend('<div class="' + me.opts.domUp.domClass + '"></div>');
          me.upInsertDOM = true;
        }

        fnTransition(me.$domUp, 0); // 下拉

        if (_absMoveY <= me.opts.distance) {
          me._offsetY = _absMoveY; // todo：move时会不断清空、增加dom，有可能影响性能，下同

          me.$domUp.html(me.opts.domUp.domRefresh); // 指定距离 < 下拉距离 < 指定距离*2
        } else if (_absMoveY > me.opts.distance && _absMoveY <= me.opts.distance * 2) {
          me._offsetY = me.opts.distance + (_absMoveY - me.opts.distance) * 0.5;
          me.$domUp.html(me.opts.domUp.domUpdate); // 下拉距离 > 指定距离*2
        } else {
          me._offsetY = me.opts.distance + me.opts.distance * 0.5 + (_absMoveY - me.opts.distance * 2) * 0.2;
        }

        me.$domUp.css({
          'height': me._offsetY
        });
      }
    } // touchend


    function fnTouchend(me) {
      var _absMoveY = Math.abs(me._moveY);

      if (me.opts.loadUpFn != '' && me.touchScrollTop <= 0 && me.direction == 'down' && !me.isLockUp) {
        fnTransition(me.$domUp, 300);

        if (_absMoveY > me.opts.distance) {
          me.$domUp.css({
            'height': me.$domUp.children().height()
          });
          me.$domUp.html(me.opts.domUp.domLoad);
          me.loading = true;
          me.opts.loadUpFn(me);
        } else {
          me.$domUp.css({
            'height': '0'
          }).on('webkitTransitionEnd mozTransitionEnd transitionend', function () {
            me.upInsertDOM = false;
            $(this).remove();
          });
        }

        me._moveY = 0;
      }
    } // 如果文档高度不大于窗口高度，数据较少，自动加载下方数据


    function fnAutoLoad(me) {
      if (me.opts.loadDownFn != '' && me.opts.autoLoad) {
        if (me._scrollContentHeight - me._threshold <= me._scrollWindowHeight) {
          loadDown(me);
        }
      }
    } // 重新获取文档高度


    function fnRecoverContentHeight(me) {
      // if (me.opts.scrollArea == win) {
      //     me._scrollContentHeight = $doc.height();
      // } else {
      //     me._scrollContentHeight = me.$element[0].scrollHeight;
      // }
      me._scrollContentHeight = me.$element[0].scrollHeight;
    } // 加载下方


    function loadDown(me) {
      if (me.isData) {
        me.direction = 'up';
        me.$domDown.html(me.opts.domDown.domLoad);
        me.loading = true;
        me.opts.loadDownFn(me);
      }
    } // 锁定


    MyDropLoad.prototype.lock = function (direction) {
      var me = this; // 如果不指定方向

      if (direction === undefined) {
        // 如果操作方向向上
        if (me.direction == 'up') {
          me.isLockDown = true; // 如果操作方向向下
        } else if (me.direction == 'down') {
          me.isLockUp = true;
        } else {
          me.isLockUp = true;
          me.isLockDown = true;
        } // 如果指定锁上方

      } else if (direction == 'up') {
        me.isLockUp = true; // 如果指定锁下方
      } else if (direction == 'down') {
        me.isLockDown = true; // 为了解决DEMO5中tab效果bug，因为滑动到下面，再滑上去点tab，direction=down，所以有bug
        // me.direction = 'up';
      }
    }; // 解锁


    MyDropLoad.prototype.unlock = function () {
      var me = this; // 简单粗暴解锁

      me.isLockUp = false;
      me.isLockDown = false; // 为了解决DEMO5中tab效果bug，因为滑动到下面，再滑上去点tab，direction=down，所以有bug
      // me.direction = 'up';
    }; // 无数据


    MyDropLoad.prototype.noData = function (flag) {
      var me = this;

      if (flag === undefined || flag == true) {
        me.isData = false;
      } else if (flag == false) {
        me.isData = true;
      }
    }; // 重置


    MyDropLoad.prototype.resetload = function () {
      var me = this;

      if (me.direction == 'down' && me.upInsertDOM) {
        me.$domUp.css({
          'height': '0'
        }).on('webkitTransitionEnd mozTransitionEnd transitionend', function () {
          me.loading = false;
          me.upInsertDOM = false;
          $(this).remove();
          fnRecoverContentHeight(me);
        });
      } else if (me.direction == 'up') {
        me.loading = false;
      } // 如果有数据


      if (me.isData) {
        // 加载区修改样式
        me.$domDown.html(me.opts.domDown.domRefresh);
        fnRecoverContentHeight(me);
        fnAutoLoad(me);
      } else {
        // 如果没数据
        me.$domDown.html(me.opts.domDown.domNoData);
      }
    };
    /**
       * 手动出发下拉
       */


    MyDropLoad.prototype.refresh = function () {
      var me = this; // 如果加载区没有DOM

      if (!me.upInsertDOM) {
        me.$element.prepend('<div class="' + me.opts.domUp.domClass + '"></div>');
        me.upInsertDOM = true;
      }

      me.direction = 'down';
      me.$domUp = $('.' + me.opts.domUp.domClass).html(me.opts.domUp.domLoad).css('transition', 'linear .1s');
      me.$domUp.css({
        'height': me.$domUp.children().height()
      });
      me.loading = true;
      me.opts.loadUpFn(me);
    }; // css过渡


    function fnTransition(dom, num) {
      dom.css({
        '-webkit-transition': 'all ' + num + 'ms',
        'transition': 'all ' + num + 'ms'
      });
    }
  })(window.Zepto || window.jQuery);

  var Config = function () {
    var Config = {
      default_avatar: BASE.DEFAULT_AVATAR || "/shijiwxy/weixin/images/defaultHead.jpg",
      _page: {
        detail: "./detail.html?d={D_id}&a={A_id}&ad={AD_id}"
      },
      page: function page(name) {
        var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        if (!Config._page.hasOwnProperty(name)) {
          return "";
        }

        var page = Config._page[name];
        page = page.replace(/{(\w+)}/g, function (match, name, index, str, callee) {
          return params[name] || "";
        });
        return page;
      },
      api: {
        base_url: BASE.API_BASE_URL || ""
      },
      user: {
        token: BASE.TOKEN,
        udid: BASE.UDID,
        id: BASE.USER_ID,
        org_id: BASE.ORG_ID,
        version: BASE.VERSION
      }
    };
    return Config;
  }();
  /**
   * 工具层
   */


  var Util = function () {
    var Util = function Util() {};

    var _this = Util.prototype;
    /**
     * 下载文件
     * url：下载文件的远程地址
     */

    _this.download = function (url) {
      window.open(url);
    };
    /**
     * 判断数据的具体类型
     */


    _this.type = function (mixin) {
      if (mixin == null) {
        return mixin + "";
      }

      var class2type = {
        '[object Boolean]': 'boolean',
        '[object Number]': 'number',
        '[object String]': 'string',
        '[object Function]': 'function',
        '[object Array]': 'array',
        '[object Date]': 'date',
        '[object RegExp]': 'regexp',
        '[object Object]': 'object',
        '[object Error]': 'error',
        '[object Symbol]': 'symbol'
      };

      var mixin_type = _typeof(mixin);

      if (mixin_type === 'undefined') {
        return 'undefined';
      }

      if (mixin_type === 'object' || mixin_type === "function") {
        var _type = class2type[Object.prototype.toString.call(mixin)];

        if (!_type) {
          return _this.isDom(mixin) ? "dom" : "object";
        } else {
          return _type;
        } // return class2type[Object.prototype.toString.call(mixin)] || "object";

      }

      return mixin_type;
    };
    /**
     * 获取路由切换的完整地址
     */


    _this.getRealPath = function () {
      var path = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      if (path == '') {
        return Router.route;
      }

      if (path.indexOf('/') == 0) {
        return path;
      }

      var hash = this.getHashSimplePath(Router.route);
      var hashs = hash == '' ? [] : hash.split('/');

      var _paths = path == '' ? [] : path.split('/');

      var paths = [];

      for (var i in _paths) {
        if (_paths[i] != hashs[i]) {
          paths = _paths.splice(i);
          break;
        }
      }

      for (var _i = 0; _i < paths.length; _i++) {
        if (paths[_i] == '') {
          continue;
        }

        if (paths[_i] == '.') {
          continue;
        }

        if (paths[_i] == '..') {
          hashs.pop();
          continue;
        }

        hashs.push(paths[_i]);
      }

      return hashs.join('/');
    };
    /**
     * 获取某元素以浏览器左上角为原点的坐标
     */


    _this.offset = function (dom) {
      var top = dom.offsetTop;
      var left = dom.offsetLeft;
      var width = dom.offsetWidth;
      var height = dom.offsetHeight;

      while (dom = dom.offsetParent) {
        top += dom.offsetTop;
        left += dom.offsetLeft;
      }

      return {
        top: top,
        left: left,
        width: width,
        height: height
      };
    };
    /**
     * 判断传入的变量是否是一个dom对象
     */


    _this.isDom = function (dom) {
      return (typeof HTMLElement === "undefined" ? "undefined" : _typeof(HTMLElement)) === 'object' ? dom instanceof HTMLElement : dom && _typeof(dom) === 'object' && dom.nodeType === 1 && typeof dom.nodeName === 'string';
    };
    /**
     * 创建上拉下拉动作
     */


    _this.scroll = function (DOM) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      options = Object.assign({
        refer: window,
        onRefresh: function onRefresh(done) {
          done();
        },
        onContinue: function onContinue(done) {
          done();
        }
      }, options);
      var DOMdropload = $(DOM).dropload({
        scrollArea: options.refer,
        loadDownFn: function loadDownFn(me) {
          me.lock('up');
          options.onContinue(function () {
            var noData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            me.unlock();
            me.noData(noData);
            me.resetload();
          });
        },
        loadUpFn: function loadUpFn(me) {
          me.lock('down');
          options.onRefresh(function () {
            var noData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
            me.unlock();
            me.noData(noData);
            me.resetload();
          });
        }
      });
      return DOMdropload;
    };
    /**
     * 从1开始的对象，遍历
     * @param {Object} maps
     * @param {Function} callback
     */


    _this.likeItemObjectMap = function (maps, callback) {
      var i = 0;
      var result = [];
      var map; // eslint-disable-next-line no-constant-condition

      while (true) {
        i += 1;

        if (!maps.hasOwnProperty(i)) {
          break;
        }

        map = callback(maps[i], i, map);

        if (map === true) {
          continue;
        }

        if (map === false) {
          break;
        }

        result.push(map);
      }

      return result;
    };

    _this.getQuery = function () {
      var query = {};
      location.search.slice(1).split("&").map(function (item) {
        var srt = item.split("=");

        if (srt[0] != "") {
          query[srt[0]] = srt[1];
        }
      });
      return query;
    };
    /**
     * 是否为空值，不包括0
     */


    _this.isEmpty = function (mixin) {
      var _type = _this.type(mixin);

      if (["null", "undefined"].includes(_type)) {
        return true;
      }

      if (_type == "boolean" && mixin == false) {
        return true;
      }

      if (_type == "array" && mixin.length == 0) {
        return true;
      }

      if (_type == "object" && Object.keys(mixin).length == 0) {
        return true;
      }

      return mixin === "";
    }; //金额输入框实时大写


    _this.convertCurrency = function (money) {
      //汉字的数字
      var cnNums = new Array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"); //基本单位

      var cnIntRadice = new Array("", "拾", "佰", "仟"); //对应整数部分扩展单位

      var cnIntUnits = new Array("", "万", "亿", "兆"); //对应小数部分单位

      var cnDecUnits = new Array("角", "分", "毫", "厘"); //整数金额时后面跟的字符

      var cnInteger = "整"; //整型完以后的单位

      var cnIntLast = "元"; //最大处理的数字

      var maxNum = 999999999999999.9999; //金额整数部分

      var integerNum; //金额小数部分

      var decimalNum; //输出的中文金额字符串

      var chineseStr = ""; //分离金额后用的数组，预定义

      var parts;

      if (money == "") {
        return "";
      }

      if ((money + "").length > 15) {
        return "大写转换最多支持15位数的金额！";
      }

      money = parseFloat(money);

      if (money >= maxNum) {
        //超出最大处理数字
        return "";
      }

      if (money == 0) {
        chineseStr = cnNums[0] + cnIntLast + cnInteger;
        return chineseStr;
      } //转换为字符串


      money = money.toString();

      if (money.indexOf(".") == -1) {
        integerNum = money;
        decimalNum = "";
      } else {
        parts = money.split(".");
        integerNum = parts[0];
        decimalNum = parts[1].substr(0, 4);
      } //获取整型部分转换


      if (parseInt(integerNum, 10) > 0) {
        var zeroCount = 0;
        var IntLen = integerNum.length;

        for (var i = 0; i < IntLen; i++) {
          var n = integerNum.substr(i, 1);
          var p = IntLen - i - 1;
          var q = p / 4;
          var m = p % 4;

          if (n == "0") {
            zeroCount++;
          } else {
            if (zeroCount > 0) {
              chineseStr += cnNums[0];
            } //归零


            zeroCount = 0;
            chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
          }

          if (m == 0 && zeroCount < 4) {
            chineseStr += cnIntUnits[q];
          }
        }

        chineseStr += cnIntLast;
      } //小数部分


      if (decimalNum != "") {
        var decLen = decimalNum.length;

        for (var j = 0; j < decLen; j++) {
          var nn = decimalNum.substr(j, 1);

          if (nn != "0") {
            chineseStr += cnNums[Number(nn)] + cnDecUnits[j];
          }
        }
      }

      if (chineseStr == "") {
        chineseStr += cnNums[0] + cnIntLast + cnInteger;
      } else if (decimalNum == "") {
        chineseStr += cnInteger;
      }

      return chineseStr;
    }; // textarea不回弹


    Util.prototype.iosTextBlurScroll = function (input) {
      if (!input) {
        return false;
      }

      var trueHeight = document.body.scrollHeight; //解决ios唤起键盘后留白

      var backPageSize = function backPageSize() {
        setTimeout(function () {
          window.scroll(0, trueHeight - 10);
          window.innerHeight = window.outerHeight = trueHeight;
        }, 200);
      };

      input.onblur = backPageSize; // onblur是核心方法
    };
    /**
     * 数字前补0变为字符串数字
     */


    _this.fullZeroNumber = function (number) {
      var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

      var __number = number + "";

      if (isNaN(__number)) {
        return number;
      }

      while (__number.length < size) {
        __number = "0" + __number;
      }

      return __number;
    };
    /**
     * 获取设置时间的小时分钟秒
     */


    _this.getCalendarDate = function () {
      var ND = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Date();

      if (_this.type(ND) == "string") {
        ND = ND.replace(/-/g, "/");
      }

      if (_this.isEmpty(ND)) {
        ND = new Date();
      } else {
        ND = new Date(ND);
      }

      var hour = ND.getHours();
      var minute = ND.getMinutes();
      var second = ND.getSeconds();
      var timestamp = ND.getTime();
      ND = new Date(ND.getFullYear(), ND.getMonth(), ND.getDate());
      var time = ND.getTime();
      var NW = {
        ND: ND,
        year: ND.getFullYear(),
        month: _this.fullZeroNumber(ND.getMonth() + 1),
        day: _this.fullZeroNumber(ND.getDate()),
        hour: _this.fullZeroNumber(hour),
        minute: _this.fullZeroNumber(minute),
        second: _this.fullZeroNumber(second),
        time: time,
        timestamp: timestamp
      };
      NW.format = NW.year + "/" + NW.month + "/" + NW.day;
      NW.formatText = NW.year + "年" + NW.month + "月" + NW.day + "日";
      NW.monthFormat = NW.year + "/" + NW.month;
      NW.monthFormatText = NW.year + "年" + NW.month + "月";
      NW.timeFormat = NW.hour + ":" + NW.minute + ":" + NW.second;
      NW.timeFormatText = NW.hour + "时" + NW.minute + "分" + NW.second + "秒";
      NW.minuteTimeFormat = NW.hour + ":" + NW.minute;
      NW.minuteTimeFormatText = NW.hour + "时" + NW.minute + "分"; // 获取当月天数，day=0时month必须+1

      NW.monthDay = _this.fullZeroNumber(new Date(ND.getFullYear(), ND.getMonth() + 1, 0).getDate());
      NW.firstWeek = new Date(ND.getFullYear(), ND.getMonth()).getDay();
      NW.firstTime = new Date(ND.getFullYear(), ND.getMonth()).getTime();
      return NW;
    };

    return new Util();
  }();

  var Render = function () {
    var Render = function Render() {};

    var _this = Render.prototype;

    _this.getContainer = function () {
      var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      var DOM = _this.create('<div id="' + id + '"></div>');

      return {
        el: DOM,
        // 支持格式： 
        // append("")  append(dom) append(Component) append([dom1, dom2, Component])
        // append(Component, dom)
        // 这种不行：
        // append("", [dom]) append([Template], "")
        append: function append(template) {
          var templates = Array.prototype.slice.call(arguments); // 如果第一个参数传了数组，后面的参数无效

          if (Util.type(template) == "array") {
            templates = template;
          }

          templates.map(function (template) {
            var type = Util.type(template);

            if (Util.isEmpty(template)) {
              return false;
            }

            if (type == "string") {
              template = template.trim();

              if (template.length == 0) {
                return false;
              }

              template = _this.create(template);
              type = "dom";
            }

            if (type == "dom") {
              return template;
            }

            if (Util.type(template.el) == "dom") {
              return template.el;
            }

            return false;
          }).filter(function (dom) {
            return dom !== false;
          }).map(function (dom) {
            DOM.appendChild(dom);
          });
          return this;
        },
        render: function render(selector) {
          if (typeof selector == 'undefined') {
            var original = document.querySelector('#' + id);
            original.parentNode.replaceChild(DOM, original);
          } else {
            var parent = document.querySelector(selector);
            parent.append(DOM);
          }

          setTimeout(function () {
            DOM.className = (DOM.className + " on").trim();
          }, 100);
        }
      };
    };
    /**
     * 通过字符串模板创建DOM
     * @param {String} string 字符串模板，模板必须包含一个最外层标签
     */


    _this.create = function (string) {
      var Element = document.createElement('div');
      string = string.trim();
      var wrapMap = {
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
      };
      var tag = (/<([a-z][^\/\0>\x20\t\r\n\f]*)/i.exec(string) || ["", ""])[1].toLowerCase();
      var wrap = wrapMap[tag] || wrapMap._default;
      Element.innerHTML = wrap[1] + string + wrap[2];
      var j = wrap[0];

      while (j--) {
        Element = Element.lastChild;
      }

      return Element.firstChild;
    };

    _this.compile = function (DOM, template) {
      var event = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
      var Element = this.create(template);
      event && event(Element);

      if (typeof DOM == 'string') {
        DOM = document.querySelector(DOM);
      }

      DOM.appendChild(Element);
    };

    return new Render();
  }();

  var Component = function () {
    var Component = function Component() {};

    var _this = Component.prototype;
    /**
           * 获取定制数组重构方法
           */

    function getArrayArgumentations(callback) {
      var aryMethods = ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'];
      var arrayArgumentations = [];
      aryMethods.forEach(function (method) {
        var original = Array.prototype[method];

        arrayArgumentations[method] = function () {
          var result = original.apply(this, arguments);
          callback && callback(method);
          return result;
        };
      }); // 清空数组只保留项数

      arrayArgumentations.clear = function () {
        var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        this.length = length;
        callback && callback('clear');
      };

      return arrayArgumentations;
    }

    ;
    /**
           * 软提示
           */

    var _CACHE_TOAST_DOMS;

    _this.toast = function (data) {
      if (typeof data == 'string') {
        data = {
          text: data
        };
      }

      var _data = Object.assign({
        // 提示文字，必填
        text: '',
        // 消失延时毫秒数
        delay: 3000
      }, data);

      if (_data.text == '') {
        return false;
      }

      var DOM = Render.create("<div class=\"toast\">\n    \n                <div class=\"toast-box\">".concat(_data.text, "</div>\n    \n            </div>"));
      return {
        el: DOM,
        render: function render(callback) {
          // 清除上一次显示的DOM
          if (_CACHE_TOAST_DOMS) {
            _CACHE_TOAST_DOMS.parentNode.removeChild(_CACHE_TOAST_DOMS);
          }

          _CACHE_TOAST_DOMS = DOM;
          $("body").addClass('clamp');
          setTimeout(function () {
            $(DOM).addClass('on');
          }, 0);
          setTimeout(function () {
            callback && callback();
            $("body").removeClass('clamp');
            $(DOM).removeClass('on');
            setTimeout(function () {
              DOM.parentNode.removeChild(DOM);
              _CACHE_TOAST_DOMS = undefined;
            }, _data.delay + 1000);
          }, _data.delay);
          $("body").append(DOM);
        }
      };
    };
    /**
           * 加载中
           */


    var _CACHE_LOADING_DOMS;

    _this.loading = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "加载中";

      if (typeof data == 'string') {
        data = {
          text: data
        };
      }

      var _data = Object.assign({
        // 提示文字，必填
        text: ""
      }, data);

      var DOM = Render.create("<div class=\"loading\">\n    \n                <div class=\"loading-box\">\n    \n                    <i class=\"fa fa-spinner fa-spin\"></i>\n    \n                    ".concat(_data.text, "\n    \n                </div>\n    \n            </div>"));
      return {
        el: DOM,
        render: function render() {
          // 清除上一次显示的DOM
          if (_CACHE_LOADING_DOMS) {
            _CACHE_LOADING_DOMS.parentNode.removeChild(_CACHE_LOADING_DOMS);
          }

          _CACHE_LOADING_DOMS = DOM;
          $('body').addClass('clamp');
          setTimeout(function () {
            $(DOM).addClass('on');
          }, 0);
          $('body').append(DOM);
          return {
            hide: function hide(callback) {
              $('body').removeClass('clamp');
              $(DOM).removeClass('on');
              setTimeout(function () {
                DOM.parentNode.removeChild(DOM);
                _CACHE_LOADING_DOMS = undefined;
                callback && callback();
              }, 300);
            }
          };
        }
      };
    };
    /**
           * 模态框
           */


    _this.media = function (data) {
      if (typeof data == 'string') {
        data = {
          component: ""
        };
      }

      var _data = Object.assign({
        title: "无标题",
        closeConfirm: false,
        body: undefined,
        onComplete: function onComplete(data, done) {
          done();
        }
      }, data);

      if (_data.text == '') {
        return false;
      }

      var DOM = Render.create("<div class=\"media\">\n    \n                <div class=\"media-box\">\n    \n                    <div class=\"media--header\">\n    \n                        <span>".concat(_data.title, "</span>\n    \n                        <span class=\"media--close\"><i class=\"fa fa-times\"></i></span>\n    \n                    </div>\n    \n                    <div class=\"media--body\"></div>\n    \n                    <div class=\"media--footer\">\n    \n                        <button type=\"button\" class=\"btn btn-small media--close\">\u53D6\u6D88</button>\n    \n                        <button type=\"button\" class=\"btn btn-small btn-primary media--complete\">\u786E\u5B9A</button>\n    \n                    </div>\n    \n                </div>\n    \n            </div>"));
      $(DOM).find(".media--body").append(_data.body);
      $(DOM).on("close", function () {
        $("body").removeClass('clamp');
        $(DOM).removeClass("on");
        setTimeout(function () {
          DOM.parentNode.removeChild(DOM);
        }, 300);
      });
      $(DOM).on("complete", function (e) {
        var data = {};
        $(_data.body).find("[data-media-form-name]").each(function (index, item) {
          var $item = $(item);
          var key = $item.attr("data-media-form-name");
          var value = $item.attr("data-media-form-value") || $item.val() || "";
          data[key] = value;
        });

        _data.onComplete(data, function () {
          $(DOM).trigger("close");
        });
      });
      $(DOM).find(".media--close").on("click", function () {
        if (_data.closeConfirm) {
          var confirm_text = "确定关闭此弹窗？";

          if (typeof _data.closeConfirm == "string") {
            confirm_text = _data.closeConfirm;
          }

          if (!window.confirm(confirm_text)) {
            return false;
          }
        }

        $(DOM).trigger("close");
      });
      $(DOM).find(".media--complete").on("click", function () {
        $(DOM).trigger("complete");
      });
      _data.el = DOM;

      _data.render = function () {
        $("body").addClass('clamp');
        setTimeout(function () {
          $(DOM).addClass('on');
        }, 0);
        $("body").append(DOM);
        return _data;
      };

      return _data;
    };
    /**
           * 确认框
           */


    _this.confirm = function () {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "继续操作？";
      var onClick = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
      var DOM = Render.create("<div class=\"confirm\">\n    \n                <div class=\"confirm-box\">\n    \n                    <div class=\"confirm--title\">".concat(message, "</div>\n    \n                    <div class=\"confirm--button\">\n    \n                        <span class=\"confirm-close\">\u53D6\u6D88</span>\n    \n                        <span class=\"confirm-complete\">\u786E\u5B9A</span>\n    \n                    </div>\n    \n                </div>\n    \n            </div>"));
      $(DOM).on("close", function (e, onClick) {
        console.log(arguments);
        $(this).removeClass("on");
        setTimeout(function () {
          DOM.parentNode.removeChild(DOM);
          onClick && onClick();
        }, 300);
      });
      $(DOM).find(".confirm-close").on("click", function () {
        $(this).trigger("close");
      });
      $(DOM).find(".confirm-complete").on("click", function () {
        $(this).trigger("close", onClick);
      });
      return {
        render: function render() {
          setTimeout(function () {
            $(DOM).addClass("on");
          }, 0);
          $("body").append(DOM);
        }
      };
    };
    /**
           * 抽屉
           */


    var _CACHE_DRAWER_DOMS = null;

    _this.drawer = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        color: "primary",
        // +yk: 扩展栏
        stretch: null,
        is_show: false,
        body: "",
        closeByBlank: true,
        onComplete: function onComplete() {},
        onClose: function onClose() {}
      }, data);

      var DOM = Render.create("<div class=\"half-screen-dialog\">\n    \n                <div class=\"half-screen-dialog-box\">\n    \n                    <div class=\"half-screen-dialog--header\"></div>\n    \n                    <div class=\"half-screen-dialog--body\"></div>\n    \n                    <div class=\"half-screen-dialog--footer\">\n    \n                        <button class=\"btn btn-".concat(_data.color, " btn-half-round screen-complete\">\u786E\u5B9A</button>\n    \n                    </div>\n    \n                </div>\n    \n            </div>"));
      var $Half_screen_dialog__body = $(DOM).find('.half-screen-dialog--body');
      var $Half_screen_dialog__footer = $(DOM).find('.half-screen-dialog--footer');

      if (_data.stretch) {
        $Half_screen_dialog__footer.prepend(_data.stretch);
        $Half_screen_dialog__footer.children(".btn").addClass("btn-small");
      } // 加载数据


      $Half_screen_dialog__body.append(_data.body); // 切换显示隐藏

      Object.defineProperty(data, 'is_show', {
        get: function get() {
          return _data.is_show;
        },
        set: function set(val) {
          _data.is_show = !!val;

          if (_data.is_show) {
            $('body').addClass('clamp');
            $(DOM).addClass('on');
            $(DOM).find('.half-screen-dialog-box').animate({
              right: 0
            });
          } else {
            $('body').removeClass('clamp');
            $(DOM).find('.half-screen-dialog-box').animate({
              right: '-100%'
            });
            $(DOM).removeClass('on');
            setTimeout(function () {
              if (_CACHE_DRAWER_DOMS) {
                _CACHE_DRAWER_DOMS.parentNode.removeChild(_CACHE_DRAWER_DOMS);

                _CACHE_DRAWER_DOMS = null;
              }
            }, 600);
          }
        }
      }); // 点击背景时

      $(DOM).on('click', function () {
        if (_data.closeByBlank) {
          data.is_show = false;

          _data.onClose.call(data);
        }
      }); // 禁止冒泡点击

      $(DOM).children().on('click', function (e) {
        e.stopPropagation();
      }); // 点击确定

      $(DOM).find('.screen-complete').on('click', function () {
        // 发送数据通知
        var result = _data.onComplete.call(data); // 只有明确返回false时才不会关闭抽屉


        if (result !== false) {
          // 关闭显示层
          data.is_show = false;
        }
      });
      data.el = DOM;

      data.render = function (frame_selector) {
        // 清除上一次显示的DOM
        if (_CACHE_DRAWER_DOMS) {
          _CACHE_DRAWER_DOMS.parentNode.removeChild(_CACHE_DRAWER_DOMS);
        }

        _CACHE_DRAWER_DOMS = DOM;
        document.body.append(DOM);
        setTimeout(function () {
          data.is_show = true;
        }, 0);
        console.log(data);
      };

      return data;
    };

    _this.header = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        avatar: Config.default_avatar,
        name: "",
        mobile: "",
        class_name: "",
        grade_name: "",
        approval_identity: ""
      }, data);

      var DOM = Render.create("<div class=\"header\">\n                <div class=\"header-box\">\n                    <div class=\"header--avatar skeleton\">\n                        <img src=\"".concat(_data.avatar, "\" alt=\"\" srcset=\"\">\n                    </div>\n                    <div class=\"header--info\">\n                        <div class=\"header--info-name skeleton\">\n                            <span class=\"header--info-name-username\">").concat(_data.name, "</span>\n                            <span class=\"header--info-name-gcname\">").concat(_data.approval_identity == '0' ? '（' + _data.grade_name + ' ' + _data.class_name + '）' : '', "</span>\n                        </div>\n                        <div class=\"header--info-tel skeleton\">").concat(_data.mobile, "</div>\n                    </div>\n                </div>\n            </div>"));
      return {
        el: DOM
      };
    };

    _this.info = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        info: [],
        status: "",
        onClickEdit: function onClickEdit() {}
      }, data);

      var DOM = Render.create("<div class=\"info\"><div class=\"info-box\"></div></div>");
      var $Info_box = $(DOM).find(".info-box");
      var info_items = [];
      Object.defineProperty(data, "info", {
        get: function get() {
          return _data.info;
        },
        set: function set(val) {
          _data.info = val;
        }
      });

      _data.info.map(function (info) {
        var _info = {
          display: info.display
        };
        var infoItemDOM;

        switch (info.type) {
          case "text":
          case "options":
          case "multiOptions":
          case "image":
          case "sign":
          case "netdisk":
          case "tel":
          case "id":
          case "address":
            //只展示非隐藏项
            if (!info.is_hidden) {
              infoItemDOM = _this.infoItemText(info, _data.onClickEdit);
            }

            break;

          case "part":
            infoItemDOM = _this.infoItemPart(info, _data.onClickEdit);
            break;
        }

        if (infoItemDOM != undefined) {
          info_items.push(infoItemDOM.el);
        }
      });

      $Info_box.append(info_items);
      return {
        el: DOM
      };
    };

    _this.infoItemText = function (data, onClickEdit) {
      var _data = Object.assign({
        id: "",
        type: "",
        title: "",
        value: "",
        value_text: "",
        is_hidden: "",
        editable: false,
        display: false
      }, data);

      var color = data.original.color; // 编译文字为特殊类型需要显示的方式

      var compile = function compile(type, value_text, original, value) {
        switch (type + "") {
          case "sign":
          case "image":
            if (value_text != "") {
              var figureDOM = value_text.split(",").map(function (src) {
                return Render.create("<figure itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">\n                                        <a href=\"".concat(src, "\" itemprop=\"contentUrl\" data-size=\"1640x1136\">\n                                            <img src=\"").concat(src, "\" itemprop=\"thumbnail\" alt=\"\" />\n                                        </a>\n                                    </figure>"));
              });
              var figureBoxDOM = Render.create("<div class=\"my-gallery\" itemscope itemtype=\"http://schema.org/ImageGallery\"></div>");
              $(figureBoxDOM).append(figureDOM);
              $(figureBoxDOM).find("img").on("load", function () {
                $(this).parent().attr("data-size", "".concat(this.naturalWidth, "x").concat(this.naturalHeight));
              });
              window.initPhotoSwipeFromElements([figureBoxDOM]);
              return figureBoxDOM;
            } else {
              return "";
            }

          case "netdisk":
            if (value_text != "") {
              try {
                var files = JSON.parse(value);

                var _figureBoxDOM = Render.create("<div></div>");

                var previewDom = Render.create("<div id=\"previewContent\" class=\"content\"></div>");
                previewDom.style.cssText = "overflow: hidden;background-color: #404040;position: fixed;z-index: 10;top: 0;left: 0;width: 100%;height: 100%;";
                var figureDOMs = files.map(function (file) {
                  var DescDOM = Render.create("<div class=\"info--item-desc-netdisk\">\n                                        <div>\n                                            <img src=\"".concat(file.icon, "\" alt=\"\" />\n                                            <p>").concat(file.name, "</p>\n                                        </div>\n                                    </div>"));
                  DescDOM.addEventListener("click", function () {
                    var rowkey = file.rowkey;
                    var preview_url = file.preview;
                    var name = file.name;
                    var url = "/shijiwxy/weixin/html/teacher/previewFile/index.html?nodownload=1&fileUrl=".concat(preview_url, "&rowkey=").concat(rowkey, "&fileName=").concat(encodeURIComponent(name));
                    location.hash = rowkey;
                    previewDom.innerHTML = "";
                    var iframe = Render.create("<iframe id=\"activePage\"></iframe>");
                    iframe.src = url;
                    iframe.width = '100%';
                    iframe.style.height = '100%';
                    iframe.style.border = "none";
                    iframe.marginwidth = '0';
                    iframe.marginheight = '0';
                    iframe.hspace = "0";
                    iframe.vspace = "0";
                    iframe.frameborder = "0";
                    previewDom.appendChild(iframe);
                    document.body.appendChild(previewDom);

                    var windowPreviewHashChangeEventCallback = function windowPreviewHashChangeEventCallback() {
                      if (location.hash.slice(1) != rowkey) {
                        document.body.removeChild(previewDom);
                        window.removeEventListener("hashchange", windowPreviewHashChangeEventCallback);
                      }
                    };

                    window.addEventListener("hashchange", windowPreviewHashChangeEventCallback);
                  });
                  return DescDOM;
                });
                $(_figureBoxDOM).append(figureDOMs);
                return _figureBoxDOM;
              } catch (error) {
                return "";
              }
            }

            return "";

          default:
            // 说明文字
            if (original.type == "9") {
              if (original.url) {
                value_text = "<a href=\"".concat(original.url, "\" style=\"color:").concat(original.color, "\">").concat(original.placeholder, "</a>");
              } else {
                value_text = "<span style=\"color:".concat(original.color, "\">").concat(original.placeholder, "</span>");
              }
            } // 手机号


            if (original.type == "31") {
              value_text = "<a href=\"tel:".concat(value, "\" style=\"text-decoration:underline;\">").concat(value, "</a>");
            } // 温度输入框


            if (original.type == "33") {
              if (+value > +original.warning) {
                value_text = "<span style=\"color: red\">".concat(value_text, "</span>");
              } else {
                value_text = "<span>".concat(value_text, "</span>");
              }
            }

            return value_text;
        }
      };

      var DOM = Render.create("<div class=\"info--item\">\n                <div class=\"info--item-title skeleton\">".concat(_data.title, "\uFF1A</div>\n                <div class=\"info--item-desc skeleton\" style=\"color: ").concat(color, "\"><div class=\"info--item-desc-box\"></div></div>\n            </div>"));
      $(DOM).find(".info--item-desc-box").append(compile(_data.type, _data.value_text, _data.original, _data.value));

      if (_data.editable) {
        $(DOM).append("<div class=\"info--item-edit\"><i class=\"fa fa-edit\"></i></div>");
      } // 这里的Info__item_desc里面固定添加了一个div


      var $Info__item_desc = $(DOM).find(".info--item-desc").children("div");
      var $Info__item_edit = $(DOM).find(".info--item-edit");
      Object.defineProperty(data, "value_text", {
        get: function get() {
          return _data.value_text;
        },
        set: function set(val) {
          _data.value_text = val;
          $Info__item_desc.html(compile(_data.type, data.value_text, data.original, data.value));
        }
      });
      $Info__item_edit.on("click", function () {
        return onClickEdit(data);
      });
      Object.defineProperty(data, "el", {
        enumerable: false,
        get: function get() {
          return DOM;
        }
      }); // 如果display为false，需要隐藏此项

      if (!_data.display) {
        $(DOM).addClass("hide");
      }

      Object.defineProperty(data, "display", {
        get: function get() {
          return _data.display;
        },
        set: function set(val) {
          _data.display = val;

          if (_data.display) {
            $(DOM).removeClass("hide");
          } else {
            $(DOM).addClass("hide");
          }
        }
      });
      return data;
    };

    _this.infoItemPart = function (data, onClickEdit) {
      var _data = Object.assign({
        title: "",
        children: [],
        editable: false,
        display: false
      }, data);

      var DOM = Render.create("<li class=\"info--parts\">\n                <div class=\"info--parts-title\">\n                    <span class=\"info--parts-title-text\">".concat(_data.title, "</span>\n                </div>\n            </li>"));
      var info_items = [];

      _data.children.map(function (info) {
        var infoItemDOM = _this.infoItemText(info, onClickEdit);

        if (infoItemDOM != undefined) {
          info_items.push(infoItemDOM.el);
        }
      });

      $(DOM).find(".info--item-edit").on("click", function () {
        onClickEdit(_data);
      });
      $(DOM).append(info_items); // 如果display为false，需要隐藏此项

      if (!_data.display) {
        $(DOM).addClass("hide");
      }

      Object.defineProperty(data, "display", {
        get: function get() {
          return _data.display;
        },
        set: function set(val) {
          _data.display = val;

          if (_data.display) {
            $(DOM).removeClass("hide");
          } else {
            $(DOM).addClass("hide");
          }
        }
      });
      return {
        el: DOM
      };
    };

    _this.process = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        title: "",
        process: [],
        onReply: function onReply(D_id, A_id, message) {}
      }, data);

      var DOM = Render.create("<div class=\"process\"><ul class=\"process-box\"></ul></div>");
      var $Process_box = $(DOM).find(".process-box");
      var process_items = [];

      if (_data.title) {
        $Process_box.append("<li style=\"margin-top:-2rem;line-height:3;\">".concat(_data.title, "</li>"));
      }

      _data.process.map(function (process) {
        if (process.end === true) {
          process_items.push(_this.processEndItem().el);
          return;
        }

        var processItemDOM = _this.processItem(_objectSpread(_objectSpread({}, process), {}, {
          complain_visible: data.complain_visible,
          onReply: _data.onReply
        }));

        if (processItemDOM != undefined) {
          process_items.push(processItemDOM.el);
        }
      });

      if (process_items.length > 0) {
        var last_process = process_items.slice(-1)[0];
        $(last_process).addClass("item-end");

        var _data2 = _data.process.slice(-1)[0];

        if (_data2.status == "approval") {
          $(last_process).addClass("now");
        }
      }

      $Process_box.append(process_items);
      return {
        el: DOM
      };
    };

    _this.processItem = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        title: "",
        D_id: "",
        A_id: "",
        avatar: Config.default_avatar,
        label: "",
        name: "",
        status: "",
        time: "",
        type: "",
        type_text: "",
        // +yk: 审批意见 {name, content}
        complain: [],
        reply: [],
        persons: [],
        // +yk: 是否对发起人可见，如果不可见，发起人的留言按钮去掉
        complain_visible: true,
        onReply: function onReply(D_id, A_id, message) {}
      }, data);

      _data.label_color = function (type) {
        var _type = {
          copy: "warning",
          approval: _data.status == "refuse" ? "danger" : "primary",
          original: ""
        };
        return _type[type] || "";
      }(_data.type);

      var DOM = Render.create("<li class=\"process--item\">\n                <div class=\"process--item-avatar skeleton\"><img src=\"".concat(_data.avatar, "\" alt=\"\" srcset=\"\"></div>\n                <div class=\"process--item-desc skeleton\">\n                    <div class=\"process--item-desc-box\">\n                        <div class=\"process--item-desc--title\">").concat(_data.title || _data.type_text, "</div>\n                        <div class=\"process--item-desc--time\">").concat(_data.time, "</div>\n                    </div>\n                </div>\n            </li>"));
      var $Process_item_desc = $(DOM).find(".process--item-desc");
      var FirstDescDOM = Render.create("<div class=\"process--item-desc-box\">\n                <div class=\"process--item-desc--name\">\n                    <span class=\"process--item-desc--name-box\">".concat(_data.name, "</span>\n                </div>\n                <div class=\"process--item-desc--status link-").concat(_data.label_color, "\">").concat(_data.label, "</div>\n            </div>")); // +yk: 除发起人不可见外，追加回复按钮
      // 可见时添加、不可见时部位流程的发起人节点添加

      if (_data.complain_visible || _data.type != "original") {
        $(FirstDescDOM).find(".process--item-desc--status").append("<div class=\"commenting\"><i class=\"fa fa-commenting\"></i></div>");
      }

      $(FirstDescDOM).find(".commenting").on("click", function (e) {
        e.stopPropagation();

        _this.reply({
          name: _data.name,
          onSubmit: function onSubmit(message) {
            _data.onReply(_data.D_id, _data.A_id, message);
          }
        }).render();
      });
      var personsDOM = Render.create("<div style=\"display:none\"></div>");

      _data.persons.map(function (person) {
        var _status = {};
        var label = "";

        switch (_data.type) {
          case "approval":
            _status = {
              // 1: {
              //     label: "审批中",
              //     color: "primary"
              // },
              2: {
                label: "已通过",
                color: "primary"
              },
              3: {
                label: "已拒绝",
                color: "danger"
              }
            };
            var __status = _status[person.status];

            if (__status) {
              label = "<span class=\"link-".concat(__status.color, "\">\uFF08").concat(__status.label, "\uFF09</span>");
            }

            break;

          case "copy":
            if (person.readed) {
              label = "<span class=\"link-warning\">\uFF08\u5DF2\u8BFB\uFF09</span>";
            }

            break;
        }

        $(personsDOM).append("<p class=\"process--item-desc--name--detail\">".concat(person.name, " ").concat(label, "</p>"));
      });

      if (_data.persons.length > 1) {
        $(FirstDescDOM).find(".process--item-desc--name-box").prepend("<i class=\"fa fa-angle-right\"></i>");
        $(FirstDescDOM).on("click", function () {
          $(FirstDescDOM).toggleClass("on");
          $(personsDOM).toggle();
        });
      }

      var ComplainBoxDOM = [];

      _data.complain.map(function (r) {
        ComplainBoxDOM.push(Render.create("<div class=\"process--item-desc-box\">\n                    <div class=\"process--item-desc--append\">".concat(r.name, "\u5BA1\u6279\u610F\u89C1\uFF1A").concat(r.content, "</div>\n                </div>")));
      });

      var DescBoxDOM = [];

      _data.reply.map(function (r) {
        DescBoxDOM.push(Render.create("<div class=\"process--item-desc-box\">\n                    <div class=\"process--item-desc--append\">".concat(r.name, "\u7559\u8A00\uFF1A").concat(r.reply, "</div>\n                </div>")));
      });

      $(FirstDescDOM).find(".process--item-desc--name-box").append(personsDOM);
      $Process_item_desc.append(FirstDescDOM); // $Process_item_desc.append(personsDOM);

      $Process_item_desc.append(ComplainBoxDOM);
      $Process_item_desc.append(DescBoxDOM);
      return {
        el: DOM
      };
    };

    _this.processEndItem = function () {
      return {
        el: Render.create("<li class=\"process--item item-end\">\n                    <div class=\"process--item-avatar\"><span>\u7ED3\u675F</span></div>\n                    <div class=\"process--item-desc\"></div>\n                </li>")
      };
    };

    _this.opera = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        data: {},
        onTrash: function onTrash() {},
        onBell: function onBell() {},
        onRetweet: function onRetweet() {},
        onClose: function onClose() {}
      }, data);

      var DOM = Render.create("<div class=\"opera\"></div>"); // 审批通过时显示作废按钮

      if (_data.data.approval_status == 1) {
        DOM = Render.create("<div class=\"opera\">\n                    <ul class=\"opera-box\">\n                        <li class=\"opera--item trash\"><i class=\"fa fa-trash\"></i> \u64A4\u9500</li>\n                        <li class=\"opera--item retweet\"><i class=\"fa fa-retweet\"></i> \u518D\u6B21\u53D1\u8D77</li>\n                        ".concat(_data.data.approval_identity == 1 ? '<li class="opera--item bell"><i class="fa fa-bell"></i> 提醒</li>' : "", "\n                        <!-- <li class=\"opera--item print\"><i class=\"fa fa-print\"></i> \u6253\u5370</li> -->\n                    </ul>\n                </div>"));
      } else if (_data.data.approval_status == 2 && !_data.data.is_invalid) {
        DOM = Render.create("<div class=\"opera\">\n                    <ul class=\"opera-box\">\n                        <li class=\"opera--item retweet\"><i class=\"fa fa-retweet\"></i> \u518D\u6B21\u53D1\u8D77</li>\n                        <li class=\"opera--item close\"><i class=\"fa fa-times\"></i> \u4F5C\u5E9F</li>\n                    </ul>\n                </div>");
      } else {
        DOM = Render.create("<div class=\"opera\">\n                    <ul class=\"opera-box\">\n                        <li class=\"opera--item retweet\"><i class=\"fa fa-retweet\"></i> \u518D\u6B21\u53D1\u8D77</li>\n                    </ul>\n                </div>");
      }

      if (_data.data.category == 2) {
        $(DOM).find(".retweet").remove();
      }

      $(DOM).find(".trash").on("click", function () {
        _data.onTrash();
      });
      $(DOM).find(".bell").on("click", function () {
        _data.onBell();
      });
      $(DOM).find(".retweet").on("click", function () {
        _data.onRetweet();
      });
      $(DOM).find(".close").on("click", function () {
        _data.onClose();
      });
      $(DOM).find(".print").on("click", function () {
        _this.toast("请用电脑登录yun.5tree.cn进行打印操作").render();
      });
      return {
        el: DOM
      };
    };

    _this.tabbar = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        onPass: function onPass(done) {},
        onRefuse: function onRefuse(done) {}
      }, data);

      var DOM = Render.create("<div class=\"tabbar\">\n                <div class=\"tabbar-box\">\n                    <button class=\"btn btn-light refuse\">\u62D2\u7EDD</button>\n                    <button class=\"btn btn-primary pass\">\u901A\u8FC7</button>\n                </div>\n            </div>");
      $(DOM).find(".refuse").on("click", function () {
        _data.onRefuse();
      });
      $(DOM).find(".pass").on("click", function () {
        _data.onPass();
      });
      return {
        el: DOM
      };
    };

    var _CACHE_REPLY_DOMS;

    _this.reply = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        name: "",
        onSubmit: function onSubmit() {}
      }, data);

      var DOM = Render.create("<div class=\"reply\">\n                <div class=\"reply-box\">\n                    <input type=\"text\" class=\"reply-text\" placeholder=\"\u56DE\u590D@".concat(_data.name, "\" />\n                    <button type=\"button\" class=\"btn btn-success reply-submit\">\u7559\u8A00</button>\n                </div>\n            </div>"));
      var $Reply_text = $(DOM).find(".reply-text");
      Util.iosTextBlurScroll($Reply_text[0]);
      $(DOM).find(".reply-submit").on("click", function () {
        var text = $Reply_text.val();

        _data.onSubmit(text);
      });
      $(DOM).on("click", function () {
        $(DOM).removeClass("on");
        $("body").removeClass("clamp");
        setTimeout(function () {
          _CACHE_REPLY_DOMS.parentNode.removeChild(_CACHE_REPLY_DOMS);

          _CACHE_REPLY_DOMS = undefined;
        }, 300);
      });
      $(DOM).children().on("click", function (e) {
        e.stopPropagation();
      });
      return {
        el: DOM,
        render: function render() {
          // 清除上一次显示的DOM
          if (_CACHE_REPLY_DOMS) {
            _CACHE_REPLY_DOMS.parentNode.removeChild(_CACHE_REPLY_DOMS);
          }

          _CACHE_REPLY_DOMS = DOM;
          $("body").addClass("clamp");
          $("body").append(DOM); // 默认选中

          setTimeout(function () {
            $(DOM).addClass("on");
            setTimeout(function () {
              $Reply_text.trigger("focus");
            }, 200);
          }, 0);
        }
      };
    };

    _this.mediaBody = function (data) {
      console.log(data.original.type);
      var editator = Editate["editate_" + data.original.type](data); // let op = getListHtml(data.original, data.original.type, data.id);

      return {
        el: editator.el
      };
    };

    _this.complain = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({}, data);

      var DOM = Render.create("<div>\n                <textarea class=\"complain-text\" data-media-form-name=\"complain\" row=\"8\" maxlength=\"1000\"></textarea>\n            </div>");
      var $Complain_text = $(DOM).find(".complain-text");
      Util.iosTextBlurScroll($Complain_text[0]);

      if (_data.required) {
        $Complain_text.attr("placeholder", _data.approval_tips == "" ? "请输入审批意见（必填）" : _data.approval_tips);
      } else {
        $Complain_text.attr("placeholder", _data.approval_tips == "" ? "请输入审批意见" : _data.approval_tips);
      }

      return {
        el: DOM
      };
    };
    /**
     * 警告文字
     */


    _this.warningText = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var _data = Object.assign({
        text: "",
        type: "success"
      }, data);

      var DOM = Render.create("<div class=\"warning-text\">\n                <div class=\"warning-text-box\"><p class=\"link-".concat(_data.type, "\">").concat(_data.text, "</p></div>\n            </div>"));
      return {
        el: DOM
      };
    };
    /**
     * 印章
     * 根据状态显示印章图片。
     * 2审批通过 3审批拒绝 4审批撤销
     */


    _this.stamp = function (status) {
      var stamps = {
        "2": "./assets/images/sptg.png",
        "3": "./assets/images/spjj.png",
        "4": "./assets/images/spcx.png",
        "7": "./assets/images/spzf.png"
      };
      var stamp = stamps[status];

      if (stamp) {
        var DOM = Render.create("<div class=\"stamp\"><img src=\"".concat(stamp, "\" alt=\"\"/></div>"));
        return {
          el: DOM
        };
      }
    };

    return new Component();
  }();

  var Api = function () {
    var Api = function Api() {};

    var _this = Api.prototype; // 接口公共地址前缀

    _this.base_url = Config.api.base_url + "/form/approval/"; // 构建一个统一请求方式

    _this.exec = {
      // 请求的所有数据
      query: {
        token: Config.user.token,
        udid: Config.user.udid,
        user_id: Config.user.id,
        version: Config.user.version,
        org_id: Config.user.org_id
      },
      // 暂存的用户数据数组
      params: {},
      callback: function callback() {},
      fail: function fail() {}
    };
    /**
           * 进行一个post请求
           */

    function _post(url, data, callback, fail) {
      var params = new FormData();

      for (var name in data) {
        params.append(name, data[name]);
      }

      _this.exec.query = {
        token: Config.user.token,
        udid: Config.user.udid,
        user_id: Config.user.id,
        version: Config.user.version,
        org_id: Config.user.org_id // category: 1,
        // subject: 1

      };

      for (var _name in _this.exec.query) {
        params.append(_name, _this.exec.query[_name]);
      }

      return $.ajax({
        url: url,
        type: 'POST',
        data: params,
        cache: false,
        processData: false,
        contentType: false,
        headers: {
          token: Config.user.token,
          udid: Config.user.udid,
          user_id: Config.user.id,
          version: Config.user.version,
          org_id: Config.user.org_id // category: 1,
          // subject: 1

        }
      }).done(function (result) {
        // 如果请求成功，但接口返回失败，提示错误
        if (result.success !== true) {
          fail && fail({
            code: result.code,
            message: result.message
          });
          return;
        }

        callback && callback(result, true);
      }).fail(function (e) {
        // 如果是手动中断，不弹出提示
        if (e.statusText == 'abort') {
          return false;
        }

        fail && fail({
          code: -1,
          message: '服务器繁忙，请重试'
        });
      });
    }

    ;

    function _get(url, data, callback, fail) {
      var params = [];

      for (var name in data) {
        params.push(name + "=" + data[name]);
      }

      _this.exec.query = {
        token: Config.user.token,
        udid: Config.user.udid,
        user_id: Config.user.id,
        version: Config.user.version,
        org_id: Config.user.org_id,
        category: 1,
        subject: 1
      };

      for (var _name2 in _this.exec.query) {
        params.push(_name2 + "=" + _this.exec.query[_name2]);
      }

      return $.ajax({
        url: url,
        type: 'GET',
        data: params.join("&"),
        cache: false,
        processData: false,
        contentType: false,
        headers: {
          token: Config.user.token,
          udid: Config.user.udid,
          user_id: Config.user.id,
          version: Config.user.version,
          org_id: Config.user.org_id
        }
      }).done(function (result) {
        // 如果请求成功，但接口返回失败，提示错误
        if (result.success !== true) {
          fail && fail({
            code: result.code,
            message: result.message
          });
          return;
        }

        callback && callback(result, true);
      }).fail(function (e) {
        // 如果是手动中断，不弹出提示
        if (e.statusText == 'abort') {
          return false;
        }

        fail && fail({
          code: -1,
          message: '服务器繁忙，请重试'
        });
      });
    }

    ;
    _this.post = _post;
    _this.get = _get;

    _this.exec.post = function (name) {
      var exec = _this.exec;
      var url = _this.base_url + _this.urls[name];

      _post(url, exec.params, exec.callback, exec.fail);
    };
    /**
           * 快速执行一个接口
           * 使用于普通通用的调用情况
           */


    _this.run = function (name, params, callback, fail) {
      _this.exec.params = params;

      if (Object.prototype.toString.call(callback) == '[object Function]') {
        _this.exec.callback = callback;
      }

      if (Object.prototype.toString.call(fail) == '[object Function]') {
        _this.exec.fail = fail;
      }

      _this.exec.post(name);
    };

    _this.resource = function (url) {
      var imgName = url.split(".")[0];
      imgName = Config.api.base_url + "/esb/res/pic/" + Math.floor(+imgName / 10000) + "/" + Math.floor(+imgName / 100) + "/" + url;
      return imgName;
    };

    _this.jssdkRegister = function (callback) {
      var _loading = Component.loading().render(),
          isQYWX = sessionStorage.isFromQYWX == '1' || Util.getQuery().from_src == 'qywx',
          jssdkRegisterUrl = '/shijiwxy/wechat/portal/getWxJsConfig.json',
          jssdkRegisterParams = {
        url: window.location.href
      }; // 如果是企业微信


      if (isQYWX) {
        jssdkRegisterParams['appName'] = 'wxy';
        jssdkRegisterParams['orgId'] = Config.user.org_id;
        jssdkRegisterUrl = '/qywxy/qywechat/service/getCorpJsConfig.htm';
      }

      _this.post(Config.api.base_url + jssdkRegisterUrl, jssdkRegisterParams, function (result) {
        var access = result.data;
        wx.config({
          debug: false,
          appId: access.appId,
          timestamp: access.timestamp,
          nonceStr: access.nonceStr,
          signature: access.signature,
          jsApiList: ['checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone', 'hideMenuItems', 'showMenuItems', 'hideAllNonBaseMenuItem', 'showAllNonBaseMenuItem', 'translateVoice', 'startRecord', 'stopRecord', 'onVoiceRecordEnd', 'playVoice', 'onVoicePlayEnd', 'pauseVoice', 'stopVoice', 'uploadVoice', 'downloadVoice', 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage', 'getNetworkType', 'openLocation', 'getLocation', 'hideOptionMenu', 'showOptionMenu', 'closeWindow', 'scanQRCode', 'chooseWXPay', 'openProductSpecificView']
        });

        _loading.hide(function () {
          callback && callback();
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    }; // 所有用到的接口名称


    _this.urls = {
      // 审批详情
      getApprovalInfo: "getApprovalInfo.json",
      // 通过拒绝
      approval: "approval.json",
      // 提醒
      sendApprovalUser: "sendApprovalUser.json",
      // 获取提醒列表
      getApprovalUser: "getApprovalUser.json",
      // 撤销
      revokeApproval: "revokeApproval.json",
      // 节点留言
      approvalLeaveMessage: "approvalLeaveMessage.json",
      // 作废流程
      getStartApprovalInfo: "getStartApprovalInfo.json",
      // 作废
      saveFormDetail: "saveFormDetail.json"
    };

    _this.getApprovalInfo = function (_ref, callback, fail) {
      var AD_id = _ref.AD_id;

      var _loading = Component.loading("数据加载中").render();

      this.run("getApprovalInfo", {
        formApprovalDetailId: AD_id
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.approval = function (_ref2, callback, fail) {
      var D_id = _ref2.D_id,
          A_id = _ref2.A_id,
          AD_id = _ref2.AD_id,
          pass = _ref2.pass,
          _ref2$complain = _ref2.complain,
          complain = _ref2$complain === void 0 ? "" : _ref2$complain,
          _ref2$formDetailJson = _ref2.formDetailJson,
          formDetailJson = _ref2$formDetailJson === void 0 ? undefined : _ref2$formDetailJson;

      var _loading = Component.loading().render();

      var params = {
        formDetailId: D_id,
        formApprovalId: A_id,
        formApprovalDetailId: AD_id,
        result: pass,
        content: complain
      };

      if (formDetailJson) {
        params.formDetailJson = formDetailJson;
      }

      this.run("approval", params, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.sendApprovalUser = function (_ref3, callback, fail) {
      var D_id = _ref3.D_id;

      var _loading = Component.loading().render();

      this.run("sendApprovalUser", {
        formDetailId: D_id
      }, function (result) {
        _loading.hide(function () {
          callback && callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
          fail && fail(e);
        });
      });
    };

    _this.getApprovalUser = function (_ref4, callback, fail) {
      var D_id = _ref4.D_id;

      var _loading = Component.loading().render();

      this.run("getApprovalUser", {
        formDetailId: D_id
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.revokeApproval = function (_ref5, callback, fail) {
      var D_id = _ref5.D_id;

      var _loading = Component.loading().render();

      this.run("revokeApproval", {
        formDetailId: D_id
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.approvalLeaveMessage = function (_ref6, callback, fail) {
      var approval_identity = _ref6.approval_identity,
          student_id = _ref6.student_id,
          D_id = _ref6.D_id,
          A_id = _ref6.A_id,
          message = _ref6.message;

      var _loading = Component.loading().render();

      this.run("approvalLeaveMessage", {
        formDetailId: D_id,
        formApprovalId: A_id,
        content: message,
        studentId: student_id || '',
        identity: approval_identity
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.getRange = function (_ref7, callback, fail) {
      _objectDestructuringEmpty(_ref7);

      var _loading = Component.loading().render();

      _this.post(Config.api.base_url + '/form/basedata/getRange.json', {
        range: "",
        identity: 1
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.getStudent = function (_ref8, callback, fail) {
      var id = _ref8.id;

      var _loading = Component.loading().render();

      _this.get(Config.api.base_url + '/esb/api/student/getStudentsByCid', {
        clas_id: id
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };
    /**
     * 上传
     */


    _this.upload = function (_ref9, callback, fail) {
      var mediaIds = _ref9.mediaIds;

      _this.post(Config.api.base_url + '/eduWeixin/uploadMedias', {
        mediaIds: mediaIds
      }, function (result) {
        callback(result);
      }, function (e) {
        fail && fail(e);
      });
    };
    /** 
     * 上传图片，通过base64
     */


    _this.uploadStudImage = function (_ref10, callback, fail) {
      var base64 = _ref10.base64,
          width = _ref10.width,
          height = _ref10.height;

      _this.post(Config.api.base_url + '/esb/api/uploadBase64', {
        file: base64.split(',')[1],
        width: width,
        height: height,
        ext: "jpg"
      }, function (data) {
        callback && callback(data);
      }, fail);
    };

    _this.getStartApprovalInfo = function (_ref11, callback, fail) {
      var D_id = _ref11.D_id;

      var _loading = Component.loading().render();

      this.run("getStartApprovalInfo", {
        formDetailId: D_id
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };

    _this.saveFormDetail = function (_ref12, callback, fail) {
      var form_id = _ref12.form_id,
          json = _ref12.json,
          D_id = _ref12.D_id,
          insert_stud_id = _ref12.insert_stud_id;

      var _loading = Component.loading().render();

      this.run("saveFormDetail", {
        formId: form_id,
        formDetailJson: json,
        isInvalid: '1',
        oldFormDetailId: D_id,
        insertStudId: insert_stud_id
      }, function (result) {
        _loading.hide(function () {
          callback(result);
        });
      }, function (e) {
        _loading.hide(function () {
          Component.toast(e.message).render();
        });
      });
    };
    /** 
     * 再次发起
     */


    _this.replyApprovalInfo = function (_ref13) {
      var approval_identity = _ref13.approval_identity,
          D_id = _ref13.D_id,
          form_id = _ref13.form_id,
          model_id = _ref13.model_id;
      var techOrParent = approval_identity == "0" ? "parent" : "teacher";
      location.href = "/shijiwxy/weixin/html/" + techOrParent + "/approval/approvalInfo.html?" + ["d=" + D_id, "formId=" + form_id, "formModelId=" + model_id].join("&");
    };

    var address = null;

    _this.getAddress = function () {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var callback = arguments.length > 1 ? arguments[1] : undefined;
      var fail = arguments.length > 2 ? arguments[2] : undefined;

      if (address) {
        callback && callback(address, true);
        return;
      }

      var _loading = Component.loading().render();

      var fd = new FormData();
      fd.append("token", Config.user.token);
      fd.append("udid", Config.user.udid);
      fd.append("version", Config.user.version);
      fd.append("org_id", Config.user.org_id);
      $.ajax({
        url: Config.api.base_url + "/studentfiles/registration/getDistrictSelectJson.htm",
        type: 'POST',
        data: fd,
        cache: false,
        processData: false,
        contentType: false
      }).done(function (result) {
        _loading.hide();

        result = JSON.parse(result);
        address = JSON.parse(result.data);
        callback && callback(JSON.parse(result.data), true);
      }).fail(function (e) {
        _loading.hide(); // 如果是手动中断，不弹出提示


        if (e.statusText == 'abort') {
          return false;
        }

        fail && fail({
          code: -1,
          message: '服务器繁忙，请重试'
        });
      });
    };

    return new Api();
  }();

  var Editate = function () {
    var Editate = function Editate() {};

    var _this = Editate.prototype;
    var _editate_DOM = {};
    var _editate_data = {};
    /**
       * 创建上传，成功后执行回调 callback(src)
       */

    _this.uploader = function (data, callback) {
      switch (data.__pictureSelectMode) {
        case "localxcut":
          _this.uploaderOnLocalxCut(data, callback);

          break;

        case "wechat":
          _this.uploaderOnWechat(data, callback);

      }
    };
    /** 
       * 通过本地图片控件上传图片并支持裁剪
       */


    _this.uploaderOnLocalxCut = function (data, callback) {
      var value = _editate_DOM["7"].__data.value;
      var choosed_length = 0;

      if (value != "") {
        choosed_length = Util.type(value) == "string" ? value.split(",").length : value.length;
      }

      var imgsArr = []; // 如果达到上限，不再打开上传

      if (data.max_image_length - choosed_length <= 0) {
        return;
      } // 尺寸方式


      var size = data.original.uploadImgSize; // size = 1时使用微信上传方式

      if (size == 1) {
        _this.uploaderOnWechat(data, callback);

        return;
      }

      var InputFile = Render.create("<input type=\"file\" accept=\"image/*\" style=\"display:none\"/>");
      var OperaDom = Render.create("<div></div>");
      OperaDom.style.cssText = "position:absolute;z-index:2001;bottom:1rem;display:flex;width:100%;";
      var OkButton = Render.create("<button class=\"btn btn-primary\" style=\"margin:1rem\">\u622A\u53D6</button>");
      var CancelButton = Render.create("<button class=\"btn btn-light\" style=\"margin:1rem\">\u53D6\u6D88</button>");
      CancelButton.addEventListener("click", function () {
        ClipContainer.style.visibility = "hidden";
      });
      OkButton.addEventListener("click", function () {
        ClipContainer.style.visibility = "hidden";
      });
      OperaDom.appendChild(OkButton);
      OperaDom.appendChild(CancelButton);
      var ClipView = Render.create("<div></div>");
      ClipView.style.cssText = "height:86%;";
      var ClipContainer = Render.create("<div></div>");
      ClipContainer.style.cssText = "visibility:hidden;position:fixed;z-index:20;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,.4);";
      ClipContainer.appendChild(ClipView);
      ClipContainer.appendChild(OperaDom);
      document.body.appendChild(ClipContainer);
      var sizeBox = {
        "2": [295, 413],
        "3": [413, 626],
        "4": [358, 441]
      },
          adaptive = {
        "2": ["90%"],
        "3": ["90%"],
        "4": ["90%"]
      };
      var pc = new PhotoClip(ClipView, {
        width: sizeBox[size][0],
        height: sizeBox[size][1],
        size: sizeBox[size],
        adaptive: adaptive[size],
        outputSize: sizeBox[size],
        file: InputFile,
        // file: "#tempClipValue",
        view: ClipView,
        ok: OkButton,
        loadStart: function loadStart(file) {},
        loadComplete: function loadComplete(file) {
          ClipContainer.style.visibility = "visible";
        },
        done: function done(base64) {
          pc.clear();
          var loading = Component.loading("图片裁剪中").render();
          Api.uploadStudImage({
            base64: base64,
            width: sizeBox[size][0],
            height: sizeBox[size][1]
          }, function (data) {
            var name = data.data;

            var changeImgUrl = function changeImgUrl(url) {
              var imgName = url.split(".")[0];
              imgName = "/esb/res/pic/" + Math.floor(+imgName / 10000) + "/" + Math.floor(+imgName / 100) + "/" + url;
              return imgName;
            };

            loading.hide();
            callback && callback([changeImgUrl(name)]);
          });
        },
        fail: function fail(msg) {
          Component.toast(msg || "裁剪失败");
        }
      });
      InputFile.click();
    };
    /** 
       * 通过微信控件上传图片
       */


    _this.uploaderOnWechat = function (data, callback) {
      var value = _editate_DOM["7"].__data.value;
      var choosed_length = 0;

      if (value != "") {
        choosed_length = Util.type(value) == "string" ? value.split(",").length : value.length;
      }

      var imgsArr = []; // 如果达到上限，不再打开上传

      if (data.max_image_length - choosed_length <= 0) {
        return;
      }

      wx.chooseImage({
        count: data.max_image_length - choosed_length,
        //可上传图片数量
        //sizeType: ['original'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: data.source_type,
        // 可以指定来源是相册还是相机，默认二者都有
        success: function success(res) {
          var localIds = res.localIds;

          var uploadimg = function uploadimg(imgIndex) {
            wx.uploadImage({
              localId: localIds[imgIndex],
              // 需要上传的图片的本地ID，由chooseImage接口获得
              isShowProgressTips: 0,
              // 默认为1，显示进度提示
              success: function success(res) {
                imgsArr.push(res.serverId);
                console.log(imgsArr);

                if (imgsArr.length < localIds.length) {
                  uploadimg(imgIndex + 1);
                } else {
                  setTimeout(function () {
                    var _loading = Component.loading("上传中").render();

                    Api.upload({
                      mediaIds: JSON.stringify(imgsArr) // 返回图片的服务器端ID

                    }, function (result) {
                      _loading.hide(function () {
                        callback && callback(result.data.map(function (src) {
                          return Api.resource(src);
                        }));
                      });
                    }, function (e) {
                      _loading.hide(function () {
                        Component.toast(e.message).render();
                      });
                    });
                  }, 100);
                }
              },
              fail: function fail(error) {
                Component.toast(error).render();
                console.log(error);
              }
            });
          }; //开始通过本地获取的localId上传获取对应的serverId最后调用内部服务获取内部服务器上该图片的真实路径


          uploadimg(0);
        },
        fail: function fail(error) {
          _loading.hide(function () {
            Component.toast(error).render();
          });

          console.log(error);
        }
      });
    };

    _this.normal = function (template) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var DOM = Render.create(template);
      DOM.__data = {};
      DOM.__data.value = data.value;
      DOM.__data.value_text = data.value_text;
      var last_value = data.value;

      switch (data.original.type) {
        case "3":
          $(DOM).find(".items-item").on("input", function () {
            if (this.value != "" && isNaN(this.value)) {
              this.value = last_value;
            }

            last_value = this.value;
            DOM.__data.value = DOM.__data.value_text = this.value;
          });
          break;

        case "6":
          $(DOM).find(".items-item").on("input", function () {
            if (this.value != "" && isNaN(this.value)) {
              this.value = last_value;
            }

            last_value = this.value;
            DOM.__data.value = this.value;
            DOM.__data.value_text = Util.convertCurrency(DOM.__data.value) || "金额大写";
            $(DOM).find(".uppercase").html(DOM.__data.value_text);
          });
          break;

        case "51":
        default:
          $(DOM).find(".items-item").on("input", function () {
            DOM.__data.value = DOM.__data.value_text = this.value;
          });
      }

      return {
        el: DOM
      };
    };

    _this.drawer = function (data) {
      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, " ").concat(data.original.limit == 1 ? "" : "（支持多选）", "</div>\n\n                <div class=\"items-item\">").concat(data.placeholder, "</div>\n\n            </div>"));
      DOM.__data = {
        value: [],
        value_text: []
      };
      return {
        el: DOM
      };
    };

    _this.drawerTreeDataMap = function (data, options) {
      var deep = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
      var trees = [];
      var drawerDOM = Render.create("<div></div>");
      data.map(function (item) {
        var DOM = Render.create("<div class=\"member-list\">\n\n                    <div class=\"member-list-box\">\n\n                        <div class=\"item-label member-list--label\">\n\n                            <div>".concat(item.label, "</div>\n\n                            <div class=\"sub\">").concat(item.sub_label || "", "</div>\n\n                        </div>\n\n                        <label class=\"member-list--checkbox\">\n\n                            <input type=\"checkbox\" class=\"item-checkbox level").concat(deep, "-checkbox\" data-label=\"").concat(item.label, "\" value=\"").concat(item.id, "\" ").concat(options.selected.includes(item.id + "") ? "checked" : "", ">\n\n                            <div class=\"item-checked\"></div>\n\n                        </label>\n\n                    </div>\n\n                    <div class=\"member-list-children\"></div>\n\n                </div>"));
        var $Member_list_box = $(DOM).children(".member-list-box");
        var $Member_list_children = $(DOM).children(".member-list-children");
        $Member_list_box.find(".member-list--checkbox")[0].__data = item;
        $Member_list_box.find(".member-list--checkbox").on("click", function (e) {
          e.stopPropagation();
        }); // 允许下一级数据时，根据结构渲染下一级内容

        if (deep < options.deep) {
          $Member_list_box.prepend("<div class=\"member-list--treei\"></div>");

          var children = _this.drawerTreeDataMap(item.children, options, deep + 1);

          $Member_list_children.append(children);
          $Member_list_box.on("click", function () {
            if ($(DOM).hasClass("on")) {
              $(DOM).removeClass("on");
            } else {
              if (item.children.length == 0) {
                Api.getStudent({
                  id: item.id
                }, function (result) {
                  var students = [];
                  result.data.map(function (student) {
                    students.push({
                      id: student.stud_id,
                      type: "s",
                      label: student.stud_name,
                      children: []
                    });
                  });
                  item.children = students;
                  children = _this.drawerTreeDataMap(item.children, options, deep + 1);
                  $Member_list_children.append(children);
                  $(DOM).addClass("on");
                });
              } else {
                $(DOM).addClass("on");
              }
            }
          });
          $Member_list_box.find(".item-checkbox").on("change", function () {
            // 选中全部子项
            $Member_list_children.find(".item-checkbox").prop("checked", this.checked); // 通知上一层

            $(drawerDOM).trigger("choose", this.checked);
          }); // 子项选择后，该级是否跟随选中处理

          $(children).on("choose", function (e, checked) {
            if (!checked) {
              $Member_list_box.find(".item-checkbox").prop("checked", false);
            } else {
              // 如果是子项选中，判断子项是否全部选中，如果全部选中，该级再选中
              $Member_list_box.find(".item-checkbox").prop("checked", $Member_list_children.find(".item-checkbox:not(:checked)").length == 0);
            }
          });
        }

        trees.push(DOM);
      });
      $(drawerDOM).append(trees);
      return drawerDOM;
    };

    _this.drawerTree = function () {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      options = Object.assign({
        // 最多允许三级
        deep: 3,
        data: [],
        selected: []
      }, options);

      if (options.deep > 3) {
        options.deep = 3;
      }

      return {
        el: _this.drawerTreeDataMap(options.data, options)
      };
    };
    /**
       * 选人抽屉使用这个方法
       */


    _this.editateDrawer = function (data, editate_type) {
      var deep = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 2;
      editate_type += "";

      var drawer = _this.drawer(data);

      var DOM = drawer.el;
      var $Items_item = $(DOM).find(".items-item");

      if (data.value != "") {
        DOM.__data.value = data.value.split(",");
      }

      if (data.value_text != "") {
        DOM.__data.value_text = data.value_text.split(",");
        $Items_item.html(DOM.__data.value_text);
      }

      $Items_item.on("click", function () {
        _this.initEditateData(editate_type, data, function (drawer_data) {
          var drawerTree = _this.drawerTree({
            deep: deep,
            data: drawer_data,
            selected: DOM.__data.value
          });

          _editate_DOM[editate_type] = drawerTree.el;
          var body = _editate_DOM[editate_type]; // let stretch = Render.create(`<div>已选：</div>`);

          Component.drawer({
            body: body,
            // stretch: stretch,
            closeByBlank: false,
            onComplete: function onComplete() {
              var keyMap = [];
              $(body).find(".level".concat(deep, "-checkbox:checked")).map(function (index, input) {
                keyMap.push(input);
              }); // 如果未选择，保留上一次的值
              // if (keyMap.length == 0) {
              //     let __deep = deep;
              //     while(--__deep > 0) {
              //         if ($(body).find(`.level${__deep}-checkbox:checked`).length > 0) {
              //             Component.toast(`只能选择一名学生`).render();
              //             return false;
              //         }
              //     }
              //     return true;
              // }

              if (keyMap.length > data.original.limit) {
                Component.toast("\u6700\u591A\u9009\u62E9".concat(data.original.limit, "\u9879")).render();
                return false;
              }

              DOM.__data.value.length = 0;
              DOM.__data.value_text.length = 0;

              for (var index in keyMap) {
                var input = keyMap[index];

                DOM.__data.value.push(input.value);

                DOM.__data.value_text.push(input.getAttribute("data-label"));
              }

              $Items_item.html(DOM.__data.value_text.join(",") || "请选择");
            }
          }).render();
        });
      });
      return DOM;
    };

    _this.initEditateData = function (type, data, callback) {
      type += "";

      if (!Util.isEmpty(_editate_data[type])) {
        return callback(_editate_data[type]);
      }

      var options = data.original.options;

      switch (type) {
        case "5":
          callback(Util.likeItemObjectMap(options, function (option, index) {
            return {
              id: index,
              label: option.text,
              type: ""
            };
          }));
          break;

        case "90":
        case "91":
          Api.getRange({}, function (result) {
            var teacher_list = [];

            if (result.data && result.data.list.length > 0) {
              teacher_list = result.data.list;
            }

            var dep_obj = {};
            teacher_list.forEach(function (teacher) {
              if (!dep_obj[teacher.dep_id]) {
                dep_obj[teacher.dep_id] = {
                  id: teacher.dep_id,
                  label: teacher.dep_name,
                  type: "d",
                  children: []
                };
              }

              if (!dep_obj[teacher.dep_id].children[teacher.tech_id]) {
                dep_obj[teacher.dep_id].children.push({
                  id: teacher.tech_id,
                  label: teacher.tech_name,
                  type: "t",
                  sub_label: teacher.user_loginname,
                  children: []
                });
              }
            });
            _editate_data[type] = Object.values(dep_obj);
            callback(_editate_data[type]);
          });
          break;

        case "92":
        case "93":
          var grades = JSON.parse(sessionStorage.allClass);
          var _data3 = [];

          for (var i in grades) {
            var grade = grades[i];

            _data3.push({
              id: grade.grade_id,
              label: grade.grade_name,
              type: "g",
              children: grade.classList
            });
          }

          _data3.map(function (item) {
            var child = [];

            for (var child_id in item.children) {
              child.push({
                id: child_id,
                label: item.children[child_id].className,
                type: "c",
                children: []
              });
            }

            item.children = child;
          });

          _editate_data[type] = _data3;
          callback(_editate_data[type]);
          break;
      }
    };
    /**
       * 单行输入框
       */


    _this.editate_1 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"text\" class=\"items-item\" value=\"").concat(data.value, "\" placeholder=\"").concat(data.placeholder, "\">\n\n            </div>"), data);
    };
    /**
       * 多行输入框
       */


    _this.editate_2 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <textarea class=\"items-item\" placeholder=\"").concat(data.placeholder, "\">").concat(data.value, "</textarea>\n\n            </div>"), data);
    };
    /**
       * 数字输入框
       */


    _this.editate_3 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"text\" class=\"items-item\" value=\"").concat(data.value, "\" placeholder=\"").concat(data.placeholder, "\">\n\n            </div>"), data);
    };
    /**
       * 单选下拉框
       */


    _this.editate_4 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (Util.isEmpty(data.placeholder)) {
        data.placeholder = "请选择";
      }

      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <select class=\"form-select items-item\"><option value=\"\">").concat(data.placeholder, "</option></select>\n\n            </div>"));
      var $Items_item = $(DOM).find(".items-item");
      var keyMap = {
        "": ""
      };
      Util.likeItemObjectMap(data.original.options, function (option, index) {
        $Items_item.append("<option value=\"".concat(index, "\" ").concat(data.value == index ? "selected" : "", ">").concat(option.text, "</option>"));
        keyMap[index] = option.text;
      });
      DOM.__data = {
        value: "",
        value_text: ""
      };
      $Items_item.on("change", function () {
        // DOM.__data.value = this.value;
        //将单选下拉框的value与value_text保持一致
        DOM.__data.value = keyMap[this.value];
        DOM.__data.value_text = keyMap[this.value];
      });
      return {
        el: DOM
      };
    };
    /**
       * 多选下拉框
       */


    _this.editate_5 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return {
        el: _this.editateDrawer(data, "5", 1)
      };
    };
    /**
       * 金额输入框
       */


    _this.editate_6 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"text\" class=\"items-item\" value=\"").concat(data.value, "\" maxlength=\"15\" placeholder=\"").concat(data.placeholder, "\">\n\n                <div class=\"uppercase\">").concat(data.original.uppercase ? "金额大写" : "", "</div>\n\n            </div>"), data);
    };
    /**
       * 日期
       */


    _this.editate_61 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"text\" class=\"items-item\" value=\"").concat(data.value, "\" placeholder=\"").concat(data.placeholder, "\">\n\n            </div>"));
      DOM.__data = {
        value: data.value || "",
        value_text: data.value_text || ""
      };
      var $Items_item = $(DOM).find(".items-item");
      $Items_item.on("change", function () {
        DOM.__data.value = DOM.__data.value_text = this.value;
      });
      var datetimePickerOptions = {};

      if (data.date_type == '年-月-日 时：分') {
        if (data.value != "") {
          datetimePickerOptions.value = data.value.trim().split(/[-\/: ]/);
        } else {
          var NW = Util.getCalendarDate();
          datetimePickerOptions.value = "".concat(NW.format, " ").concat(NW.minuteTimeFormat).trim().split(/[-\/: ]/);
        } // 绑定日期组件


        $($Items_item).datetimePicker(datetimePickerOptions);
      } else if (data.date_type == '年-月-日') {
        // 绑定日期组件
        $($Items_item).calendar({
          value: data.value ? [data.value] : undefined
        });
      }

      return {
        el: DOM
      };
    };
    /**
       * 上传图片
       */


    _this.editate_7 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <div class=\"items-item\"></div>\n\n            </div>"));
      DOM.__data = {
        value: data.value,
        value_text: data.value_text
      };
      _editate_DOM["7"] = DOM;
      var PreviewDOM = Render.create("<div class=\"img-preview\" style=\"float:left\"></div>");
      var UploadDOM = Render.create("<div class=\"img-item img-uploader\">\n\n                    <i class=\"img-icon fa fa-camera\"></i>\n\n                    <span>\u6DFB\u52A0\u56FE\u7247</span>\n\n                    <div id=\"clipView\"></div>\n\n                </div>");

      if (DOM.__data.value != "") {
        if (Util.type(DOM.__data.value) == "string") {
          DOM.__data.value = DOM.__data.value.split(",");
        }

        var ImgDOMs = DOM.__data.value.map(function (src) {
          return _this.editate_7_item(src, data).el;
        });

        _this.editate_7_save(data);

        $(PreviewDOM).append(ImgDOMs);
      }

      $(UploadDOM).on("click", function () {
        data.__pictureSelectMode = "localxcut";

        _this.uploader(data, function (srcs) {
          var ImgDOMs = srcs.map(function (src) {
            return _this.editate_7_item(src, data).el;
          });
          $(PreviewDOM).append(ImgDOMs);

          _this.editate_7_save(data);
        });
      });
      $(DOM).children(".items-item").append([PreviewDOM, UploadDOM]);
      return {
        el: DOM
      };
    };

    _this.editate_7_item = function (src, data) {
      var ImgDOM = Render.create("<div class=\"img-item\">\n\n                <img class=\"image\" src=\"".concat(src, "\" alt=\"\">\n\n                <div class=\"img-item-remove\"><i class=\"remove-img fa fa-trash-o\"></i></div>\n\n            </div>"));
      $(ImgDOM).find(".img-item-remove").on("click", function () {
        ImgDOM.parentNode.removeChild(ImgDOM);

        _this.editate_7_save(data);
      });
      return {
        el: ImgDOM
      };
    };

    _this.editate_7_save = function (data) {
      setTimeout(function () {
        var images = [];
        $(_editate_DOM["7"]).find(".img-preview").find(".image").map(function (index, image) {
          images.push(image.src);
        });
        _editate_DOM["7"].__data.value = _editate_DOM["7"].__data.value_text = images;

        if (images.length < data.max_image_length) {
          $(_editate_DOM["7"]).find(".img-uploader").show();
        } else {
          $(_editate_DOM["7"]).find(".img-uploader").hide();
        }
      }, 0);
    };
    /**
       * 手写签名
       */


    _this.editate_71 = function (data) {
      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <div class=\"items-item\">").concat(data.placeholder, "</div>\n\n            </div>")),
          placeholder = data.placeholder;
      DOM.__data = {
        value: data.value,
        value_text: data.value_text
      };
      _editate_DOM["71"] = DOM;

      if (data.value_text) {
        $(DOM).find('.items-item').html("<div class=\"img-item sign\">\n\n                    <img class=\"image\" src=\"".concat(data.value_text, "\" alt=\"\">\n\n                    <div class=\"img-item-remove\"><i class=\"remove-img fa fa-trash-o\"></i></div>\n\n                </div>"));
        $(DOM).find('.img-item-remove').on('click', function (e) {
          e.stopPropagation();
          $(this).parents('.items-item').html(placeholder);
          _editate_DOM["71"].__data.value = _editate_DOM["71"].__data.value_text = '';
        });
      }

      var SignDom = Render.create("<div id=\"signDialogBox\" class=\"sign-dialog-box\">\n\n                <div class=\"sign-tools-box\">\n\n                    <div class=\"sign-tools\">\n\n                        <div class=\"sign-tips\">\u8BF7\u5728\u4E0B\u65B9\u7A7A\u767D\u533A\u57DF\u7B7E\u5B57</div>\n\n                        <div class=\"sign-tools-items\">\n\n                            <span id=\"signCancelBtn\" class=\"sign-tools-item sign-cancel\">\u53D6\u6D88</span>\n\n                            <span id=\"signClearBtn\" class=\"sign-tools-item sign-clear\">\u6E05\u9664</span>\n\n                            <span id=\"signConfirmBtn\" class=\"sign-tools-item sign-confirm\">\u786E\u5B9A</span>\n\n                        </div>\n\n                    </div>\n\n                </div>\n\n                <div id=\"my_tablet\" class=\"sign-dialog\"></div>\n\n            </div>"); //记录下当前页面滚动位置

      function pageScrollTop(pointY) {
        if (document.scrollingElement) {
          if (!pointY && pointY !== 0) {
            return document.scrollingElement.scrollTop;
          }

          document.scrollingElement.scrollTop = pointY;
        }

        if (!pointY && pointY !== 0) {
          return Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
        }

        document.documentElement.scrollTop = pointY;
      }

      var htmlScrollTop = pageScrollTop();
      $('body').append(SignDom);
      var tablet = new Tablet("#my_tablet", {
        /* canvas画布是否响应式，默认为true。当设置为响应式后浏览器大小改变后会重新计算canvas画布的宽高，
          并且之前绘制的内容会被清除掉（canvas的一个特性）*/
        // response: true,
        // 签名板的额外class
        extraClass: "",
        //是否在画布上进行涂画
        hasDraw: false,
        // tablet初始化后执行的函数（此时canvas上下文并未初始化
        onInit: function onInit() {
          var that = this,
              container = this.container;
          that.rotate(90);
          that.setBackgroundColor('#fff');
          $('#signCancelBtn').on('click', function () {
            that.clear();
            $(SignDom).removeClass('on'); //将页面滚动恢复

            pageScrollTop(htmlScrollTop);
          });
          $('#signConfirmBtn').on('click', function () {
            if (!that.hasDraw) {
              Component.toast("手写签名内容不能为空！").render();
              $('.toast').css({
                'z-index': '10010',
                'transform': 'rotate(90deg)'
              });
              setTimeout(function () {
                $('.toast').css({
                  'z-index': '10',
                  'transform': 'rotate(0deg)'
                });
              }, 3000);
              return false;
            }

            var sizeBox = [178, 100];
            Api.uploadStudImage({
              base64: that.getBase64(),
              width: sizeBox[0],
              height: sizeBox[1]
            }, function (data) {
              var changeImgUrl = function changeImgUrl(url) {
                var imgName = url.split(".")[0];
                imgName = window.location.origin + "/esb/res/pic/" + Math.floor(+imgName / 10000) + "/" + Math.floor(+imgName / 100) + "/" + url;
                return imgName;
              };

              var src = changeImgUrl(data.data);
              _editate_DOM["71"].__data.value = _editate_DOM["71"].__data.value_text = src;
              $(SignDom).removeClass('on'); //将页面滚动恢复

              pageScrollTop(htmlScrollTop);
              that.clear();
              $(DOM).find('.items-item').html($("<div class=\"img-item sign\">\n\n                                <img class=\"image\" src=\"".concat(src, "\" alt=\"\">\n\n                                <div class=\"img-item-remove\"><i class=\"remove-img fa fa-trash-o\"></i></div>\n\n                            </div>")));
              $(DOM).find('.img-item-remove').on('click', function (e) {
                e.stopPropagation();
                $(this).parents('.items-item').html(placeholder);
                _editate_DOM["71"].__data.value = _editate_DOM["71"].__data.value_text = '';
              });
            });
          });
          $('#signClearBtn').on('click', function () {
            that.clear();
          });
        },
        //涂画中
        onMoveDraw: function onMoveDraw() {
          if (!this.hasDraw) {
            this.hasDraw = true;
          }
        },
        // 清除画布后执行的函数
        onClear: function onClear() {
          this.setBackgroundColor('#fff');
          this.hasDraw = false;
        }
      });
      $(DOM).find('.items-item').on('click', function () {
        if ($(this).find('img').length > 0) {
          return false;
        } //将页面先放置到顶部


        pageScrollTop(0);
        $(SignDom).addClass('on');
      });
      return {
        el: DOM
      };
    },
    /** 
       * 选择附件
       */
    _this.editate_8 = function (data) {
      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <div class=\"items-item\"></div>\n\n            </div>"));
      DOM.__data = {
        value: data.value,
        value_text: data.value_text
      };
      _editate_DOM["8"] = DOM;
      var PreviewDOM = Render.create("<div class=\"img-preview\" style=\"float:left\"></div>");
      var SelectDOM = Render.create("<div class=\"img-item img-uploader\">\n\n                    <i class=\"img-icon fa fa-file\"></i>\n\n                    <span>\u6DFB\u52A0\u6587\u4EF6</span>\n\n                </div>");

      try {
        DOM.__data.value = JSON.parse(DOM.__data.value);
      } catch (error) {
        DOM.__data.value = [];
      }

      var FileDOMs = DOM.__data.value.map(function (file) {
        return _this.editate_8_item(file, data, DOM).el;
      });

      $(PreviewDOM).append(FileDOMs);
      $(PreviewDOM).append(SelectDOM);
      $(SelectDOM).on("click", function () {
        var netDisk = new oaNetDiskMobile();
        location.hash = "netdisk";

        var windowNetdiskHashChangeEventCallback = function windowNetdiskHashChangeEventCallback() {
          if (location.hash.slice(1) != "netdisk") {
            var dom = document.getElementById("oaNetDisk");

            if (dom) {
              dom.parentNode.removeChild(dom);
            }

            window.removeEventListener("hashchange", windowNetdiskHashChangeEventCallback);
          }
        };

        window.addEventListener("hashchange", windowNetdiskHashChangeEventCallback);
        netDisk.select(function (files, done) {
          var FileDOMs = files.map(function (file) {
            // 包含file_id视为重复
            // 查看id
            // id相同 -> name相同视为重复
            // id不同 -> 可以添加
            if (DOM.__data.value.filter(function (item) {
              return item.file_id == file.file_id;
            }).length > 0) {
              // file_id 重复
              return false;
            }

            ;

            if (DOM.__data.value.filter(function (item) {
              return item.id == file.id && item.name == file.name;
            }).length > 0) {
              // id重复 name重复
              return false;
            }

            DOM.__data.value.push(file);

            return _this.editate_8_item(file, data, DOM).el;
          }); // $(PreviewDOM).append(FileDOMs);

          $(SelectDOM).before(FileDOMs);
          done();
        });
      });
      $(DOM).children(".items-item").append([PreviewDOM
      /* SelectDOM */
      ]);
      return {
        el: DOM
      };
    };

    _this.editate_8_item = function (file, data, DOM) {
      var FileDOM = Render.create("<div class=\"netdisk-item\" style=\"background-color:transparent\">\n\n                <img class=\"image\" src=\"".concat(file.icon, "\" alt=\"\">\n\n                <div class=\"netdisk-item-remove\"><i class=\"remove-img fa fa-trash-o\"></i></div>\n\n                <p>").concat(file.name, "</p>\n\n            </div>"));
      $(FileDOM).find(".netdisk-item-remove").on("click", function () {
        FileDOM.parentNode.removeChild(FileDOM);

        DOM.__data.value.splice(DOM.__data.value.indexOf(file), 1);
      });
      return {
        el: FileDOM
      };
    };
    /**
       * 选择教师
       */


    _this.editate_90 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return {
        el: _this.editateDrawer(data, "90")
      };
    };
    /**
       * 选择部门
       */


    _this.editate_91 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return {
        el: _this.editateDrawer(data, "91", 1)
      };
    };

    _this.editate_92 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return {
        el: _this.editateDrawer(data, "92", 3)
      };
    };

    _this.editate_93 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return {
        el: _this.editateDrawer(data, "93")
      };
    };
    /** 
       * 手机号
       */


    _this.editate_31 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"text\" class=\"items-item\" value=\"").concat(data.value, "\" maxlength=\"11\" placeholder=\"").concat(data.placeholder, "\">\n\n            </div>"), data);
    };
    /** 
       * 身份证号
       */


    _this.editate_32 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"text\" class=\"items-item\" value=\"").concat(data.value, "\" maxlength=\"18\" placeholder=\"").concat(data.placeholder, "\">\n\n            </div>"), data);
    };
    /** 
       * 温度输入框
       */


    _this.editate_33 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _this.normal("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <input type=\"number\" class=\"items-item\" value=\"").concat(data.value, "\" maxlength=\"\" placeholder=\"").concat(data.placeholder, "\">\n\n            </div>"), data);
    };
    /** 
       * 地址
       */


    _this.editate_51 = function () {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (Util.isEmpty(data.placeholder)) {
        data.placeholder = "请选择";
      }

      console.log(51, data);
      var DOM = Render.create("<div>\n\n                <div class=\"items-title\">".concat(data.title, "</div>\n\n                <div class=\"items-citychoose\"></div>\n\n            </div>"));
      var $citychoose = $(DOM).find(".items-citychoose");

      var createSelect = function createSelect(list) {
        var values = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
        var select = $("<select class=\"form-select items-item\" style=\"margin-top:1rem;\"><option value=\"\">\u8BF7\u9009\u62E9\u5730\u533A</option></select>");
        var selected = "";
        var cls = list.map(function (city) {
          var $option = $("<option>").val(city.code).text(city.cName);

          if (values[0] == city.code) {
            selected = city.code;
            $option.attr("selected", "selected");
          }

          $option[0].preData = city.sList || city.qList || [];
          return $option;
        });
        select.find("option").not(":first").empty();
        select.append(cls);
        $citychoose.append(select);

        var showNext = function showNext(value, defaults) {
          select.nextAll().remove();
          var list = select.find("option[value='" + value + "']")[0].preData;

          if (list.length > 0) {
            createSelect(list, defaults);
          } else {
            var $textarea = $("<textarea style=\"margin-top:1rem;width:100%;resize:none;height:8rem;padding:1rem;border-color:#ccc;\" placeholder=\"\u8BF7\u586B\u5199\u8BE6\u7EC6\u5730\u5740\">".concat(defaults[0] || "", "</textarea>"));
            $citychoose.append($textarea);
            setTimeout(function () {
              $textarea.focus();
            });
          }
        };

        select.on("change", function () {
          showNext(this.value, []);
        });

        if (selected) {
          showNext(selected, values.slice(1));
        }
      };

      var citys = Object.values(data.preData);
      var v = data.value.split(" ");
      v.push(data.value_text.split(" ").slice(v.length).join(" "));
      createSelect(citys, v);

      DOM.__data = function () {
        var value = [];
        var value_text = [];
        var sels = $citychoose.find("select");
        var text = $citychoose.find("textarea");

        if (sels.length == 1 && sels[0].value == "") {
          value.push("");
          value_text.push("");
        } else {
          for (var i = 0; i < sels.length; i++) {
            if (sels[i].value == "") {
              return "详细地区地址填写不完整！";
            }

            value.push(sels[i].value);
            value_text.push($(sels[i]).find("option[value='" + sels[i].value + "']")[0].innerHTML);
          }

          if (text.length == 0 || text.val() == "") {
            return "详细地区地址填写不完整！";
          } else {
            value_text.push(text[0].value);
          }
        }

        return {
          emptyTip: "详细地区地址填写不完整！",
          value: value.join(" "),
          value_text: value_text.join(" ")
        };
      }; // let $Items_item = $(DOM).find(".items-item");
      // let keyMap = {"": data.placeholder};
      // Util.likeItemObjectMap(data.original.options, function (option, index) {
      //     $Items_item.append(`<option value="${index}" ${data.value == index ? "selected" : ""}>${option.text}</option>`);
      //     keyMap[index] = option.text;
      // });
      // DOM.__data = {
      //     value: "",
      //     value_text: ""
      // };
      // $Items_item.on("change", function () {
      //     DOM.__data.value = this.value;
      //     DOM.__data.value_text = keyMap[this.value];
      // });


      return {
        el: DOM
      };
    };

    return new Editate();
  }();
  /* global $:true */


  +function ($) {
    "use strict"; //全局配置

    var defaults = {
      autoInit: false,
      //自动初始化页面
      showPageLoadingIndicator: true,
      //push.js加载页面的时候显示一个加载提示
      router: true,
      //默认使用router
      swipePanel: "left",
      //滑动打开侧栏
      swipePanelOnlyClose: false,
      //只允许滑动关闭，不允许滑动打开侧栏
      pushAnimationDuration: 400 //不要动这个，这是解决安卓 animationEnd 事件无法触发的bug

    };
    $.smConfig = $.extend(defaults, $.config);
  }($);
  /*===========================
  Device/OS Detection
  ===========================*/

  /* global $:true */

  ;

  (function ($) {
    "use strict";

    var device = {};
    var ua = navigator.userAgent;
    var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
    var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
    var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
    device.ios = device.android = device.iphone = device.ipad = device.androidChrome = false; // Android

    if (android) {
      device.os = 'android';
      device.osVersion = android[2];
      device.android = true;
      device.androidChrome = ua.toLowerCase().indexOf('chrome') >= 0;
    }

    if (ipad || iphone || ipod) {
      device.os = 'ios';
      device.ios = true;
    } // iOS


    if (iphone && !ipod) {
      device.osVersion = iphone[2].replace(/_/g, '.');
      device.iphone = true;
    }

    if (ipad) {
      device.osVersion = ipad[2].replace(/_/g, '.');
      device.ipad = true;
    }

    if (ipod) {
      device.osVersion = ipod[3] ? ipod[3].replace(/_/g, '.') : null;
      device.iphone = true;
    } // iOS 8+ changed UA


    if (device.ios && device.osVersion && ua.indexOf('Version/') >= 0) {
      if (device.osVersion.split('.')[0] === '10') {
        device.osVersion = ua.toLowerCase().split('version/')[1].split(' ')[0];
      }
    } // Webview


    device.webView = (iphone || ipad || ipod) && ua.match(/.*AppleWebKit(?!.*Safari)/i); // Minimal UI

    if (device.os && device.os === 'ios') {
      var osVersionArr = device.osVersion.split('.');
      device.minimalUi = !device.webView && (ipod || iphone) && (osVersionArr[0] * 1 === 7 ? osVersionArr[1] * 1 >= 1 : osVersionArr[0] * 1 > 7) && $('meta[name="viewport"]').length > 0 && $('meta[name="viewport"]').attr('content').indexOf('minimal-ui') >= 0;
    } // Check for status bar and fullscreen app mode


    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    device.statusBar = false;

    if (device.webView && windowWidth * windowHeight === screen.width * screen.height) {
      device.statusBar = true;
    } else {
      device.statusBar = false;
    } // Classes


    var classNames = []; // Pixel Ratio

    device.pixelRatio = window.devicePixelRatio || 1;
    classNames.push('pixel-ratio-' + Math.floor(device.pixelRatio));

    if (device.pixelRatio >= 2) {
      classNames.push('retina');
    } // OS classes


    if (device.os) {
      classNames.push(device.os, device.os + '-' + device.osVersion.split('.')[0], device.os + '-' + device.osVersion.replace(/\./g, '-'));

      if (device.os === 'ios') {
        var major = parseInt(device.osVersion.split('.')[0], 10);

        for (var i = major - 1; i >= 6; i--) {
          classNames.push('ios-gt-' + i);
        }
      }
    } // Status bar classes


    if (device.statusBar) {
      classNames.push('with-statusbar-overlay');
    } else {
      $('html').removeClass('with-statusbar-overlay');
    } // Add html classes


    if (classNames.length > 0) $('html').addClass(classNames.join(' '));
    $.device = device;
  })($);
  /* global $:true */


  +function ($) {
    "use strict"; //比较一个字符串版本号
    //a > b === 1
    //a = b === 0
    //a < b === -1

    $.compareVersion = function (a, b) {
      var as = a.split('.');
      var bs = b.split('.');
      if (a === b) return 0;

      for (var i = 0; i < as.length; i++) {
        var x = parseInt(as[i]);
        if (!bs[i]) return 1;
        var y = parseInt(bs[i]);
        if (x < y) return -1;
        if (x > y) return 1;
      }

      return 1;
    };

    $.getTouchPosition = function (e) {
      e = e.originalEvent || e; //jquery wrap the originevent

      if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend') {
        return {
          x: e.targetTouches[0].pageX,
          y: e.targetTouches[0].pageY
        };
      } else {
        return {
          x: e.pageX,
          y: e.pageY
        };
      }
    };
  }($);

  (function ($) {
    "use strict";

    ['width', 'height'].forEach(function (dimension) {
      var Dimension = dimension.replace(/./, function (m) {
        return m[0].toUpperCase();
      });

      $.fn['outer' + Dimension] = function (margin) {
        var elem = this;

        if (elem) {
          var size = elem[dimension]();
          var sides = {
            'width': ['left', 'right'],
            'height': ['top', 'bottom']
          };
          sides[dimension].forEach(function (side) {
            if (margin) size += parseInt(elem.css('margin-' + side), 10);
          });
          return size;
        } else {
          return null;
        }
      };
    });

    $.noop = function () {}; //support


    $.support = function () {
      var support = {
        touch: !!('ontouchstart' in window || window.DocumentTouch && document instanceof window.DocumentTouch)
      };
      return support;
    }();

    $.touchEvents = {
      start: $.support.touch ? 'touchstart' : 'mousedown',
      move: $.support.touch ? 'touchmove' : 'mousemove',
      end: $.support.touch ? 'touchend' : 'mouseup'
    };

    $.getTranslate = function (el, axis) {
      var matrix, curTransform, curStyle, transformMatrix; // automatic axis detection

      if (typeof axis === 'undefined') {
        axis = 'x';
      }

      curStyle = window.getComputedStyle(el, null);

      if (window.WebKitCSSMatrix) {
        // Some old versions of Webkit choke when 'none' is passed; pass
        // empty string instead in this case
        transformMatrix = new WebKitCSSMatrix(curStyle.webkitTransform === 'none' ? '' : curStyle.webkitTransform);
      } else {
        transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform || curStyle.transform || curStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,');
        matrix = transformMatrix.toString().split(',');
      }

      if (axis === 'x') {
        //Latest Chrome and webkits Fix
        if (window.WebKitCSSMatrix) curTransform = transformMatrix.m41; //Crazy IE10 Matrix
        else if (matrix.length === 16) curTransform = parseFloat(matrix[12]); //Normal Browsers
          else curTransform = parseFloat(matrix[4]);
      }

      if (axis === 'y') {
        //Latest Chrome and webkits Fix
        if (window.WebKitCSSMatrix) curTransform = transformMatrix.m42; //Crazy IE10 Matrix
        else if (matrix.length === 16) curTransform = parseFloat(matrix[13]); //Normal Browsers
          else curTransform = parseFloat(matrix[5]);
      }

      return curTransform || 0;
    };

    $.requestAnimationFrame = function (callback) {
      if (window.requestAnimationFrame) return window.requestAnimationFrame(callback);else if (window.webkitRequestAnimationFrame) return window.webkitRequestAnimationFrame(callback);else if (window.mozRequestAnimationFrame) return window.mozRequestAnimationFrame(callback);else {
        return window.setTimeout(callback, 1000 / 60);
      }
    };

    $.cancelAnimationFrame = function (id) {
      if (window.cancelAnimationFrame) return window.cancelAnimationFrame(id);else if (window.webkitCancelAnimationFrame) return window.webkitCancelAnimationFrame(id);else if (window.mozCancelAnimationFrame) return window.mozCancelAnimationFrame(id);else {
        return window.clearTimeout(id);
      }
    };

    $.fn.transitionEnd = function (callback) {
      var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
          i,
          dom = this;

      function fireCallBack(e) {
        /*jshint validthis:true */
        if (e.target !== this) return;
        callback.call(this, e);

        for (i = 0; i < events.length; i++) {
          dom.off(events[i], fireCallBack);
        }
      }

      if (callback) {
        for (i = 0; i < events.length; i++) {
          dom.on(events[i], fireCallBack);
        }
      }

      return this;
    };

    $.fn.dataset = function () {
      var el = this[0];

      if (el) {
        var dataset = {};

        if (el.dataset) {
          for (var dataKey in el.dataset) {
            // jshint ignore:line
            dataset[dataKey] = el.dataset[dataKey];
          }
        } else {
          for (var i = 0; i < el.attributes.length; i++) {
            var attr = el.attributes[i];

            if (attr.name.indexOf('data-') >= 0) {
              dataset[$.toCamelCase(attr.name.split('data-')[1])] = attr.value;
            }
          }
        }

        for (var key in dataset) {
          if (dataset[key] === 'false') dataset[key] = false;else if (dataset[key] === 'true') dataset[key] = true;else if (parseFloat(dataset[key]) === dataset[key] * 1) dataset[key] = dataset[key] * 1;
        }

        return dataset;
      } else return undefined;
    };

    $.fn.data = function (key, value) {
      if (typeof value === 'undefined') {
        // Get value
        if (this[0] && this[0].getAttribute) {
          var dataKey = this[0].getAttribute('data-' + key);

          if (dataKey) {
            return dataKey;
          } else if (this[0].smElementDataStorage && key in this[0].smElementDataStorage) {
            return this[0].smElementDataStorage[key];
          } else {
            return undefined;
          }
        } else return undefined;
      } else {
        // Set value
        for (var i = 0; i < this.length; i++) {
          var el = this[i];
          if (!el.smElementDataStorage) el.smElementDataStorage = {};
          el.smElementDataStorage[key] = value;
        }

        return this;
      }
    };

    $.fn.animationEnd = function (callback) {
      var events = ['webkitAnimationEnd', 'OAnimationEnd', 'MSAnimationEnd', 'animationend'],
          i,
          dom = this;

      function fireCallBack(e) {
        callback(e);

        for (i = 0; i < events.length; i++) {
          dom.off(events[i], fireCallBack);
        }
      }

      if (callback) {
        for (i = 0; i < events.length; i++) {
          dom.on(events[i], fireCallBack);
        }
      }

      return this;
    };

    $.fn.transition = function (duration) {
      if (typeof duration !== 'string') {
        duration = duration + 'ms';
      }

      for (var i = 0; i < this.length; i++) {
        var elStyle = this[i].style;
        elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
      }

      return this;
    };

    $.fn.transform = function (transform) {
      for (var i = 0; i < this.length; i++) {
        var elStyle = this[i].style;
        elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
      }

      return this;
    };

    $.fn.prevAll = function (selector) {
      var prevEls = [];
      var el = this[0];
      if (!el) return $([]);

      while (el.previousElementSibling) {
        var prev = el.previousElementSibling;

        if (selector) {
          if ($(prev).is(selector)) prevEls.push(prev);
        } else prevEls.push(prev);

        el = prev;
      }

      return $(prevEls);
    };

    $.fn.nextAll = function (selector) {
      var nextEls = [];
      var el = this[0];
      if (!el) return $([]);

      while (el.nextElementSibling) {
        var next = el.nextElementSibling;

        if (selector) {
          if ($(next).is(selector)) nextEls.push(next);
        } else nextEls.push(next);

        el = next;
      }

      return $(nextEls);
    }; //重置zepto的show方法，防止有些人引用的版本中 show 方法操作 opacity 属性影响动画执行


    $.fn.show = function () {
      var elementDisplay = {};

      function defaultDisplay(nodeName) {
        var element, display;

        if (!elementDisplay[nodeName]) {
          element = document.createElement(nodeName);
          document.body.appendChild(element);
          display = getComputedStyle(element, '').getPropertyValue("display");
          element.parentNode.removeChild(element);
          display === "none" && (display = "block");
          elementDisplay[nodeName] = display;
        }

        return elementDisplay[nodeName];
      }

      return this.each(function () {
        this.style.display === "none" && (this.style.display = '');
        if (getComputedStyle(this, '').getPropertyValue("display") === "none") ;
        this.style.display = defaultDisplay(this.nodeName);
      });
    };

    $.fn.scrollHeight = function () {
      return this[0].scrollHeight;
    };
  })($);
  /*======================================================
  ************   Modals   ************
  ======================================================*/

  /*jshint unused: false*/

  /* global $:true */


  +function ($) {
    "use strict";

    $.modalStack = [];

    $.pickerModal = function (pickerModal, removeOnClose) {
      if (typeof removeOnClose === 'undefined') removeOnClose = true;

      if (typeof pickerModal === 'string' && pickerModal.indexOf('<') >= 0) {
        pickerModal = $(pickerModal);

        if (pickerModal.length > 0) {
          if (removeOnClose) pickerModal.addClass('remove-on-close');
          $(defaults.modalContainer).append(pickerModal[0]);
        } else return false; //nothing found

      }

      pickerModal = $(pickerModal);
      if (pickerModal.length === 0) return false;
      pickerModal.show();
      $.openModal(pickerModal);
      return pickerModal[0];
    };

    $.openModal = function (modal) {
      if (defaults.closePrevious) $.closeModal();
      modal = $(modal);
      var isModal = modal.hasClass('modal');

      if ($('.modal.modal-in:not(.modal-out)').length && defaults.modalStack && isModal) {
        $.modalStack.push(function () {
          $.openModal(modal);
        });
        return;
      }

      var isPopover = modal.hasClass('popover');
      var isPopup = modal.hasClass('popup');
      var isLoginScreen = modal.hasClass('login-screen');
      var isPickerModal = modal.hasClass('picker-modal');
      var isToast = modal.hasClass('toast');

      if (isModal) {
        modal.show();
        modal.css({
          marginTop: -Math.round(modal.outerHeight() / 2) + 'px'
        });
      }

      if (isToast) {
        modal.show();
        modal.css({
          marginLeft: -Math.round(parseInt(window.getComputedStyle(modal[0]).width) / 2) + 'px' //

        });
      }

      var overlay;

      if (!isLoginScreen && !isPickerModal && !isToast) {
        if ($('.modal-overlay').length === 0 && !isPopup) {
          $(defaults.modalContainer).append('<div class="modal-overlay"></div>');
        }

        if ($('.popup-overlay').length === 0 && isPopup) {
          $(defaults.modalContainer).append('<div class="popup-overlay"></div>');
        }

        overlay = isPopup ? $('.popup-overlay') : $('.modal-overlay');
      } // Trugger open event


      modal.trigger('open'); // Picker modal body class

      if (isPickerModal) {
        $(defaults.modalContainer).addClass('with-picker-modal');
      } // Classes for transition in


      if (!isLoginScreen && !isPickerModal && !isToast) overlay.addClass('modal-overlay-visible');
      modal.removeClass('modal-out').addClass('modal-in').transitionEnd(function (e) {
        if (modal.hasClass('modal-out')) modal.trigger('closed');else modal.trigger('opened');
      });
      return true;
    };

    $.closeModal = function (modal) {
      modal = $(modal || '.modal-in');

      if (typeof modal !== 'undefined' && modal.length === 0) {
        return;
      }

      var isModal = modal.hasClass('modal');
      var isPopover = modal.hasClass('popover');
      var isPopup = modal.hasClass('popup');
      var isLoginScreen = modal.hasClass('login-screen');
      var isPickerModal = modal.hasClass('picker-modal');
      var removeOnClose = modal.hasClass('remove-on-close');
      var overlay = isPopup ? $('.popup-overlay') : $('.modal-overlay');

      if (isPopup) {
        if (modal.length === $('.popup.modal-in').length) {
          overlay.removeClass('modal-overlay-visible');
        }
      } else if (!isPickerModal) {
        overlay.removeClass('modal-overlay-visible');
      }

      modal.trigger('close'); // Picker modal body class

      if (isPickerModal) {
        $(defaults.modalContainer).removeClass('with-picker-modal');
        $(defaults.modalContainer).addClass('picker-modal-closing');
      }

      if (!isPopover) {
        modal.removeClass('modal-in').addClass('modal-out').transitionEnd(function (e) {
          if (modal.hasClass('modal-out')) modal.trigger('closed');else modal.trigger('opened');

          if (isPickerModal) {
            $(defaults.modalContainer).removeClass('picker-modal-closing');
          }

          if (isPopup || isLoginScreen || isPickerModal) {
            modal.removeClass('modal-out').hide();

            if (removeOnClose && modal.length > 0) {
              modal.remove();
            }
          } else {
            modal.remove();
          }
        });

        if (isModal && defaults.modalStack) {
          $.modalStackClearQueue();
        }
      } else {
        modal.removeClass('modal-in modal-out').trigger('closed').hide();

        if (removeOnClose) {
          modal.remove();
        }
      }

      return true;
    };

    var defaults = {
      modalButtonOk: 'OK',
      modalButtonCancel: 'Cancel',
      modalPreloaderTitle: 'Loading...',
      modalContainer: document.body,
      modalCloseByOutside: true,
      actionsCloseByOutside: false,
      popupCloseByOutside: true,
      closePrevious: true //close all previous modal before open

    };
  }($);
  /*======================================================
  ************   Picker   ************
  ======================================================*/

  /* global $:true */

  /* jshint unused:false */

  /* jshint multistr:true */

  +function ($) {
    "use strict";

    var Picker = function Picker(params) {
      var p = this;
      var defaults = {
        updateValuesOnMomentum: false,
        updateValuesOnTouchmove: true,
        rotateEffect: false,
        momentumRatio: 7,
        freeMode: false,
        // Common settings
        scrollToInput: true,
        inputReadOnly: true,
        convertToPopover: true,
        onlyInPopover: false,
        toolbar: true,
        toolbarCloseText: '确定',
        toolbarTemplate: '<header class="bar bar-nav">\
                    <button class="button button-link pull-right close-picker">确定</button>\
                    <h1 class="title"></h1>\
                    </header>'
      };
      params = params || {};

      for (var def in defaults) {
        if (typeof params[def] === 'undefined') {
          params[def] = defaults[def];
        }
      }

      p.params = params;
      p.cols = [];
      p.initialized = false; // Inline flag

      p.inline = p.params.container ? true : false; // 3D Transforms origin bug, only on safari

      var originBug = $.device.ios || navigator.userAgent.toLowerCase().indexOf('safari') >= 0 && navigator.userAgent.toLowerCase().indexOf('chrome') < 0 && !$.device.android; // Should be converted to popover

      function isPopover() {
        var toPopover = false;
        if (!p.params.convertToPopover && !p.params.onlyInPopover) return toPopover;

        if (!p.inline && p.params.input) {
          if (p.params.onlyInPopover) toPopover = true;else {
            if ($.device.ios) {
              toPopover = $.device.ipad ? true : false;
            } else {
              if ($(window).width() >= 768) toPopover = true;
            }
          }
        }

        return toPopover;
      }

      function inPopover() {
        if (p.opened && p.container && p.container.length > 0 && p.container.parents('.popover').length > 0) return true;else return false;
      } // Value


      p.setValue = function (arrValues, transition) {
        var valueIndex = 0;

        for (var i = 0; i < p.cols.length; i++) {
          if (p.cols[i] && !p.cols[i].divider) {
            p.cols[i].setValue(arrValues[valueIndex], transition);
            valueIndex++;
          }
        }
      };

      p.updateValue = function () {
        var newValue = [];
        var newDisplayValue = [];

        for (var i = 0; i < p.cols.length; i++) {
          if (!p.cols[i].divider) {
            newValue.push(p.cols[i].value);
            newDisplayValue.push(p.cols[i].displayValue);
          }
        }

        if (newValue.indexOf(undefined) >= 0) {
          return;
        }

        p.value = newValue;
        p.displayValue = newDisplayValue;

        if (p.params.onChange) {
          p.params.onChange(p, p.value, p.displayValue);
        }

        if (p.input && p.input.length > 0) {
          $(p.input).val(p.params.formatValue ? p.params.formatValue(p, p.value, p.displayValue) : p.value.join(' '));
          $(p.input).trigger('change');
        }
      }; // Columns Handlers


      p.initPickerCol = function (colElement, updateItems) {
        var colContainer = $(colElement);
        var colIndex = colContainer.index();
        var col = p.cols[colIndex];
        if (col.divider) return;
        col.container = colContainer;
        col.wrapper = col.container.find('.picker-items-col-wrapper');
        col.items = col.wrapper.find('.picker-item');
        var i, j;
        var wrapperHeight, itemHeight, itemsHeight, minTranslate, maxTranslate;

        col.replaceValues = function (values, displayValues) {
          col.destroyEvents();
          col.values = values;
          col.displayValues = displayValues;
          var newItemsHTML = p.columnHTML(col, true);
          col.wrapper.html(newItemsHTML);
          col.items = col.wrapper.find('.picker-item');
          col.calcSize();
          col.setValue(col.values[0], 0, true);
          col.initEvents();
        };

        col.calcSize = function () {
          if (p.params.rotateEffect) {
            col.container.removeClass('picker-items-col-absolute');
            if (!col.width) col.container.css({
              width: ''
            });
          }

          var colWidth, colHeight;
          colWidth = 0;
          colHeight = col.container[0].offsetHeight;
          wrapperHeight = col.wrapper[0].offsetHeight;
          itemHeight = col.items[0].offsetHeight;
          itemsHeight = itemHeight * col.items.length;
          minTranslate = colHeight / 2 - itemsHeight + itemHeight / 2;
          maxTranslate = colHeight / 2 - itemHeight / 2;

          if (col.width) {
            colWidth = col.width;
            if (parseInt(colWidth, 10) === colWidth) colWidth = colWidth + 'px';
            col.container.css({
              width: colWidth
            });
          }

          if (p.params.rotateEffect) {
            if (!col.width) {
              col.items.each(function () {
                var item = $(this);
                item.css({
                  width: 'auto'
                });
                colWidth = Math.max(colWidth, item[0].offsetWidth);
                item.css({
                  width: ''
                });
              });
              col.container.css({
                width: colWidth + 2 + 'px'
              });
            }

            col.container.addClass('picker-items-col-absolute');
          }
        };

        col.calcSize();
        col.wrapper.transform('translate3d(0,' + maxTranslate + 'px,0)').transition(0);
        var activeIndex = 0;
        var animationFrameId; // Set Value Function

        col.setValue = function (newValue, transition, valueCallbacks) {
          if (typeof transition === 'undefined') transition = '';
          var newActiveIndex = col.wrapper.find('.picker-item[data-picker-value="' + newValue + '"]').index();

          if (typeof newActiveIndex === 'undefined' || newActiveIndex === -1) {
            return;
          }

          var newTranslate = -newActiveIndex * itemHeight + maxTranslate; // Update wrapper

          col.wrapper.transition(transition);
          col.wrapper.transform('translate3d(0,' + newTranslate + 'px,0)'); // Watch items

          if (p.params.updateValuesOnMomentum && col.activeIndex && col.activeIndex !== newActiveIndex) {
            $.cancelAnimationFrame(animationFrameId);
            col.wrapper.transitionEnd(function () {
              $.cancelAnimationFrame(animationFrameId);
            });
            updateDuringScroll();
          } // Update items


          col.updateItems(newActiveIndex, newTranslate, transition, valueCallbacks);
        };

        col.updateItems = function (activeIndex, translate, transition, valueCallbacks) {
          if (typeof translate === 'undefined') {
            translate = $.getTranslate(col.wrapper[0], 'y');
          }

          if (typeof activeIndex === 'undefined') activeIndex = -Math.round((translate - maxTranslate) / itemHeight);
          if (activeIndex < 0) activeIndex = 0;
          if (activeIndex >= col.items.length) activeIndex = col.items.length - 1;
          var previousActiveIndex = col.activeIndex;
          col.activeIndex = activeIndex;
          /*
          col.wrapper.find('.picker-selected, .picker-after-selected, .picker-before-selected').removeClass('picker-selected picker-after-selected picker-before-selected');
                col.items.transition(transition);
          var selectedItem = col.items.eq(activeIndex).addClass('picker-selected').transform('');
          var prevItems = selectedItem.prevAll().addClass('picker-before-selected');
          var nextItems = selectedItem.nextAll().addClass('picker-after-selected');
          */
          //去掉 .picker-after-selected, .picker-before-selected 以提高性能

          col.wrapper.find('.picker-selected').removeClass('picker-selected');

          if (p.params.rotateEffect) {
            col.items.transition(transition);
          }

          var selectedItem = col.items.eq(activeIndex).addClass('picker-selected').transform('');

          if (valueCallbacks || typeof valueCallbacks === 'undefined') {
            // Update values
            col.value = selectedItem.attr('data-picker-value');
            col.displayValue = col.displayValues ? col.displayValues[activeIndex] : col.value; // On change callback

            if (previousActiveIndex !== activeIndex) {
              if (col.onChange) {
                col.onChange(p, col.value, col.displayValue);
              }

              p.updateValue();
            }
          } // Set 3D rotate effect


          if (!p.params.rotateEffect) {
            return;
          }

          var percentage = (translate - (Math.floor((translate - maxTranslate) / itemHeight) * itemHeight + maxTranslate)) / itemHeight;
          col.items.each(function () {
            var item = $(this);
            var itemOffsetTop = item.index() * itemHeight;
            var translateOffset = maxTranslate - translate;
            var itemOffset = itemOffsetTop - translateOffset;
            var percentage = itemOffset / itemHeight;
            var itemsFit = Math.ceil(col.height / itemHeight / 2) + 1;
            var angle = -18 * percentage;
            if (angle > 180) angle = 180;
            if (angle < -180) angle = -180; // Far class

            if (Math.abs(percentage) > itemsFit) item.addClass('picker-item-far');else item.removeClass('picker-item-far'); // Set transform

            item.transform('translate3d(0, ' + (-translate + maxTranslate) + 'px, ' + (originBug ? -110 : 0) + 'px) rotateX(' + angle + 'deg)');
          });
        };

        function updateDuringScroll() {
          animationFrameId = $.requestAnimationFrame(function () {
            col.updateItems(undefined, undefined, 0);
            updateDuringScroll();
          });
        } // Update items on init


        if (updateItems) col.updateItems(0, maxTranslate, 0);
        var allowItemClick = true;
        var isTouched, isMoved, touchStartY, touchCurrentY, touchStartTime, touchEndTime, startTranslate, returnTo, currentTranslate, prevTranslate, velocityTranslate, velocityTime;

        function handleTouchStart(e) {
          if (isMoved || isTouched) return;
          e.preventDefault();
          isTouched = true;
          var position = $.getTouchPosition(e);
          touchStartY = touchCurrentY = position.y;
          touchStartTime = new Date().getTime();
          allowItemClick = true;
          startTranslate = currentTranslate = $.getTranslate(col.wrapper[0], 'y');
        }

        function handleTouchMove(e) {
          if (!isTouched) return;
          e.preventDefault();
          allowItemClick = false;
          var position = $.getTouchPosition(e);
          touchCurrentY = position.y;

          if (!isMoved) {
            // First move
            $.cancelAnimationFrame(animationFrameId);
            isMoved = true;
            startTranslate = currentTranslate = $.getTranslate(col.wrapper[0], 'y');
            col.wrapper.transition(0);
          }

          e.preventDefault();
          var diff = touchCurrentY - touchStartY;
          currentTranslate = startTranslate + diff;
          returnTo = undefined; // Normalize translate

          if (currentTranslate < minTranslate) {
            currentTranslate = minTranslate - Math.pow(minTranslate - currentTranslate, 0.8);
            returnTo = 'min';
          }

          if (currentTranslate > maxTranslate) {
            currentTranslate = maxTranslate + Math.pow(currentTranslate - maxTranslate, 0.8);
            returnTo = 'max';
          } // Transform wrapper


          col.wrapper.transform('translate3d(0,' + currentTranslate + 'px,0)'); // Update items

          col.updateItems(undefined, currentTranslate, 0, p.params.updateValuesOnTouchmove); // Calc velocity

          velocityTranslate = currentTranslate - prevTranslate || currentTranslate;
          velocityTime = new Date().getTime();
          prevTranslate = currentTranslate;
        }

        function handleTouchEnd(e) {
          if (!isTouched || !isMoved) {
            isTouched = isMoved = false;
            return;
          }

          isTouched = isMoved = false;
          col.wrapper.transition('');

          if (returnTo) {
            if (returnTo === 'min') {
              col.wrapper.transform('translate3d(0,' + minTranslate + 'px,0)');
            } else col.wrapper.transform('translate3d(0,' + maxTranslate + 'px,0)');
          }

          touchEndTime = new Date().getTime();
          var velocity, newTranslate;

          if (touchEndTime - touchStartTime > 300) {
            newTranslate = currentTranslate;
          } else {
            velocity = Math.abs(velocityTranslate / (touchEndTime - velocityTime));
            newTranslate = currentTranslate + velocityTranslate * p.params.momentumRatio;
          }

          newTranslate = Math.max(Math.min(newTranslate, maxTranslate), minTranslate); // Active Index

          var activeIndex = -Math.floor((newTranslate - maxTranslate) / itemHeight); // Normalize translate

          if (!p.params.freeMode) newTranslate = -activeIndex * itemHeight + maxTranslate; // Transform wrapper

          col.wrapper.transform('translate3d(0,' + parseInt(newTranslate, 10) + 'px,0)'); // Update items

          col.updateItems(activeIndex, newTranslate, '', true); // Watch items

          if (p.params.updateValuesOnMomentum) {
            updateDuringScroll();
            col.wrapper.transitionEnd(function () {
              $.cancelAnimationFrame(animationFrameId);
            });
          } // Allow click


          setTimeout(function () {
            allowItemClick = true;
          }, 100);
        }

        function handleClick(e) {
          if (!allowItemClick) return;
          $.cancelAnimationFrame(animationFrameId);
          /*jshint validthis:true */

          var value = $(this).attr('data-picker-value');
          col.setValue(value);
        }

        col.initEvents = function (detach) {
          var method = detach ? 'off' : 'on';
          col.container[method]($.touchEvents.start, handleTouchStart);
          col.container[method]($.touchEvents.move, handleTouchMove);
          col.container[method]($.touchEvents.end, handleTouchEnd);
          col.items[method]('click', handleClick);
        };

        col.destroyEvents = function () {
          col.initEvents(true);
        };

        col.container[0].f7DestroyPickerCol = function () {
          col.destroyEvents();
        };

        col.initEvents();
      };

      p.destroyPickerCol = function (colContainer) {
        colContainer = $(colContainer);
        if ('f7DestroyPickerCol' in colContainer[0]) colContainer[0].f7DestroyPickerCol();
      }; // Resize cols


      function resizeCols() {
        if (!p.opened) return;

        for (var i = 0; i < p.cols.length; i++) {
          if (!p.cols[i].divider) {
            p.cols[i].calcSize();
            p.cols[i].setValue(p.cols[i].value, 0, false);
          }
        }
      }

      $(window).on('resize', resizeCols); // HTML Layout

      p.columnHTML = function (col, onlyItems) {
        var columnItemsHTML = '';
        var columnHTML = '';

        if (col.divider) {
          columnHTML += '<div class="picker-items-col picker-items-col-divider ' + (col.textAlign ? 'picker-items-col-' + col.textAlign : '') + ' ' + (col.cssClass || '') + '">' + col.content + '</div>';
        } else {
          for (var j = 0; j < col.values.length; j++) {
            columnItemsHTML += '<div class="picker-item" data-picker-value="' + col.values[j] + '">' + (col.displayValues ? col.displayValues[j] : col.values[j]) + '</div>';
          }

          columnHTML += '<div class="picker-items-col ' + (col.textAlign ? 'picker-items-col-' + col.textAlign : '') + ' ' + (col.cssClass || '') + '"><div class="picker-items-col-wrapper">' + columnItemsHTML + '</div></div>';
        }

        return onlyItems ? columnItemsHTML : columnHTML;
      };

      p.layout = function () {
        var pickerHTML = '';
        var pickerClass = '';
        var i;
        p.cols = [];
        var colsHTML = '';

        for (i = 0; i < p.params.cols.length; i++) {
          var col = p.params.cols[i];
          colsHTML += p.columnHTML(p.params.cols[i]);
          p.cols.push(col);
        }

        pickerClass = 'picker-modal picker-columns ' + (p.params.cssClass || '') + (p.params.rotateEffect ? ' picker-3d' : '');
        pickerHTML = '<div class="' + pickerClass + '">' + (p.params.toolbar ? p.params.toolbarTemplate.replace(/{{closeText}}/g, p.params.toolbarCloseText) : '') + '<div class="picker-modal-inner picker-items">' + colsHTML + '<div class="picker-center-highlight"></div>' + '</div>' + '</div>';
        p.pickerHTML = pickerHTML;
      }; // Input Events


      function openOnInput(e) {
        e.preventDefault();
        if (p.opened) return;
        p.open();

        if (p.params.scrollToInput && !isPopover()) {
          var pageContent = p.input.parents('.content');
          if (pageContent.length === 0) return;
          var paddingTop = parseInt(pageContent.css('padding-top'), 10),
              paddingBottom = parseInt(pageContent.css('padding-bottom'), 10),
              pageHeight = pageContent[0].offsetHeight - paddingTop - p.container.height(),
              pageScrollHeight = pageContent[0].scrollHeight - paddingTop - p.container.height(),
              newPaddingBottom;
          var inputTop = p.input.offset().top - paddingTop + p.input[0].offsetHeight;

          if (inputTop > pageHeight) {
            var scrollTop = pageContent.scrollTop() + inputTop - pageHeight;

            if (scrollTop + pageHeight > pageScrollHeight) {
              newPaddingBottom = scrollTop + pageHeight - pageScrollHeight + paddingBottom;

              if (pageHeight === pageScrollHeight) {
                newPaddingBottom = p.container.height();
              }

              pageContent.css({
                'padding-bottom': newPaddingBottom + 'px'
              });
            }

            pageContent.scrollTop(scrollTop, 300);
          }
        }
      }

      function closeOnHTMLClick(e) {
        if (inPopover()) return;

        if (p.input && p.input.length > 0) {
          if (e.target !== p.input[0] && $(e.target).parents('.picker-modal').length === 0) p.close();
        } else {
          if ($(e.target).parents('.picker-modal').length === 0) p.close();
        }
      }

      if (p.params.input) {
        p.input = $(p.params.input);

        if (p.input.length > 0) {
          if (p.params.inputReadOnly) p.input.prop('readOnly', true);

          if (!p.inline) {
            p.input.on("click", function (e) {
              openOnInput(e); //修复部分安卓系统下，即使设置了readonly依然会弹出系统键盘的bug

              if (p.params.inputReadOnly) {
                this.focus();
                this.blur();
              }
            });
          }

          if (p.params.inputReadOnly) {
            p.input.on('focus mousedown', function (e) {
              e.preventDefault();
            });
          }
        }
      }

      if (!p.inline) $('html').on('click', closeOnHTMLClick); // Open

      function onPickerClose() {
        p.opened = false;
        if (p.input && p.input.length > 0) p.input.parents('.page-content').css({
          'padding-bottom': ''
        });
        if (p.params.onClose) p.params.onClose(p); // Destroy events

        p.container.find('.picker-items-col').each(function () {
          p.destroyPickerCol(this);
        });
      }

      p.opened = false;

      p.open = function () {
        var toPopover = isPopover();

        if (!p.opened) {
          // Layout
          p.layout(); // Append

          if (toPopover) {
            p.pickerHTML = '<div class="popover popover-picker-columns"><div class="popover-inner">' + p.pickerHTML + '</div></div>';
            p.popover = $.popover(p.pickerHTML, p.params.input, true);
            p.container = $(p.popover).find('.picker-modal');
            $(p.popover).on('close', function () {
              onPickerClose();
            });
          } else if (p.inline) {
            p.container = $(p.pickerHTML);
            p.container.addClass('picker-modal-inline');
            $(p.params.container).append(p.container);
          } else {
            p.container = $($.pickerModal(p.pickerHTML));
            $(p.container).on('close', function () {
              onPickerClose();
            });
          } // Store picker instance


          p.container[0].f7Picker = p; // Init Events

          p.container.find('.picker-items-col').each(function () {
            var updateItems = true;
            if (!p.initialized && p.params.value || p.initialized && p.value) updateItems = false;
            p.initPickerCol(this, updateItems);
          }); // Set value

          if (!p.initialized) {
            if (p.params.value) {
              p.setValue(p.params.value, 0);
            }
          } else {
            if (p.value) p.setValue(p.value, 0);
          }
        } // Set flag


        p.opened = true;
        p.initialized = true;
        if (p.params.onOpen) p.params.onOpen(p);
      }; // Close


      p.close = function () {
        if (!p.opened || p.inline) return;

        if (inPopover()) {
          $.closeModal(p.popover);
          return;
        } else {
          $.closeModal(p.container);
          return;
        }
      }; // Destroy


      p.destroy = function () {
        p.close();

        if (p.params.input && p.input.length > 0) {
          p.input.off('click focus', openOnInput);
        }

        $('html').off('click', closeOnHTMLClick);
        $(window).off('resize', resizeCols);
      };

      if (p.inline) {
        p.open();
      }

      return p;
    };

    $(document).on("click", ".close-picker", function () {
      var pickerToClose = $('.picker-modal.modal-in');

      if (pickerToClose.length > 0) {
        $.closeModal(pickerToClose);
      } else {
        pickerToClose = $('.popover.modal-in .picker-modal');

        if (pickerToClose.length > 0) {
          $.closeModal(pickerToClose.parents('.popover'));
        }
      }
    }); //修复picker会滚动页面的bug

    $(document).on($.touchEvents.move, ".picker-modal-inner", function (e) {
      e.preventDefault();
    });

    $.fn.picker = function (params) {
      var args = arguments;
      return this.each(function () {
        if (!this) return;
        var $this = $(this);
        var picker = $this.data("picker");

        if (!picker) {
          params = params || {};
          var inputValue = $this.val();

          if (params.value === undefined && inputValue !== "") {
            params.value = params.cols.length > 1 ? inputValue.split(" ") : [inputValue];
          }

          var p = $.extend({
            input: this
          }, params);
          picker = new Picker(p);
          $this.data("picker", picker);
        }

        if (_typeof(params) === _typeof("a")) {
          picker[params].apply(picker, Array.prototype.slice.call(args, 1));
        }
      });
    };
  }($);
  /* global $:true */

  /* jshint unused:false*/

  +function ($) {
    "use strict";

    $.fn.datetimePicker = function (params) {
      return this.each(function () {
        if (!this) return;
        var today = new Date();

        var getDays = function getDays(max) {
          var days = [];

          for (var i = 1; i <= (max || 31); i++) {
            days.push(i < 10 ? "0" + i : i);
          }

          return days;
        };

        var getDaysByMonthAndYear = function getDaysByMonthAndYear(month, year) {
          var int_d = new Date(year, parseInt(month) + 1 - 1, 1);
          var d = new Date(int_d - 1);
          return getDays(d.getDate());
        };

        var formatNumber = function formatNumber(n) {
          return n < 10 ? "0" + n : n;
        };

        var initMonthes = '01 02 03 04 05 06 07 08 09 10 11 12'.split(' ');

        var initYears = function () {
          var arr = [];

          for (var i = 1950; i <= 2030; i++) {
            arr.push(i);
          }

          return arr;
        }();

        var defaults = {
          rotateEffect: false,
          //为了性能
          value: [today.getFullYear(), formatNumber(today.getMonth() + 1), today.getDate(), formatNumber(today.getHours()), formatNumber(today.getMinutes())],
          onChange: function onChange(picker, values, displayValues) {
            var days = getDaysByMonthAndYear(picker.cols[1].value, picker.cols[0].value);
            var currentValue = picker.cols[2].value;
            if (currentValue > days.length) currentValue = days.length;
            picker.cols[2].setValue(currentValue);
          },
          formatValue: function formatValue(p, values, displayValues) {
            return displayValues[0] + '-' + values[1] + '-' + values[2] + ' ' + values[3] + ':' + values[4];
          },
          cols: [// Years
          {
            values: initYears
          }, // Months
          {
            values: initMonthes
          }, // Days
          {
            values: getDays()
          }, // Space divider
          {
            divider: true,
            content: '  '
          }, // Hours
          {
            values: function () {
              var arr = [];

              for (var i = 0; i <= 23; i++) {
                arr.push(formatNumber(i));
              }

              return arr;
            }()
          }, // Divider
          {
            divider: true,
            content: ':'
          }, // Minutes
          {
            values: function () {
              var arr = [];

              for (var i = 0; i <= 59; i++) {
                arr.push(formatNumber(i));
              }

              return arr;
            }()
          }]
        };
        params = params || {};
        var inputValue = $(this).val();

        if (params.value === undefined && inputValue !== "") {
          params.value = [].concat(inputValue.split(" ")[0].split("-"), inputValue.split(" ")[1].split(":"));
        }

        var p = $.extend(defaults, params);
        $(this).picker(p);
      });
    };
    /*======================================================
    ************   Calendar   ************
    ======================================================*/

    /* global $:true */

    /*jshint unused: false*/


    +function ($) {
      "use strict";

      var rtl = false;
      var defaults;

      var Calendar = function Calendar(params) {
        var p = this;
        params = params || {};

        for (var def in defaults) {
          if (typeof params[def] === 'undefined') {
            params[def] = defaults[def];
          }
        }

        p.params = params;
        p.initialized = false; // Inline flag

        p.inline = p.params.container ? true : false; // Is horizontal

        p.isH = p.params.direction === 'horizontal'; // RTL inverter

        var inverter = p.isH ? rtl ? -1 : 1 : 1; // Animating flag

        p.animating = false; // Should be converted to popover

        function isPopover() {
          var toPopover = false;
          if (!p.params.convertToPopover && !p.params.onlyInPopover) return toPopover;

          if (!p.inline && p.params.input) {
            if (p.params.onlyInPopover) toPopover = true;else {
              if ($.device.ios) {
                toPopover = $.device.ipad ? true : false;
              } else {
                if ($(window).width() >= 768) toPopover = true;
              }
            }
          }

          return toPopover;
        }

        function inPopover() {
          if (p.opened && p.container && p.container.length > 0 && p.container.parents('.popover').length > 0) return true;else return false;
        } // Format date


        function formatDate(date) {
          date = new Date(date);
          var year = date.getFullYear();
          var month = date.getMonth();
          var month1 = month + 1;
          var day = date.getDate();
          var weekDay = date.getDay();
          return p.params.dateFormat.replace(/yyyy/g, year).replace(/yy/g, (year + '').substring(2)).replace(/mm/g, month1 < 10 ? '0' + month1 : month1).replace(/m/g, month1).replace(/MM/g, p.params.monthNames[month]).replace(/M/g, p.params.monthNamesShort[month]).replace(/dd/g, day < 10 ? '0' + day : day).replace(/d/g, day).replace(/DD/g, p.params.dayNames[weekDay]).replace(/D/g, p.params.dayNamesShort[weekDay]);
        } // Value


        p.addValue = function (value) {
          if (p.params.multiple) {
            if (!p.value) p.value = [];
            var inValuesIndex;

            for (var i = 0; i < p.value.length; i++) {
              if (new Date(value).getTime() === new Date(p.value[i]).getTime()) {
                inValuesIndex = i;
              }
            }

            if (typeof inValuesIndex === 'undefined') {
              p.value.push(value);
            } else {
              p.value.splice(inValuesIndex, 1);
            }

            p.updateValue();
          } else {
            p.value = [value];
            p.updateValue();
          }
        };

        p.setValue = function (arrValues) {
          p.value = arrValues;
          p.updateValue();
        };

        p.updateValue = function () {
          p.wrapper.find('.picker-calendar-day-selected').removeClass('picker-calendar-day-selected');
          var i, inputValue;

          for (i = 0; i < p.value.length; i++) {
            var valueDate = new Date(p.value[i]);
            p.wrapper.find('.picker-calendar-day[data-date="' + valueDate.getFullYear() + '-' + valueDate.getMonth() + '-' + valueDate.getDate() + '"]').addClass('picker-calendar-day-selected');
          }

          if (p.params.onChange) {
            p.params.onChange(p, p.value, p.value.map(formatDate));
          }

          if (p.input && p.input.length > 0) {
            if (p.params.formatValue) inputValue = p.params.formatValue(p, p.value);else {
              inputValue = [];

              for (i = 0; i < p.value.length; i++) {
                inputValue.push(formatDate(p.value[i]));
              }

              inputValue = inputValue.join(', ');
            }
            $(p.input).val(inputValue);
            $(p.input).trigger('change');
          }
        }; // Columns Handlers


        p.initCalendarEvents = function () {
          var col;
          var allowItemClick = true;
          var isTouched, isMoved, touchStartX, touchStartY, touchCurrentX, touchCurrentY, touchStartTime, touchEndTime, startTranslate, currentTranslate, wrapperWidth, wrapperHeight, percentage, touchesDiff, isScrolling;

          function handleTouchStart(e) {
            if (isMoved || isTouched) return; // e.preventDefault();

            isTouched = true;
            var position = $.getTouchPosition(e);
            touchStartX = touchCurrentY = position.x;
            touchStartY = touchCurrentY = position.y;
            touchStartTime = new Date().getTime();
            percentage = 0;
            allowItemClick = true;
            isScrolling = undefined;
            startTranslate = currentTranslate = p.monthsTranslate;
          }

          function handleTouchMove(e) {
            if (!isTouched) return;
            var position = $.getTouchPosition(e);
            touchCurrentX = position.x;
            touchCurrentY = position.y;

            if (typeof isScrolling === 'undefined') {
              isScrolling = !!(isScrolling || Math.abs(touchCurrentY - touchStartY) > Math.abs(touchCurrentX - touchStartX));
            }

            if (p.isH && isScrolling) {
              isTouched = false;
              return;
            }

            e.preventDefault();

            if (p.animating) {
              isTouched = false;
              return;
            }

            allowItemClick = false;

            if (!isMoved) {
              // First move
              isMoved = true;
              wrapperWidth = p.wrapper[0].offsetWidth;
              wrapperHeight = p.wrapper[0].offsetHeight;
              p.wrapper.transition(0);
            }

            e.preventDefault();
            touchesDiff = p.isH ? touchCurrentX - touchStartX : touchCurrentY - touchStartY;
            percentage = touchesDiff / (p.isH ? wrapperWidth : wrapperHeight);
            currentTranslate = (p.monthsTranslate * inverter + percentage) * 100; // Transform wrapper

            p.wrapper.transform('translate3d(' + (p.isH ? currentTranslate : 0) + '%, ' + (p.isH ? 0 : currentTranslate) + '%, 0)');
          }

          function handleTouchEnd(e) {
            if (!isTouched || !isMoved) {
              isTouched = isMoved = false;
              return;
            }

            isTouched = isMoved = false;
            touchEndTime = new Date().getTime();

            if (touchEndTime - touchStartTime < 300) {
              if (Math.abs(touchesDiff) < 10) {
                p.resetMonth();
              } else if (touchesDiff >= 10) {
                if (rtl) p.nextMonth();else p.prevMonth();
              } else {
                if (rtl) p.prevMonth();else p.nextMonth();
              }
            } else {
              if (percentage <= -0.5) {
                if (rtl) p.prevMonth();else p.nextMonth();
              } else if (percentage >= 0.5) {
                if (rtl) p.nextMonth();else p.prevMonth();
              } else {
                p.resetMonth();
              }
            } // Allow click


            setTimeout(function () {
              allowItemClick = true;
            }, 100);
          }

          function handleDayClick(e) {
            if (!allowItemClick) return;
            var day = $(e.target).parents('.picker-calendar-day');

            if (day.length === 0 && $(e.target).hasClass('picker-calendar-day')) {
              day = $(e.target);
            }

            if (day.length === 0) return;
            if (day.hasClass('picker-calendar-day-selected') && !p.params.multiple) return;
            if (day.hasClass('picker-calendar-day-disabled')) return;
            if (day.hasClass('picker-calendar-day-next')) p.nextMonth();
            if (day.hasClass('picker-calendar-day-prev')) p.prevMonth();
            var dateYear = day.attr('data-year');
            var dateMonth = day.attr('data-month');
            var dateDay = day.attr('data-day');

            if (p.params.onDayClick) {
              p.params.onDayClick(p, day[0], dateYear, dateMonth, dateDay);
            }

            p.addValue(new Date(dateYear, dateMonth, dateDay).getTime());
            if (p.params.closeOnSelect) p.close();
          }

          p.container.find('.picker-calendar-prev-month').on('click', p.prevMonth);
          p.container.find('.picker-calendar-next-month').on('click', p.nextMonth);
          p.container.find('.picker-calendar-prev-year').on('click', p.prevYear);
          p.container.find('.picker-calendar-next-year').on('click', p.nextYear);
          p.wrapper.on('click', handleDayClick);

          if (p.params.touchMove) {
            p.wrapper.on($.touchEvents.start, handleTouchStart);
            p.wrapper.on($.touchEvents.move, handleTouchMove);
            p.wrapper.on($.touchEvents.end, handleTouchEnd);
          }

          p.container[0].f7DestroyCalendarEvents = function () {
            p.container.find('.picker-calendar-prev-month').off('click', p.prevMonth);
            p.container.find('.picker-calendar-next-month').off('click', p.nextMonth);
            p.container.find('.picker-calendar-prev-year').off('click', p.prevYear);
            p.container.find('.picker-calendar-next-year').off('click', p.nextYear);
            p.wrapper.off('click', handleDayClick);

            if (p.params.touchMove) {
              p.wrapper.off($.touchEvents.start, handleTouchStart);
              p.wrapper.off($.touchEvents.move, handleTouchMove);
              p.wrapper.off($.touchEvents.end, handleTouchEnd);
            }
          };
        };

        p.destroyCalendarEvents = function (colContainer) {
          if ('f7DestroyCalendarEvents' in p.container[0]) p.container[0].f7DestroyCalendarEvents();
        }; // Calendar Methods


        p.daysInMonth = function (date) {
          var d = new Date(date);
          return new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate();
        };

        p.monthHTML = function (date, offset) {
          date = new Date(date);
          var year = date.getFullYear(),
              month = date.getMonth(),
              day = date.getDate();

          if (offset === 'next') {
            if (month === 11) date = new Date(year + 1, 0);else date = new Date(year, month + 1, 1);
          }

          if (offset === 'prev') {
            if (month === 0) date = new Date(year - 1, 11);else date = new Date(year, month - 1, 1);
          }

          if (offset === 'next' || offset === 'prev') {
            month = date.getMonth();
            year = date.getFullYear();
          }

          var daysInPrevMonth = p.daysInMonth(new Date(date.getFullYear(), date.getMonth()).getTime() - 10 * 24 * 60 * 60 * 1000),
              daysInMonth = p.daysInMonth(date),
              firstDayOfMonthIndex = new Date(date.getFullYear(), date.getMonth()).getDay();
          if (firstDayOfMonthIndex === 0) firstDayOfMonthIndex = 7;
          var dayDate,
              currentValues = [],
              i,
              j,
              rows = 6,
              cols = 7,
              monthHTML = '',
              dayIndex = 0 + (p.params.firstDay - 1),
              today = new Date().setHours(0, 0, 0, 0),
              minDate = p.params.minDate ? new Date(p.params.minDate).getTime() : null,
              maxDate = p.params.maxDate ? new Date(p.params.maxDate).getTime() : null;

          if (p.value && p.value.length) {
            for (i = 0; i < p.value.length; i++) {
              currentValues.push(new Date(p.value[i]).setHours(0, 0, 0, 0));
            }
          }

          for (i = 1; i <= rows; i++) {
            var rowHTML = '';
            var row = i;

            for (j = 1; j <= cols; j++) {
              var col = j;
              dayIndex++;
              var dayNumber = dayIndex - firstDayOfMonthIndex;
              var addClass = '';

              if (dayNumber < 0) {
                dayNumber = daysInPrevMonth + dayNumber + 1;
                addClass += ' picker-calendar-day-prev';
                dayDate = new Date(month - 1 < 0 ? year - 1 : year, month - 1 < 0 ? 11 : month - 1, dayNumber).getTime();
              } else {
                dayNumber = dayNumber + 1;

                if (dayNumber > daysInMonth) {
                  dayNumber = dayNumber - daysInMonth;
                  addClass += ' picker-calendar-day-next';
                  dayDate = new Date(month + 1 > 11 ? year + 1 : year, month + 1 > 11 ? 0 : month + 1, dayNumber).getTime();
                } else {
                  dayDate = new Date(year, month, dayNumber).getTime();
                }
              } // Today


              if (dayDate === today) addClass += ' picker-calendar-day-today'; // Selected

              if (currentValues.indexOf(dayDate) >= 0) addClass += ' picker-calendar-day-selected'; // Weekend

              if (p.params.weekendDays.indexOf(col - 1) >= 0) {
                addClass += ' picker-calendar-day-weekend';
              } // Disabled


              if (minDate && dayDate < minDate || maxDate && dayDate > maxDate) {
                addClass += ' picker-calendar-day-disabled';
              }

              dayDate = new Date(dayDate);
              var dayYear = dayDate.getFullYear();
              var dayMonth = dayDate.getMonth();
              rowHTML += '<div data-year="' + dayYear + '" data-month="' + dayMonth + '" data-day="' + dayNumber + '" class="picker-calendar-day' + addClass + '" data-date="' + (dayYear + '-' + dayMonth + '-' + dayNumber) + '"><span>' + dayNumber + '</span></div>';
            }

            monthHTML += '<div class="picker-calendar-row">' + rowHTML + '</div>';
          }

          monthHTML = '<div class="picker-calendar-month" data-year="' + year + '" data-month="' + month + '">' + monthHTML + '</div>';
          return monthHTML;
        };

        p.animating = false;

        p.updateCurrentMonthYear = function (dir) {
          if (typeof dir === 'undefined') {
            p.currentMonth = parseInt(p.months.eq(1).attr('data-month'), 10);
            p.currentYear = parseInt(p.months.eq(1).attr('data-year'), 10);
          } else {
            p.currentMonth = parseInt(p.months.eq(dir === 'next' ? p.months.length - 1 : 0).attr('data-month'), 10);
            p.currentYear = parseInt(p.months.eq(dir === 'next' ? p.months.length - 1 : 0).attr('data-year'), 10);
          }

          p.container.find('.current-month-value').text(p.params.monthNames[p.currentMonth]);
          p.container.find('.current-year-value').text(p.currentYear);
        };

        p.onMonthChangeStart = function (dir) {
          p.updateCurrentMonthYear(dir);
          p.months.removeClass('picker-calendar-month-current picker-calendar-month-prev picker-calendar-month-next');
          var currentIndex = dir === 'next' ? p.months.length - 1 : 0;
          p.months.eq(currentIndex).addClass('picker-calendar-month-current');
          p.months.eq(dir === 'next' ? currentIndex - 1 : currentIndex + 1).addClass(dir === 'next' ? 'picker-calendar-month-prev' : 'picker-calendar-month-next');

          if (p.params.onMonthYearChangeStart) {
            p.params.onMonthYearChangeStart(p, p.currentYear, p.currentMonth);
          }
        };

        p.onMonthChangeEnd = function (dir, rebuildBoth) {
          p.animating = false;
          var nextMonthHTML, prevMonthHTML, newMonthHTML;
          p.wrapper.find('.picker-calendar-month:not(.picker-calendar-month-prev):not(.picker-calendar-month-current):not(.picker-calendar-month-next)').remove();

          if (typeof dir === 'undefined') {
            dir = 'next';
            rebuildBoth = true;
          }

          if (!rebuildBoth) {
            newMonthHTML = p.monthHTML(new Date(p.currentYear, p.currentMonth), dir);
          } else {
            p.wrapper.find('.picker-calendar-month-next, .picker-calendar-month-prev').remove();
            prevMonthHTML = p.monthHTML(new Date(p.currentYear, p.currentMonth), 'prev');
            nextMonthHTML = p.monthHTML(new Date(p.currentYear, p.currentMonth), 'next');
          }

          if (dir === 'next' || rebuildBoth) {
            p.wrapper.append(newMonthHTML || nextMonthHTML);
          }

          if (dir === 'prev' || rebuildBoth) {
            p.wrapper.prepend(newMonthHTML || prevMonthHTML);
          }

          p.months = p.wrapper.find('.picker-calendar-month');
          p.setMonthsTranslate(p.monthsTranslate);

          if (p.params.onMonthAdd) {
            p.params.onMonthAdd(p, dir === 'next' ? p.months.eq(p.months.length - 1)[0] : p.months.eq(0)[0]);
          }

          if (p.params.onMonthYearChangeEnd) {
            p.params.onMonthYearChangeEnd(p, p.currentYear, p.currentMonth);
          }
        };

        p.setMonthsTranslate = function (translate) {
          translate = translate || p.monthsTranslate || 0;
          if (typeof p.monthsTranslate === 'undefined') p.monthsTranslate = translate;
          p.months.removeClass('picker-calendar-month-current picker-calendar-month-prev picker-calendar-month-next');
          var prevMonthTranslate = -(translate + 1) * 100 * inverter;
          var currentMonthTranslate = -translate * 100 * inverter;
          var nextMonthTranslate = -(translate - 1) * 100 * inverter;
          p.months.eq(0).transform('translate3d(' + (p.isH ? prevMonthTranslate : 0) + '%, ' + (p.isH ? 0 : prevMonthTranslate) + '%, 0)').addClass('picker-calendar-month-prev');
          p.months.eq(1).transform('translate3d(' + (p.isH ? currentMonthTranslate : 0) + '%, ' + (p.isH ? 0 : currentMonthTranslate) + '%, 0)').addClass('picker-calendar-month-current');
          p.months.eq(2).transform('translate3d(' + (p.isH ? nextMonthTranslate : 0) + '%, ' + (p.isH ? 0 : nextMonthTranslate) + '%, 0)').addClass('picker-calendar-month-next');
        };

        p.nextMonth = function (transition) {
          if (typeof transition === 'undefined' || _typeof(transition) === 'object') {
            transition = '';
            if (!p.params.animate) transition = 0;
          }

          var nextMonth = parseInt(p.months.eq(p.months.length - 1).attr('data-month'), 10);
          var nextYear = parseInt(p.months.eq(p.months.length - 1).attr('data-year'), 10);
          var nextDate = new Date(nextYear, nextMonth);
          var nextDateTime = nextDate.getTime();
          var transitionEndCallback = p.animating ? false : true;

          if (p.params.maxDate) {
            if (nextDateTime > new Date(p.params.maxDate).getTime()) {
              return p.resetMonth();
            }
          }

          p.monthsTranslate--;

          if (nextMonth === p.currentMonth) {
            var nextMonthTranslate = -p.monthsTranslate * 100 * inverter;
            var nextMonthHTML = $(p.monthHTML(nextDateTime, 'next')).transform('translate3d(' + (p.isH ? nextMonthTranslate : 0) + '%, ' + (p.isH ? 0 : nextMonthTranslate) + '%, 0)').addClass('picker-calendar-month-next');
            p.wrapper.append(nextMonthHTML[0]);
            p.months = p.wrapper.find('.picker-calendar-month');

            if (p.params.onMonthAdd) {
              p.params.onMonthAdd(p, p.months.eq(p.months.length - 1)[0]);
            }
          }

          p.animating = true;
          p.onMonthChangeStart('next');
          var translate = p.monthsTranslate * 100 * inverter;
          p.wrapper.transition(transition).transform('translate3d(' + (p.isH ? translate : 0) + '%, ' + (p.isH ? 0 : translate) + '%, 0)');

          if (transitionEndCallback) {
            p.wrapper.transitionEnd(function () {
              p.onMonthChangeEnd('next');
            });
          }

          if (!p.params.animate) {
            p.onMonthChangeEnd('next');
          }
        };

        p.prevMonth = function (transition) {
          if (typeof transition === 'undefined' || _typeof(transition) === 'object') {
            transition = '';
            if (!p.params.animate) transition = 0;
          }

          var prevMonth = parseInt(p.months.eq(0).attr('data-month'), 10);
          var prevYear = parseInt(p.months.eq(0).attr('data-year'), 10);
          var prevDate = new Date(prevYear, prevMonth + 1, -1);
          var prevDateTime = prevDate.getTime();
          var transitionEndCallback = p.animating ? false : true;

          if (p.params.minDate) {
            if (prevDateTime < new Date(p.params.minDate).getTime()) {
              return p.resetMonth();
            }
          }

          p.monthsTranslate++;

          if (prevMonth === p.currentMonth) {
            var prevMonthTranslate = -p.monthsTranslate * 100 * inverter;
            var prevMonthHTML = $(p.monthHTML(prevDateTime, 'prev')).transform('translate3d(' + (p.isH ? prevMonthTranslate : 0) + '%, ' + (p.isH ? 0 : prevMonthTranslate) + '%, 0)').addClass('picker-calendar-month-prev');
            p.wrapper.prepend(prevMonthHTML[0]);
            p.months = p.wrapper.find('.picker-calendar-month');

            if (p.params.onMonthAdd) {
              p.params.onMonthAdd(p, p.months.eq(0)[0]);
            }
          }

          p.animating = true;
          p.onMonthChangeStart('prev');
          var translate = p.monthsTranslate * 100 * inverter;
          p.wrapper.transition(transition).transform('translate3d(' + (p.isH ? translate : 0) + '%, ' + (p.isH ? 0 : translate) + '%, 0)');

          if (transitionEndCallback) {
            p.wrapper.transitionEnd(function () {
              p.onMonthChangeEnd('prev');
            });
          }

          if (!p.params.animate) {
            p.onMonthChangeEnd('prev');
          }
        };

        p.resetMonth = function (transition) {
          if (typeof transition === 'undefined') transition = '';
          var translate = p.monthsTranslate * 100 * inverter;
          p.wrapper.transition(transition).transform('translate3d(' + (p.isH ? translate : 0) + '%, ' + (p.isH ? 0 : translate) + '%, 0)');
        };

        p.setYearMonth = function (year, month, transition) {
          if (typeof year === 'undefined') year = p.currentYear;
          if (typeof month === 'undefined') month = p.currentMonth;

          if (typeof transition === 'undefined' || _typeof(transition) === 'object') {
            transition = '';
            if (!p.params.animate) transition = 0;
          }

          var targetDate;

          if (year < p.currentYear) {
            targetDate = new Date(year, month + 1, -1).getTime();
          } else {
            targetDate = new Date(year, month).getTime();
          }

          if (p.params.maxDate && targetDate > new Date(p.params.maxDate).getTime()) {
            return false;
          }

          if (p.params.minDate && targetDate < new Date(p.params.minDate).getTime()) {
            return false;
          }

          var currentDate = new Date(p.currentYear, p.currentMonth).getTime();
          var dir = targetDate > currentDate ? 'next' : 'prev';
          var newMonthHTML = p.monthHTML(new Date(year, month));
          p.monthsTranslate = p.monthsTranslate || 0;
          var prevTranslate = p.monthsTranslate;
          var monthTranslate, wrapperTranslate;
          var transitionEndCallback = p.animating ? false : true;

          if (targetDate > currentDate) {
            // To next
            p.monthsTranslate--;
            if (!p.animating) p.months.eq(p.months.length - 1).remove();
            p.wrapper.append(newMonthHTML);
            p.months = p.wrapper.find('.picker-calendar-month');
            monthTranslate = -(prevTranslate - 1) * 100 * inverter;
            p.months.eq(p.months.length - 1).transform('translate3d(' + (p.isH ? monthTranslate : 0) + '%, ' + (p.isH ? 0 : monthTranslate) + '%, 0)').addClass('picker-calendar-month-next');
          } else {
            // To prev
            p.monthsTranslate++;
            if (!p.animating) p.months.eq(0).remove();
            p.wrapper.prepend(newMonthHTML);
            p.months = p.wrapper.find('.picker-calendar-month');
            monthTranslate = -(prevTranslate + 1) * 100 * inverter;
            p.months.eq(0).transform('translate3d(' + (p.isH ? monthTranslate : 0) + '%, ' + (p.isH ? 0 : monthTranslate) + '%, 0)').addClass('picker-calendar-month-prev');
          }

          if (p.params.onMonthAdd) {
            p.params.onMonthAdd(p, dir === 'next' ? p.months.eq(p.months.length - 1)[0] : p.months.eq(0)[0]);
          }

          p.animating = true;
          p.onMonthChangeStart(dir);
          wrapperTranslate = p.monthsTranslate * 100 * inverter;
          p.wrapper.transition(transition).transform('translate3d(' + (p.isH ? wrapperTranslate : 0) + '%, ' + (p.isH ? 0 : wrapperTranslate) + '%, 0)');

          if (transitionEndCallback) {
            p.wrapper.transitionEnd(function () {
              p.onMonthChangeEnd(dir, true);
            });
          }

          if (!p.params.animate) {
            p.onMonthChangeEnd(dir);
          }
        };

        p.nextYear = function () {
          p.setYearMonth(p.currentYear + 1);
        };

        p.prevYear = function () {
          p.setYearMonth(p.currentYear - 1);
        }; // HTML Layout


        p.layout = function () {
          var pickerHTML = '';
          var pickerClass = '';
          var i;
          var layoutDate = p.value && p.value.length ? p.value[0] : new Date().setHours(0, 0, 0, 0);
          var prevMonthHTML = p.monthHTML(layoutDate, 'prev');
          var currentMonthHTML = p.monthHTML(layoutDate);
          var nextMonthHTML = p.monthHTML(layoutDate, 'next');
          var monthsHTML = '<div class="picker-calendar-months"><div class="picker-calendar-months-wrapper">' + (prevMonthHTML + currentMonthHTML + nextMonthHTML) + '</div></div>'; // Week days header

          var weekHeaderHTML = '';

          if (p.params.weekHeader) {
            for (i = 0; i < 7; i++) {
              var weekDayIndex = i + p.params.firstDay > 6 ? i - 7 + p.params.firstDay : i + p.params.firstDay;
              var dayName = p.params.dayNamesShort[weekDayIndex];
              weekHeaderHTML += '<div class="picker-calendar-week-day ' + (p.params.weekendDays.indexOf(weekDayIndex) >= 0 ? 'picker-calendar-week-day-weekend' : '') + '"> ' + dayName + '</div>';
            }

            weekHeaderHTML = '<div class="picker-calendar-week-days">' + weekHeaderHTML + '</div>';
          }

          pickerClass = 'picker-modal picker-calendar ' + (p.params.cssClass || '');
          var toolbarHTML = p.params.toolbar ? p.params.toolbarTemplate.replace(/{{closeText}}/g, p.params.toolbarCloseText) : '';

          if (p.params.toolbar) {
            toolbarHTML = p.params.toolbarTemplate.replace(/{{closeText}}/g, p.params.toolbarCloseText).replace(/{{monthPicker}}/g, p.params.monthPicker ? p.params.monthPickerTemplate : '').replace(/{{yearPicker}}/g, p.params.yearPicker ? p.params.yearPickerTemplate : '');
          }

          pickerHTML = '<div class="' + pickerClass + '">' + toolbarHTML + '<div class="picker-modal-inner">' + weekHeaderHTML + monthsHTML + '</div>' + '</div>';
          p.pickerHTML = pickerHTML;
        }; // Input Events


        function openOnInput(e) {
          e.preventDefault();
          if (p.opened) return;
          p.open();

          if (p.params.scrollToInput && !isPopover()) {
            var pageContent = p.input.parents('.page-content');
            if (pageContent.length === 0) return;
            var paddingTop = parseInt(pageContent.css('padding-top'), 10),
                paddingBottom = parseInt(pageContent.css('padding-bottom'), 10),
                pageHeight = pageContent[0].offsetHeight - paddingTop - p.container.height(),
                pageScrollHeight = pageContent[0].scrollHeight - paddingTop - p.container.height(),
                newPaddingBottom;
            var inputTop = p.input.offset().top - paddingTop + p.input[0].offsetHeight;

            if (inputTop > pageHeight) {
              var scrollTop = pageContent.scrollTop() + inputTop - pageHeight;

              if (scrollTop + pageHeight > pageScrollHeight) {
                newPaddingBottom = scrollTop + pageHeight - pageScrollHeight + paddingBottom;

                if (pageHeight === pageScrollHeight) {
                  newPaddingBottom = p.container.height();
                }

                pageContent.css({
                  'padding-bottom': newPaddingBottom + 'px'
                });
              }

              pageContent.scrollTop(scrollTop, 300);
            }
          }
        }

        function closeOnHTMLClick(e) {
          if (inPopover()) return;

          if (p.input && p.input.length > 0) {
            if (e.target !== p.input[0] && $(e.target).parents('.picker-modal').length === 0) p.close();
          } else {
            if ($(e.target).parents('.picker-modal').length === 0) p.close();
          }
        }

        if (p.params.input) {
          p.input = $(p.params.input);

          if (p.input.length > 0) {
            if (p.params.inputReadOnly) p.input.prop('readOnly', true);

            if (!p.inline) {
              p.input.on("click", function (e) {
                openOnInput(e); //修复部分安卓系统下，即使设置了readonly依然会弹出系统键盘的bug

                if (p.params.inputReadOnly) {
                  this.focus();
                  this.blur();
                }
              });
            }

            if (p.params.inputReadOnly) {
              p.input.on('focus mousedown', function (e) {
                e.preventDefault();
              });
            }
          }
        }

        if (!p.inline) $('html').on('click', closeOnHTMLClick); // Open

        function onPickerClose() {
          p.opened = false;
          if (p.input && p.input.length > 0) p.input.parents('.page-content').css({
            'padding-bottom': ''
          });
          if (p.params.onClose) p.params.onClose(p); // Destroy events

          p.destroyCalendarEvents();
        }

        p.opened = false;

        p.open = function () {
          var toPopover = isPopover();
          var updateValue = false;

          if (!p.opened) {
            // Set date value
            if (!p.value) {
              if (p.params.value) {
                p.value = p.params.value;
                updateValue = true;
              }
            } // Layout


            p.layout(); // Append

            if (toPopover) {
              p.pickerHTML = '<div class="popover popover-picker-calendar"><div class="popover-inner">' + p.pickerHTML + '</div></div>';
              p.popover = $.popover(p.pickerHTML, p.params.input, true);
              p.container = $(p.popover).find('.picker-modal');
              $(p.popover).on('close', function () {
                onPickerClose();
              });
            } else if (p.inline) {
              p.container = $(p.pickerHTML);
              p.container.addClass('picker-modal-inline');
              $(p.params.container).append(p.container);
            } else {
              p.container = $($.pickerModal(p.pickerHTML));
              $(p.container).on('close', function () {
                onPickerClose();
              });
            } // Store calendar instance


            p.container[0].f7Calendar = p;
            p.wrapper = p.container.find('.picker-calendar-months-wrapper'); // Months

            p.months = p.wrapper.find('.picker-calendar-month'); // Update current month and year

            p.updateCurrentMonthYear(); // Set initial translate

            p.monthsTranslate = 0;
            p.setMonthsTranslate(); // Init events

            p.initCalendarEvents(); // Update input value

            if (updateValue) p.updateValue();
          } // Set flag


          p.opened = true;
          p.initialized = true;

          if (p.params.onMonthAdd) {
            p.months.each(function () {
              p.params.onMonthAdd(p, this);
            });
          }

          if (p.params.onOpen) p.params.onOpen(p);
        }; // Close


        p.close = function () {
          if (!p.opened || p.inline) return;

          if (inPopover()) {
            $.closeModal(p.popover);
            return;
          } else {
            $.closeModal(p.container);
            return;
          }
        }; // Destroy


        p.destroy = function () {
          p.close();

          if (p.params.input && p.input.length > 0) {
            p.input.off('click focus', openOnInput);
          }

          $('html').off('click', closeOnHTMLClick);
        };

        if (p.inline) {
          p.open();
        }

        return p;
      };

      $.fn.calendar = function (params) {
        return this.each(function () {
          var $this = $(this);
          if (!$this[0]) return;
          var calendar = $this.data("calendar");

          if (!calendar) {
            var p = {};

            if ($this[0].tagName.toUpperCase() === "INPUT") {
              p.input = $this;
            } else {
              p.container = $this;
            }

            $this.data("calendar", new Calendar($.extend(p, params)));
          }
        });
      };

      defaults = $.fn.calendar.prototype.defaults = {
        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        dayNames: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
        dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
        firstDay: 1,
        // First day of the week, Monday
        weekendDays: [0, 6],
        // Sunday and Saturday
        multiple: false,
        dateFormat: 'yyyy-mm-dd',
        direction: 'horizontal',
        // or 'vertical'
        minDate: null,
        maxDate: null,
        touchMove: true,
        animate: true,
        closeOnSelect: true,
        monthPicker: true,
        monthPickerTemplate: '<div class="picker-calendar-month-picker" style="height: 4rem; line-height: 4rem; font-size: 1.8rem">' + '<a href="#" class="link icon-only picker-calendar-prev-month" style="height: 4rem; line-height: 4rem; font-size: 1.8rem"><i class="icon icon-prev"></i></a>' + '<div class="current-month-value" style="height: 4rem; line-height: 4rem; font-size: 1.8rem"></div>' + '<a href="#" class="link icon-only picker-calendar-next-month" style="height: 4rem; line-height: 4rem; font-size: 1.8rem"><i class="icon icon-next"></i></a>' + '</div>',
        yearPicker: true,
        yearPickerTemplate: '<div class="picker-calendar-year-picker" style="height: 4rem; line-height: 4rem; font-size: 1.8rem">' + '<a href="#" class="link icon-only picker-calendar-prev-year" style="height: 4rem; line-height: 4rem; font-size: 1.8rem"><i class="icon icon-prev"></i></a>' + '<span class="current-year-value" style="height: 4rem; line-height: 4rem; font-size: 1.8rem"></span>' + '<a href="#" class="link icon-only picker-calendar-next-year" style="height: 4rem; line-height: 4rem; font-size: 1.8rem"><i class="icon icon-next"></i></a>' + '</div>',
        weekHeader: true,
        // Common settings
        scrollToInput: true,
        inputReadOnly: true,
        convertToPopover: true,
        onlyInPopover: false,
        toolbar: true,
        toolbarCloseText: 'Done',
        toolbarTemplate: '<div class="toolbar">' + '<div class="toolbar-inner" style="height: 4rem;">' + '{{monthPicker}}' + '{{yearPicker}}' + // '<a href="#" class="link close-picker">{{closeText}}</a>' +
        '</div>' + '</div>'
        /* Callbacks
        onMonthAdd
        onChange
        onOpen
        onClose
        onDayClick
        onMonthYearChangeStart
        onMonthYearChangeEnd
        */

      };

      $.initCalendar = function (content) {
        var $content = content ? $(content) : $(document.body);
        $content.find("[data-toggle='date']").each(function () {
          $(this).calendar();
        });
      };
    }($);
  }($);
  var QUERY = Util.getQuery();
  /**
     * 初始化
     */

  var Init = function () {
    var Init = function Init() {}; // 图片幻灯片


    window.initPhotoSwipeFromElements = function (galleryElements) {
      // parse slide data (url, title, size ...) from DOM elements
      // (children of gallerySelector)
      var parseThumbnailElements = function parseThumbnailElements(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for (var i = 0; i < numNodes; i++) {
          figureEl = thumbElements[i]; // <figure> element
          // include only element nodes

          if (figureEl.nodeType !== 1) {
            continue;
          }

          linkEl = figureEl.children[0]; // <a> element

          size = linkEl.getAttribute('data-size').split('x'); // create slide object

          item = {
            src: linkEl.getAttribute('href'),
            w: parseInt(size[0], 10),
            h: parseInt(size[1], 10)
          };

          if (figureEl.children.length > 1) {
            // <figcaption> content
            item.title = figureEl.children[1].innerHTML;
          }

          if (linkEl.children.length > 0) {
            // <img> thumbnail element, retrieving thumbnail url
            item.msrc = linkEl.children[0].getAttribute('src');
          }

          item.el = figureEl; // save link to element for getThumbBoundsFn

          items.push(item);
        }

        return items;
      }; // find nearest parent element


      var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
      }; // triggers when user clicks on thumbnail


      var onThumbnailsClick = function onThumbnailsClick(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
        var eTarget = e.target || e.srcElement; // find root element of slide

        var clickedListItem = closest(eTarget, function (el) {
          return el.tagName && el.tagName.toUpperCase() === 'FIGURE';
        });

        if (!clickedListItem) {
          return;
        } // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute


        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
          if (childNodes[i].nodeType !== 1) {
            continue;
          }

          if (childNodes[i] === clickedListItem) {
            index = nodeIndex;
            break;
          }

          nodeIndex++;
        }

        if (index >= 0) {
          // open PhotoSwipe if valid index found
          openPhotoSwipe(index, clickedGallery);
        }

        return false;
      }; // parse picture index and gallery index from URL (#&pid=1&gid=2)


      var photoswipeParseHash = function photoswipeParseHash() {
        var hash = window.location.hash.substring(1),
            params = {};

        if (hash.length < 5) {
          return params;
        }

        var vars = hash.split('&');

        for (var i = 0; i < vars.length; i++) {
          if (!vars[i]) {
            continue;
          }

          var pair = vars[i].split('=');

          if (pair.length < 2) {
            continue;
          }

          params[pair[0]] = pair[1];
        }

        if (params.gid) {
          params.gid = parseInt(params.gid, 10);
        }

        return params;
      };

      var openPhotoSwipe = function openPhotoSwipe(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        if (!pswpElement) {
          pswpElement = Render.create("<div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\n                        <div class=\"pswp__bg\"></div>\n\n                        <div class=\"pswp__scroll-wrap\">\n\n                            <div class=\"pswp__container\">\n\n                                <div class=\"pswp__item\"></div>\n\n                                <div class=\"pswp__item\"></div>\n\n                                <div class=\"pswp__item\"></div>\n\n                            </div>\n\n                            <div class=\"pswp__ui pswp__ui--hidden\">\n\n                                <div class=\"pswp__top-bar\">\n\n                                    <div class=\"pswp__counter\"></div>\n\n                                    <button class=\"pswp__button pswp__button--close\" title=\"Close (Esc)\"></button>\n\n                                    <button class=\"pswp__button pswp__button--share\" title=\"Share\"></button>\n\n                                    <button class=\"pswp__button pswp__button--fs\" title=\"Toggle fullscreen\"></button>\n\n                                    <button class=\"pswp__button pswp__button--zoom\" title=\"Zoom in/out\"></button>\n\n                                    <div class=\"pswp__preloader\">\n\n                                        <div class=\"pswp__preloader__icn\">\n\n                                            <div class=\"pswp__preloader__cut\">\n\n                                                <div class=\"pswp__preloader__donut\"></div>\n\n                                            </div>\n\n                                        </div>\n\n                                    </div>\n\n                                </div>\n\n                                <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">\n\n                                    <div class=\"pswp__share-tooltip\"></div>\n\n                                </div>\n\n                                <button class=\"pswp__button pswp__button--arrow--left\" title=\"Previous (arrow left)\">\n\n                                </button>\n\n                                <button class=\"pswp__button pswp__button--arrow--right\" title=\"Next (arrow right)\">\n\n                                </button>\n\n                                <div class=\"pswp__caption\">\n\n                                    <div class=\"pswp__caption__center\"></div>\n\n                                </div>\n\n                            </div>\n\n                        </div>\n\n                    </div>");
          document.body.appendChild(pswpElement);
        }

        items = parseThumbnailElements(galleryElement); // define options (if needed)

        options = {
          // define gallery index (for URL)
          galleryUID: galleryElement.getAttribute('data-pswp-uid'),
          getThumbBoundsFn: function getThumbBoundsFn(index) {
            // See Options -> getThumbBoundsFn section of documentation for more info
            var thumbnail = items[index].el.getElementsByTagName('img')[0],
                // find thumbnail
            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                rect = thumbnail.getBoundingClientRect();
            return {
              x: rect.left,
              y: rect.top + pageYScroll,
              w: rect.width
            };
          }
        }; // PhotoSwipe opened from URL

        if (fromURL) {
          if (options.galleryPIDs) {
            // parse real index when custom PIDs are used
            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
            for (var j = 0; j < items.length; j++) {
              if (items[j].pid == index) {
                options.index = j;
                break;
              }
            }
          } else {
            // in URL indexes start from 1
            options.index = parseInt(index, 10) - 1;
          }
        } else {
          options.index = parseInt(index, 10);
        } // exit if index not found


        if (isNaN(options.index)) {
          return;
        }

        if (disableAnimation) {
          options.showAnimationDuration = 0;
        } // Pass data to PhotoSwipe and initialize it


        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
      }; // loop through all gallery elements and bind events
      // var galleryElements = document.querySelectorAll( gallerySelector );


      for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
      } // Parse URL and open gallery if it contains #&pid=3&gid=1


      var hashData = photoswipeParseHash(); // if(hashData.pid && hashData.gid) {

      openPhotoSwipe(hashData.pid, galleryElements[0], true, true); // }
    };

    function newInstance(data) {
      console.log(data);
      var Header = Component.header(data);
      var Info = Component.info(_objectSpread(_objectSpread({}, data), {}, {
        onClickEdit: function onClickEdit(info) {
          console.log("info", info);

          function open() {
            //清除签名画布
            $('#signDialogBox').remove();
            var mediaBody = Component.mediaBody(info);
            var media = Component.media({
              title: "编辑表单",
              body: mediaBody.el,
              onComplete: function onComplete(exp, done) {
                var md = mediaBody.el.__data;

                if (Util.type(md) == "function") {
                  md = md();

                  if (Util.type(md) == "string") {
                    Component.toast(md).render();
                    return;
                  }
                } // 判断必填项


                if (info.required && Util.isEmpty(md.value)) {
                  Component.toast(md.emptyTip || "请填写内容").render();
                  return;
                } // 新增验证


                if (info.validate) {
                  var result = info.validate(md.value, md.value_text, info);

                  if (result !== true) {
                    Component.toast(result).render();
                    return;
                  }

                  md.value = info.value;
                  md.value_text = info.value_text;
                } // 云盘数据为json


                if (info.type == "netdisk") {
                  info.value = JSON.stringify(md.value);
                  info.value_text = JSON.stringify(md.value);
                } else if (Util.type(md.value) == "array") {
                  info.value = md.value.join(",");
                  info.value_text = md.value_text.join(",");
                } else {
                  info.value = md.value;
                  info.value_text = md.value_text;
                } // 更新选项关联
                // setRelations(info);
                // 如果修改的时单选框，对其关联项进行显示隐藏


                if (info.type == "options" || info.type == "multiOptions") {
                  // 首先将所有关联项隐藏
                  info.options_control.map(function (id) {
                    if (data.info_maps["".concat(id, "_").concat(info.index)]) {
                      data.info_maps["".concat(id, "_").concat(info.index)].display = false;
                    }
                  }); // 针对多选下拉框对value处理成数组形式

                  info.value_text.split(',').forEach(function (value) {
                    // 然后对当前关联项进行显示
                    if (info.options.hasOwnProperty(value)) {
                      info.options[value].map(function (id) {
                        if (data.info_maps["".concat(id, "_").concat(info.index)]) {
                          data.info_maps["".concat(id, "_").concat(info.index)].display = true;
                        }
                      });
                    }
                  });
                }

                done();
              }
            }).render();
          }

          if (info.pre) {
            info.pre(function (data) {
              info.preData = data;
              open();
            });
          } else {
            open();
          }
        }
      }));
      var Process = Component.process(_objectSpread(_objectSpread({}, data), {}, {
        onReply: function onReply(D_id, A_id, message) {
          message = message.trim();

          if (message == "") {
            Component.toast("请输入回复内容").render();
            return;
          }

          Api.approvalLeaveMessage({
            approval_identity: data.approval_identity,
            student_id: data.student_id,
            D_id: D_id,
            A_id: A_id,
            message: message
          }, function (result) {
            Component.toast("回复成功").render(function () {
              location.reload();
            });
          });
        }
      }));

      if (data.is_creator) {
        var Opera = Component.opera({
          data: data,
          onTrash: function onTrash() {
            if (!window.confirm("确定撤销审核吗？")) {
              return false;
            }

            Api.revokeApproval({
              D_id: data.D_id
            }, function (result) {
              Component.toast("操作成功").render(function () {
                location.reload();
              });
            });
          },
          // 打开提醒
          onBell: function onBell() {
            if (!window.confirm("确定提醒此次审批吗？")) {
              return false;
            }

            Api.sendApprovalUser({
              D_id: data.D_id
            }, function (result) {
              Component.toast("操作成功").render(function () {
                location.reload();
              });
            });
          },
          // 再次发起
          onRetweet: function onRetweet() {
            Api.replyApprovalInfo({
              D_id: data.D_id,
              form_id: data.form_id,
              model_id: data.model_id,
              approval_identity: data.approval_identity
            });
          },
          // 作废
          onClose: function onClose() {
            Component.confirm("是否作废审批？", function () {
              Api.saveFormDetail({
                form_id: data.form_id,
                json: JSON.stringify(data.info_original),
                D_id: data.D_id,
                insert_stud_id: data.approval_identity == "0" ? data.student_id : ""
              }, function () {
                location.reload();
              });
            }).render();
          }
        });
      } // 将界面的表单数据整合到json中


      var unite = function unite(info, json, values) {
        info.map(function (item) {
          if (item.type == "part") {
            unite(item.children, json[item.id].set, json[item.id].values);
          } else if (item.editable) {
            // 不可修改的表单不放入值
            var id = item.id.split("_").slice(-1)[0]; // 如果不是显示的表单项，清空内部的值

            if (!item.display) {
              json[id].value = '';
              json[id].value_text = '';
            } else if (Util.isEmpty(values)) {
              // 如果有values，把值放入values中
              json[id].value = item.value;
              json[id].value_text = item.value_text;
            } else {
              var thisIndex = item.parent ? item.parentIndex : item.index;
              values[thisIndex][id].value = item.value;
              values[thisIndex][id].value_text = item.value_text;
            }
          }
        });
      };

      if (data.assessor) {
        var el = Component.complain(data).el;
        var prefix_title = data.complain_required ? "<span class='link-danger'>*</span> " : "";
        var Tabbar = Component.tabbar(Object.assign(data, {
          onPass: function onPass() {
            Component.media({
              title: prefix_title + "通过审批",
              body: el,
              onComplete: function onComplete(form, done) {
                // 是否有必填项没填写的情况
                for (var i = 0; i < data.info.length; i++) {
                  var info = data.info[i]; // 显示项 && 必填项 && 可编辑项 && 值不能为空

                  if (info.display && info.required && info.editable && Util.isEmpty(info.value)) {
                    Component.toast("".concat(info.title, "\u4E0D\u80FD\u4E3A\u7A7A\uFF01")).render();
                    return false;
                  }
                } // 限制必填审批意见


                if (data.complain_required && form.complain == "") {
                  Component.toast("请填写审批意见").render();
                  return false;
                } // 保存json数据


                var json = JSON.parse(JSON.stringify(data.info_original));
                unite(data.info, json);
                Component.confirm("是否同意审批？", function () {
                  done();
                  Api.approval({
                    D_id: data.D_id,
                    A_id: data.A_id,
                    AD_id: data.AD_id,
                    pass: "1",
                    complain: form.complain,
                    formDetailJson: JSON.stringify(json)
                  }, function (result) {
                    location.reload();
                  });
                }).render();
              }
            }).render();
          },
          onRefuse: function onRefuse() {
            Component.media({
              title: prefix_title + "拒绝审批",
              body: el,
              onComplete: function onComplete(form, done) {
                if (data.complain_required && form.complain == "") {
                  Component.toast("请填写审批意见").render();
                  return false;
                } // 保存json数据


                var json = JSON.parse(JSON.stringify(data.info_original));
                unite(data.info, json);
                Component.confirm("是否拒绝审批？", function () {
                  done();
                  Api.approval({
                    D_id: data.D_id,
                    A_id: data.A_id,
                    AD_id: data.AD_id,
                    pass: "0",
                    complain: form.complain,
                    formDetailJson: JSON.stringify(json)
                  }, function (result) {
                    location.reload();
                  });
                }).render();
              }
            }).render();
          }
        }));
      } // 加载组件


      var Container = Render.getContainer('app');
      Container.append(Header, Info, Process);

      if (data.warning_label != "") {
        Container.append(Component.warningText({
          text: data.warning_label,
          type: "danger"
        }));
      }

      if (data.is_creator) {
        Container.append(Opera);
      }

      if (data.assessor) {
        Container.append(Tabbar);
      } // +yk: 添加印章


      var Stamp = Component.stamp(data.approval_status);

      if (Stamp) {
        Container.append(Stamp);
      }

      if (data.invalid_id !== false) {
        Api.getStartApprovalInfo({
          D_id: data.invalid_id
        }, function (result) {
          var detail = getDetail(result.data);
          Process.el.after(Component.process({
            title: "作废流程",
            process: detail.process || [],
            onReply: function onReply(D_id, A_id, message) {
              message = message.trim();

              if (message == "") {
                Component.toast("请输入回复内容").render();
                return;
              }

              Api.approvalLeaveMessage({
                approval_identity: data.approval_identity,
                student_id: data.student_id,
                D_id: D_id,
                A_id: A_id,
                message: message
              }, function (result) {
                Component.toast("回复成功").render(function () {
                  location.reload();
                });
              });
            }
          }).el);
        });
      } // 修改单选多选对其关联项进行显示隐藏


      function setRelations(info) {
        if (info.type != "options" && info.type != "multiOptions") {
          return;
        }

        if (info.options_control.length === 0) {
          return;
        }

        var values_obj = {},
            // 用于存储所有表单项的value和value_text
        hidden_keys = [].concat(info.options_control),
            // 用于存储即将被隐藏的项
        current_keys = [],
            // 用于存储即将显示的项
        current_obj = {}; // 去重用

        for (var _i2 in data.info_maps) {
          values_obj[_i2] = {
            value: data.info_maps[_i2].value,
            value_text: data.info_maps[_i2].value_text
          };
        }

        info.value.split(',').map(function (val) {
          if (info.original.type == "4") {
            if (info.options[val]) {
              info.options[val].map(function (infoVal) {
                current_obj[infoVal] = infoVal;
              });
            }
          } else if (info.original.type == "5") {
            if (info.options_values[val]) {
              info.options_values[val].map(function (infoVal) {
                current_obj[infoVal] = infoVal;
              });
            }
          }
        });

        for (var _i3 in current_obj) {
          current_keys.push(current_obj[_i3]);
        }

        current_keys.map(function (k) {
          if (~hidden_keys.indexOf(k)) {
            hidden_keys.splice(hidden_keys.indexOf(k), 1);
          }
        }); // 首先将所有关联项隐藏

        info.options_control.map(function (id) {
          if (data.info_maps["".concat(id, "_").concat(info.index)]) {
            data.info_maps["".concat(id, "_").concat(info.index)].display = false;
            data.info_maps["".concat(id, "_").concat(info.index)].value = '';
            data.info_maps["".concat(id, "_").concat(info.index)].value_text = '';
          }
        });
        /**
           * 先获取选中的链表
           * 在已有的链表规则中寻找当前操作获取的链表 
           * 
           * */

        var keysArr = data.relationLines.keys,
            keysObj = {},
            ownKeyName = info.id,
            disabledRelationLines = data.relationLines.disabledRelationLines,
            isLineHead = false,
            changeOwnkeys = function changeOwnkeys(key) {
          return info.parent ? key.replace(/(\d+)_(\d+)/, '$1_' + info.parentIndex + '_$2') : key; // 转换自增明细内的
        }; // 明细内


        if (info.parent) {
          disabledRelationLines = data.relationLines.disabledOwnRelationLines;
          keysArr = data.relationLines.ownKeys;
          ownKeyName = [info.parent, info.parentIndex, info.id].join('_');
        }

        for (var _i4 = 0, len = disabledRelationLines.length; _i4 < len; _i4++) {
          var line = disabledRelationLines[_i4].map(function (item) {
            return changeOwnkeys(item);
          });

          if (line.indexOf(ownKeyName) === 0) {
            isLineHead = true;
          }

          disabledRelationLines[_i4].map(function (t, index) {
            if (index > 0) {
              t = changeOwnkeys(t);
              var _i5 = 0;

              while (true) {
                if (!data.info_maps[t + '_' + _i5]) {
                  break;
                }

                data.info_maps[t + '_' + _i5].display = false;
                data.info_maps[t + '_' + _i5].value = '';
                data.info_maps[t + '_' + _i5].value_text = '';
                _i5 += 1;
              }
            }
          });
        }

        keysArr.map(function (item) {
          var itemIndex = item.indexOf(ownKeyName); // 过滤删除

          if (~itemIndex) {
            if (~hidden_keys.indexOf(item[itemIndex + 1])) {
              item.splice(itemIndex + 1);
            }
          }

          keysObj[JSON.stringify(item)] = item;
        }); // 去重

        keysArr.length = 0;

        for (var i in keysObj) {
          if (keysObj[i].length > 1) {
            keysArr.push(keysObj[i]);
          }
        }

        if (isLineHead) {
          current_keys.forEach(function (key) {
            keysArr.push([ownKeyName].concat(key));
          });
        } else {
          // 去重
          keysArr.length = 0;

          for (var i in keysObj) {
            keysArr.push(keysObj[i]);
          }

          var tempPushLine = []; // 即将加入的链表

          keysArr.map(function (item) {
            var itemIndex = item.indexOf(ownKeyName);

            if (~itemIndex) {
              for (var _i6 = 1, _len = current_keys.length; _i6 < _len; _i6++) {
                tempPushLine.push([].concat(item).push(current_keys[_i6]));
              }

              item.push(current_keys[0]);
            }
          });
          keysArr.concat(tempPushLine);
        }

        disabledRelationLines.forEach(function (item) {
          keysArr.map(function (line) {
            var ownItem = item.map(function (k) {
              return changeOwnkeys(k);
            });

            if (ownItem.join(',').indexOf(line.join(',')) === 0) {
              line.map(function (t) {
                var i = 0;

                while (true) {
                  if (!data.info_maps[t + '_' + i]) {
                    break;
                  }

                  data.info_maps[t + '_' + i].display = true;
                  data.info_maps[t + '_' + i].value = values_obj[t + '_' + i].value;
                  data.info_maps[t + '_' + i].value_text = values_obj[t + '_' + i].value_text;
                  i += 1;
                }
              });
            }
          });
        }); // 针对多选下拉框对value处理成数组形式

        info.value_text.split(',').forEach(function (value) {
          // 然后对当前关联项进行显示
          if (info.options.hasOwnProperty(value)) {
            info.options[value].map(function (id) {
              if (data.info_maps["".concat(id, "_").concat(info.index)]) {// data.info_maps[`${id}_${info.index}`].display = true;
              }
            });
          }
        });
      }

      Container.render();
    }

    function getDetail(data) {
      var detail = {
        A_id: QUERY.a,
        D_id: data.formDetail.id,
        AD_id: QUERY.ad,
        approval_identity: data.formDetail.identity,
        student_id: data.formDetail.insert_user == Config.user.id ? data.formDetail.insert_stud_id : "",
        form_id: data.formDetail.form_id,
        model_id: data.formDetail.model_id,
        avatar: data.formDetail.head_url || Config.default_avatar,
        name: data.formDetail.startUserName,
        mobile: data.formDetail.startMobile,
        info: JSON.parse(data.formDetail.json),
        info_original: JSON.parse(data.formDetail.json),
        process: data.formApprovalList,
        // 1正在审批 2审批通过 3审批拒绝 4审批撤销
        status: data.formDetail.formApprovalDetailStatus + "",
        // +yk: 是否为审核员，用来展示按钮用
        assessor: function (formDetail) {
          return formDetail.formApprovalDetailStatus == 1 && formDetail.approval_status == 1 && formDetail.formApprovalStatus == 1 && !formDetail.invalid_id;
        }(data.formDetail),
        // ＋yk: 是否为创作者
        is_creator: data.formDetail.insert_user == Config.user.id,
        creator_id: data.formDetail.insert_user,
        node: undefined,
        // 审批意见是否必填
        complain_required: data.formDetail.approvalIsRequired == 1,
        // 审批意见是否对发起人可见
        complain_visible: true,
        warning_label: "",
        // 1正在审批 2审批通过 3审批拒绝 4审批撤销
        approval_status: data.formDetail.approval_status + "",
        // 审批意见提示文字
        approval_tips: Util.isEmpty(data.formDetail.approvalTips) ? "" : data.formDetail.approvalTips,
        // 作废ID
        invalid_id: data.formDetail.invalid_id || false,
        // 是否已作废
        is_invalid: data.formDetail.is_invalid == 1,
        // 教职考勤 2
        category: data.formDetail.category,
        //年级名称
        grade_name: data.formDetail.startGradeName,
        //班级名称
        class_name: data.formDetail.startClassName
      }; // 审批意见是否对发起人可见 approvalIsHidden 0显示 1隐藏
      // 判断是否不为发起人，否则判断是否可见

      detail.complain_visible = !detail.is_creator || data.formDetail.approvalIsHidden == 0; // ＋yk: 状态为5添加警告文字

      if (data.formDetail.approval_status == 5) {
        detail.warning_label = "因流程设置异常，该审批流程无法通过，请联系学校管理人员修改！";
      } // 追加当前查看节点


      try {
        var approval = JSON.parse(data.formDetail.approval);
        data.formDetail.node.trim().split(",").map(function (n) {
          if (n != "") approval = approval[n];
        });
        detail.node = approval;
      } catch (error) {
        console.error("approval查找node失败");
      } // 追加编辑权限，只放入可编辑的index


      var auth = {}; // if (detail.node != undefined && detail.assessor) {
      //     for (let index in detail.node.auth) {
      //         if (detail.node.auth[index] == "1") {
      //             auth.push(index);
      //         }
      //     }
      // }

      var _authProcess = function _authProcess(process) {
        var ele = {};

        if (process.hasOwnProperty("approvalEleJson") && !Util.isEmpty(process.approvalEleJson)) {
          ele = JSON.parse(process.approvalEleJson);
        }

        if (!Util.isEmpty(ele.auth)) {
          for (var key in ele.auth) {
            if (auth[key]) {
              if (ele.auth[key] == "1" && ["2", "3"].includes(auth[key] + "")) {
                continue;
              }

              if (ele.auth[key] == "2" && ["3"].includes(auth[key] + "")) {
                continue;
              }
            }

            auth[key] = ele.auth[key] + "";
          }
        }
      }; // 处理显示权限
      // 查询到隐藏时添加隐藏属性，否则都可看
      // assessor=true时为先天条件


      var reverse = [].concat(detail.process).reverse();

      if (detail.assessor && !detail.is_invalid) {
        _authProcess(reverse[0]);
      } else {
        reverse = reverse.slice(1);

        for (var index in reverse) {
          var process = reverse[index];

          if (Util.isEmpty(process.user_id)) {
            break;
          }

          if (!process.user_id.split(",").includes(Config.user.id)) {
            break;
          }

          _authProcess(process);
        } // 1权限变为2只读权限


        for (var i in auth) {
          if (auth[i] == "1") {
            auth[i] = "2";
          }
        }
      } // 处理详情


      detail.info_maps = {};

      detail.info = function compileInfo(info) {
        var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var editable = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        var parent = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
        var parentIndex = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
        var _info = [];

        if (!Util.isEmpty(value)) {
          for (var _i7 in value) {
            info[_i7].value = value[_i7].value;
            info[_i7].value_text = value[_i7].value_text;
          }
        }

        Util.likeItemObjectMap(info, function (item, index) {
          // 权限隐藏
          if (auth[index] == "3" && parent === "") {
            return true;
          }

          item = JSON.parse(JSON.stringify(item));

          if (item.type == "9" && item.is_show == false) {
            return true;
          }

          var __infos = [];

          switch (item.type + "") {
            case "4":
              var maps4 = {}; // 该单选框控制的其他表单项id

              var controls4 = [];
              Util.likeItemObjectMap(item.options, function (option) {
                maps4[option.text] = option.relation.map(function (r) {
                  r += "";

                  if (parent !== "") {
                    r = [parent, parentIndex, r].join('_'); // r = parent + '_' + parentIndex + '_' + index + '_' + r;
                  }

                  if (!controls4.includes(r)) {
                    controls4.push(r);
                  }

                  return r;
                });
              });

              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "options",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                options: maps4,
                options_control: controls4
              });

              break;

            case "5":
              var maps5 = {},
                  mapsValues = {}; // 该多选框控制的其他表单项id

              var controls5 = [];
              Util.likeItemObjectMap(item.options, function (option, index) {
                maps5[option.text] = option.relation.map(function (r) {
                  r += "";

                  if (parent !== "") {
                    r = parent + '_' + r;
                  }

                  if (!controls5.includes(r)) {
                    controls5.push(r);
                  }

                  return r;
                });
                mapsValues[index] = maps5[option.text];
              });

              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "multiOptions",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                options: maps5,
                options_control: controls5,
                options_values: mapsValues
              });

              break;

            case "7":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "image",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                max_image_length: item.uploadImgNumber == "只可上传一张" ? 1 : 9,
                source_type: item.uploadImgWay == "拍照或本地上传" ? ['album', 'camera'] : ['camera']
              });

              break;

            case "71":
              //手写签名
              __infos.push({
                id: index + "",
                index: 0,
                parent: parent,
                parentIndex: parentIndex,
                title: item.title,
                placeholder: item.placeholder,
                type: "sign",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force
              });

              break;

            case "8":
              // 附件
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "netdisk",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force
              });

              break;

            case "99":
              item.values.map(function (value, i) {
                __infos.push({
                  id: index + "",
                  index: i,
                  parentIndex: parentIndex,
                  parent: parent,
                  title: item.title,
                  type: "part",
                  original: item,
                  children: compileInfo(item.set, value, auth[index] == "1", index + "", i),
                  editable: editable || auth[index] == "1"
                });
              });
              break;

            case "9":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "text",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: false,
                required: item.is_force
              });

              break;

            case "31":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "tel",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                validate: function validate(value, value_text, data) {
                  if (value == "") {
                    data.value = value;
                    data.value_text = value_text;
                    return true;
                  }

                  if (!/^1[0-9]{10}$/.test(value)) {
                    return "手机号格式错误！";
                  }

                  data.value = value;
                  data.value_text = value_text;
                  return true;
                }
              });

              break;

            case "32":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "id",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                validate: function validate(value, value_text, data) {
                  if (value == "") {
                    data.value = value;
                    data.value_text = value_text;
                    return true;
                  }

                  var val = value;
                  var isRight = false;

                  if ($.trim(val).length === 18) {
                    if (isNaN(+val.substring(0, 17))) {
                      isRight = false;
                    } else {
                      if (val.substring(17, 18) !== 'x' && val.substring(17, 18) !== 'X' && isNaN(+val.substring(17, 18))) {
                        isRight = false;
                      } else {
                        isRight = true;
                      }
                    }
                  }

                  if (!isRight) {
                    return '身份证号格式错误！';
                  }

                  data.value = value.replace('x', 'X');
                  data.value_text = value_text.replace('x', 'X');
                  return true;
                }
              });

              break;

            case "33":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "text",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                warning: item.warning
              });

              break;

            case "51":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "address",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                pre: function pre(done) {
                  Api.getAddress({}, function (data) {
                    done(data);
                  }, function (e) {
                    console.log(e);
                  });
                },
                preData: {}
              });

              break;

            case "71":
              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "sign",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force
              });

              break;

            default:
              // +yk: 判断数字输入框，然后title后面追加单位unit文字
              if (item.type == "3" && !Util.isEmpty(item.unit)) {
                item.title += "（" + item.unit + "）";
              }

              __infos.push({
                id: index + "",
                index: 0,
                parentIndex: parentIndex,
                parent: parent,
                title: item.title,
                placeholder: item.placeholder,
                type: "text",
                value: item.value,
                value_text: item.value_text,
                original: item,
                editable: editable || auth[index] == "1",
                required: item.is_force,
                date_type: item.dateType || "",
                is_hidden: item.is_hidden || false
              });

          }

          __infos.map(function (__info) {
            // 添加显示隐藏
            if (Util.isEmpty(item.relationed) || Util.type(item.relationed) == "array" && item.relationed.length == 1 && item.relationed[0] == 0) {
              // 未关联直接显示
              __info.display = true;
              __info.relationed = [];
            } else {
              // 已关联先隐藏
              __info.display = false;
              __info.relationed = item.relationed.map(function (r) {
                return r + "";
              });
            }

            _info.push(__info);

            if (!parent) {
              detail.info_maps["".concat(__info.id, "_").concat(__info.index)] = __info;
            } else {
              detail.info_maps["".concat(parent, "_").concat(parentIndex, "_").concat(__info.id, "_").concat(__info.index)] = __info;
            }
          });
        }); // 将所有的单选框关联项打开

        _info.map(function (__info) {
          if (__info.type == "options" || __info.type == "multiOptions") {
            __info.value_text.split(',').forEach(function (value) {
              if (__info.options.hasOwnProperty(value)) {
                __info.options[value].map(function (r) {
                  var i = 0; // 解决遍历明细造成info_maps数据错乱情况

                  var key = "".concat(r, "_").concat(i); // if (parent) {
                  //     key = `${parent}_${r}_${i}`;
                  // }

                  while (detail.info_maps.hasOwnProperty(key)) {
                    detail.info_maps[key].display = true;
                    i++;
                    key = "".concat(r, "_").concat(i); // if (parent) {
                    //     key = `${parent}_${r}_${i}`;
                    // }
                  }
                });
              }
            });
          }
        });

        return _info;
      }(detail.info); // 定义将要选项关联的keys


      detail.relationLines = {
        keys: [],
        ownKeys: [],
        disabledOwnRelationLines: detail.info_original.others.disabledOwnRelationLines.map(function (item) {
          return item.map(function (i) {
            return i + "";
          });
        }),
        disabledRelationLines: detail.info_original.others.disabledRelationLines.map(function (item) {
          return item.map(function (i) {
            return i + "";
          });
        })
      }; // 包含单选框的详情初始显示数据修改

      detail.info.map(function (info) {
        var map = "".concat(info.id, "_").concat(info.index);

        if (info.type != "options" && info.type != "multiOptions") {
          return true;
        }

        info.value.split(",").forEach(function (value) {
          if (info.options.hasOwnProperty(value)) {
            info.options[value].map(function (id) {
              if (detail.info_maps["".concat(id, "_").concat(info.index)]) {
                detail.info_maps["".concat(id, "_").concat(info.index)].display = true;
              }
            });
          }
        });
      }); // 处理流程

      detail.process = detail.process.map(function (process, index) {
        var persons = process.formApprovalDetailList.map(function (person) {
          return {
            andor: person.andor || "",
            avatar: person.head_url || "",
            name: person.user_name,
            status: person.detail_status,
            complain: person.content || "",
            readed: person.is_read == 1
          };
        });
        var person = persons[0],
            person_count = persons.length;

        if (person == undefined) {
          person = {};
        }

        var andor_text = {
          1: "(会签)",
          2: "(或签)"
        }[person.andor] || "";
        var ele = {};

        if (process.hasOwnProperty("approvalEleJson") && !Util.isEmpty(process.approvalEleJson)) {
          ele = JSON.parse(process.approvalEleJson);
        }

        var result = {
          D_id: process.formDetailId,
          A_id: process.formApprovalId,
          persons: persons,
          title: ele.title,
          avatar: person.avatar || Config.default_avatar,
          name: (person.name || "") + (person_count > 1 ? "等" + person_count + "人" + andor_text : ""),
          time: process.type == 2 ? process.status == 2 || process.status == 3 ? process.approvalTimeStr : "" : process.insertTimeStr,
          status: function (status) {
            var _status = {
              "1": "approval",
              "2": "success",
              "3": "refuse"
            };
            return _status[status];
          }(process.status + ""),
          type: function (type) {
            var _type = {
              "1": "copy",
              "2": "approval"
            };
            return index == 0 ? "original" : _type[type] || "";
          }(process.type + ''),
          type_text: "",
          label: "",
          // 留言
          reply: function (message_list, complain_visible) {
            var replys = [];
            message_list.map(function (message) {
              // 先判断是否对发起人可见，如果不可见，仅获取发起人的留言内容
              if (complain_visible || detail.creator_id == message.user_id) {
                replys.push({
                  id: message.formApprovalMessageId + "",
                  reply: message.content,
                  name: message.user_name
                });
              }
            });
            return replys;
          }(process.formApprovalMessageList, detail.complain_visible),
          // 审批意见
          complain: function (persons, complain_visible) {
            var complains = [];
            persons.map(function (person) {
              // 先判断是否对发起人可见，如果不可见，仅获取发起人的留言内容
              if (complain_visible || detail.creator_id == person.user_id) {
                if (person.complain) {
                  complains.push({
                    name: person.name,
                    content: person.complain
                  });
                }
              }
            });
            return complains;
          }(persons, detail.complain_visible)
        };

        result.type_text = function (type) {
          var _type = {
            copy: "抄送人",
            approval: "审批人",
            original: "发起人"
          };
          return _type[type] || "";
        }(result.type);

        if (result.type == "original") {
          result.label = "";
        } else if (result.type == "copy") {
          var readed = 0;
          process.formApprovalDetailList.map(function (item) {
            if (item.is_read == 1) {
              readed += 1;
            }
          });
          result.label = readed + "人已读";
        } else if (result.status == "approval") {
          result.label = "审批中";
        } else if (result.status == "success") {
          result.label = "已通过";
        } else if (result.status == "refuse") {
          result.label = "已拒绝";
        }

        return result;
      });

      if (data.formDetail.node == "end") {
        detail.process.push({
          end: true
        });
      }

      return detail;
    }

    Init.prototype.instance = function () {
      // 解决当预览过程中刷新页面时造成的后退多次才能生效的问题
      if (location.hash.slice(1) != "") {
        history.back();
      }

      Api.getApprovalInfo({
        AD_id: QUERY.ad
      }, function (result) {
        if (Util.type(result.data.formDetail) == 'null' || result.data.formDetail + "" == "") {
          Component.toast('数据不存在').render();
          return false;
        } // +yk: 列表页返回刷新


        sessionStorage.setItem("approvalDetailChanged", "changed"); // 整理数据

        var detail = getDetail(result.data);
        newInstance(detail);
      });
    };

    return new Init();
  }();

  if (Util.isEmpty(BASE.TOKEN)) {
    getUserLoginBaseInfoOfOpenid(QUERY.openid, QUERY.org_id, QUERY.identity, function (data) {
      Config.user.token = BASE.TOKEN = data.token;
      Config.user.udid = BASE.UDID = data.udid;
      Config.user.id = BASE.USER_ID = data.orguser.user_id + "";
      Config.user.org_id = BASE.ORG_ID = data.orguser.org_id; // 初始化执行

      Api.jssdkRegister(function () {
        Init.instance();
      });
    }, true);
  } else {
    // 初始化执行
    Api.jssdkRegister(function () {
      Init.instance();
    });
  }
})(function () {
  var Base = {
    orguser: {}
  };

  try {
    Base = JSON.parse(sessionStorage.baseUser);
  } catch (e) {}

  return {
    DEFAULT_AVATAR: "/shijiwxy/weixin/images/defaultHead.jpg",
    API_BASE_URL: domainName,
    TOKEN: Base.token,
    UDID: Base.udid,
    USER_ID: Base.orguser.user_id + "",
    ORG_ID: Base.orguser.org_id,
    VERSION: baseParameter.version
  };
});